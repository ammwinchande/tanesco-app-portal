<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$now = new \DateTime('now');
$tomorrow = new \DateTime('tomorrow');
$start = $now->getTimestamp();
$end = $tomorrow->getTimestamp();
$sess_timeout = $end - $start;

$config = [
    'id' => 'tanesco_app',
    'name' => 'TANESCO App Portal',
    'basePath' => dirname(__DIR__),

    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@mdm/admin' => '@vendor/mdmsoft/yii2-admin',
    ],
    'components' => [
        'session' => [
            'timeout' => $sess_timeout, //1440,
            'class' => 'yii\web\DbSession',
            'writeCallback' => function ($session) {
                return [
                    'user_id' => Yii::$app->user->id
                ];
            }
        ],
        'toSdm' => [
            'class' => 'app\components\SendToSdm',
        ],
        'soapclient' => [
            'class' => 'app\components\SoapClientComponent',
        ],
        'filter' => [
            'class' => 'app\components\ArrayAccess',
        ],
        'behavior' => [
            'class' => 'app\components\ActionBehaviors',
        ],
        'formatData' => [
            'class' => 'app\components\FormatData',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'yoe4mEt9UBNk-Mx1pZhg8dlLsJPIgSgy',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            // 'identityClass' => 'Edvlerblog\Adldap2\model\UserDbLdap',
            'enableAutoLogin' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'class' => 'yii\log\Dispatcher',
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                'db' => [
                    'class' => 'yii\log\DbTarget',
                    'except' => ['yii\web\HttpException:404'],
                    'levels' => ['error', 'warning', 'info', 'trace', 'profile'],
                    //'levels' => ['error'],
                ],
                'file' => [
                    'class' => 'yii\log\FileTarget',
                    //'categories' => ['yii\web\HttpException:404'],
                    'except' => ['yii\web\HttpException:404'],
                    //'levels' => ['error', 'warning', 'info', 'trace','profile'],
                    'levels' => ['warning', 'info', 'trace', 'profile'],
                    'logFile' => '@runtime/logs/app.log',
                    'maxFileSize' => 1024 * 1024 * 1,
                    'maxLogFiles' => 5,
                ],
                'email' => [
                    'class' => 'yii\log\EmailTarget',
                    'except' => ['yii\web\HttpException:404'],
                    'levels' => ['error'],
                    'message' => [
                        'from' => 'epayment.system@tanesco.co.tz',
                        'to' => 'juma.kobello@tanesco.co.tz'
                    ],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager' // or use 'yii\rbac\PhpManager',
        ],
        'as access' => [
            'class' => 'mdm\admin\components\AccessControl',
            'allowActions' => [
                /*'site/*',
                'admin/*',
                'some-controller/some-action',*/]
        ],
        'ad' => [
            'class' => 'Edvlerblog\Adldap2\Adldap2Wrapper',
            'providers' => [
                'default' => [
                    'autoconnect' => true,
                    'config' => [
                        'timeout' => 60,
                        'account_suffix' => '@tanesco.co.tz',
                        'hosts' => ['192.168.249.2', '192.168.249.9'],
                        'base_dn' => 'dc=tanesco,dc=co,dc=tz',
                        'username' => 'system.prepaidrecon@tanesco.co.tz',
                        'password' => 'TopPAID18',
                    ]
                ],
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
        ],
        'api' => [
            'class' => 'app\modules\api\Module',
        ],
        'user-db-log' => [
            'class' => 'podtserkovsky\userdblog\Module',
        ],
        'audit' => 'bedezign\yii2\audit\Audit',
        'gridview' => [
            'class' => '\kartik\grid\Module',
            //'downloadAction' => 'gridview/export/download',
            //'downloadAction' =>'/project-screen-shots/index',
            'i18n' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@kvgrid/messages',
                'forceTranslation' => true
            ]
        ],
        'dynagrid' => [
            'class' => '\kartik\dynagrid\Module',
            'dbSettings' => [
                'tableName' => 'tbl_dynagrid',
                'idAttr' => 'id',
                'filterAttr' => 'filter_id',
                'sortAttr' => 'sort_id',
                'dataAttr' => 'data'
            ],
            'dbSettingsDtl' => [
                'tableName' => 'tbl_dynagrid_dtl',
                'idAttr' => 'id',
                'categoryAttr' => 'category',
                'nameAttr' => 'name',
                'dataAttr' => 'data',
                'dynaGridIdAttr' => 'dynagrid_id'
            ],
            'dynaGridOptions' => [
                'allowThemeSetting' => false
            ],
            'defaultTheme' => 'panel-default',
            'defaultPageSize' => 20,
            'minPageSize' => 0,
            'maxPageSize' => 1000000
        ],
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '::1'],
            //'password' => '123456'
        ],
    ],

    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';

    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1'],
        // 'generators' => [
        //     'sintret' => [
        //         'class' => 'sintret\gii\generators\crud\Generator',
        //     ],
        //     'sintretModel' => [
        //         'class' => 'sintret\gii\generators\model\Generator'
        //     ]
        // ]
    ];
}

return $config;
