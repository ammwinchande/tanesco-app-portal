<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 90,
    'bsVersion' => '4.x',
    'NavX::bsVersion' => '3.x',
];
