<?php

namespace app\components;

use yii\base\Component;
use SoapClient;

class SoapClientComponent extends Component
{
    private $url;
    private $client;

    public function init()
    {
        $this->url = 'http://192.168.1.22:8080/websdm.asmx?wsdl';
        parent::init();
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function display($content = null)
    {
        if ($content != null) {
            $this->content = $content;
        }
        echo Html::encode($this->content);
    }

    public function getClient()
    {
        $service_url = $this->url;

        try {
            $arrContextOptions = [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT
                ]
            ];
            $options = [
                'soap_version' => SOAP_1_2,
                'exceptions' => true,
                'trace' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'stream_context' => stream_context_create($arrContextOptions)
            ];
            $this->client = new SoapClient($service_url, $options);
        } catch (Exception $e) {
            echo '<h2>Exception Error!</h2>';
            echo $e->getMessage();
        }
        return $this->client;
    }

    public function getServiceNumberOld($service_report)
    {
        $tb = $this->getClient()->__call('NewEmergency', ['body' => $service_report]);
        return $tb->NewEmergencyResult;
    }

    public function getServiceNumber($service_report)
    {
        $tb = $this->getClient()->__call('NewService', ['body' => $service_report]);
        return $tb->NewServiceResult;
    }

    public function getMeterNoVerification($meter_no)
    {
        $isMeterNoValid = $this->getClient()->__call('VerifyMeter', ['body' => ['Meter_Number' => $meter_no]]);
        return new \SimpleXMLElement($isMeterNoValid->VerifyMeterResult);
    }

    public function getServiceNumberStatus($service_no)
    {
        $service_status = $this->getClient()->__call('ServiceStatus', ['body' => ['Service_Number' => $service_no]]);
        // return $service_status->ServiceStatusResult;
        return new \SimpleXMLElement($service_status->ServiceStatusResult);
    }

    public function getZones()
    {
        $zones = $this->getClient()->__call('Zones', ['body' => '']);
        return new \SimpleXMLElement($zones->ZonesResult);
    }

    public function getRegions()
    {
        $regions = $this->getClient()->__call('Regions', ['body' => '']);
        return new \SimpleXMLElement($regions->RegionsResult);
    }

    public function getDistricts($region)
    {
        $districts = $this->getClient()->__call('Districts', ['body' => $region]);
        return new \SimpleXMLElement($districts->DistrictsResult);
    }

    public function getAreas($region_and_district)
    {
        $areas = $this->getClient()->__call('Areas', ['body' => $region_and_district]);
        return new \SimpleXMLElement($areas->AreasResult);
    }

    public function getAreasStreets()
    {
        $areasStreets = $this->getClient()->__call('AreasStreets', ['body' => '']);
        return new \SimpleXMLElement($areasStreets->AreasStreetsResult);
    }

    public function getStreets()
    {
        $streets = $this->getClient()->__call('Streets', ['body' => '']);
        return new \SimpleXMLElement($streets->StreetsResult);
    }

    public function getServiceNames()
    {
        $serviceNames = $this->getClient()->__call('ServiceNames', ['body' => '']);
        return new \SimpleXMLElement($serviceNames->ServiceNamesResult);
    }

    public function getServices($params)
    {
        $services = $this->getClient()->__call('Services', ['body' => $params]);
        // return $services->ServicesResult;
        return new \SimpleXMLElement($services->ServicesResult);
    }

    public function getServicesSummary($params)
    {
        $reportedServices = $this->getClient()->__call('Statistics', ['body' => $params]);
        return new \SimpleXMLElement($reportedServices->StatisticsResult);
    }

    public function getServicesSummaryByLocation($params)
    {
        $reportedServicesByLocation = $this->getClient()->__call('StatisticsbyLocation', ['body' => $params]);
        return new \SimpleXMLElement($reportedServicesByLocation->StatisticsbyLocationResult);
    }

    public function getProblemsFrequency($params)
    {
        $preblemFrequency = $this->getClient()->__call('ProblemsFrequency', ['body' => $params]);
        // return $preblemFrequency->ProblemsFrequencyResult;
        return new \SimpleXMLElement($preblemFrequency->ProblemsFrequencyResult);
    }

    public function getServicesByDuration($params)
    {
        $servicesByDuration = $this->getClient()->__call('ServicesDuration', ['body' => $params]);
        return new \SimpleXMLElement($servicesByDuration->ServicesDurationResult);
    }
}
