<?php
/**
 * Created by Engr. Kobello
 * Date: 08-Nov-2019
 * Time: 15:47
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\Inflector;
use yii\filters\VerbFilter;

/**
 * Description of Traits
 */
class SendToSdm extends Component
{

    /**
     * Initializing Component
     * @return component
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Send service report from client to SDM
     *
     * @param Service $service_report
     *
     * @return $status
     */
    public function send($service_report)
    {
        return Yii::$app->soapclient->getServiceNumber($service_report);
    }
}
