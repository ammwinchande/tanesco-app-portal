<?php
/**
 * Created by Engr. Kobello
 * Date: 19-July-2019
 * Time: 15:51
 */

namespace app\components;

use Yii;
use yii\helpers\Json;
use yii\base\Component;
use app\models\User;
use app\models\Reports;
use yii\helpers\Inflector;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;

/**
 * Description of Traits
 */
class FormatData extends Component
{

    /**
     * Initializing Component
     * @return component
     */
    public function init()
    {
        parent::init();
    }

    public function xml2array( $xmlObject, $out = [] )
    {
        foreach ( (array) $xmlObject as $index => $node )
            $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;

        return $out;
    }

    public function xml2arrayElements( $xmlObject, $out = [] )
    {
        $array = (array) $xmlObject;

        if (count($array) === 0) {
            return $array;
            // return (string) $xmlObject;
        }
        foreach ((array) $xmlObject as $index => $node) {
            if (!is_object($node) || strpos(get_class($node), 'SimpleXML') === false) {
                continue;
            };
            $out[$index] = count($array) === 1 ? $node : xml2arrayElements($node);
        }
        return $out;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArayDataProvider
     */
    public function search(
        $fromDate = null,
        $toDate = null,
        $zone = "",
        $region = "",
        $district = "",
        $service = "",
        $status = "",
        $duration = ""
    ) {
        $usesrId = Yii::$app->user->getId();
        $role = User::getRole();

        if ($role == "sysadmin" || $role == "admin" || $role == "hq-supervisor" ||  $role == "hq-user") {
            $zone = "";
            $region = "";
            $district = "";
        } elseif ($role == "zonal-user" || $role == "zonal-supervisor" || $role == "zonal-manager") {
            $zone = $zone;
            $region = "";
            $district = "";
        } elseif ($role == "regional-user" || $role == "regional-supervisor" || $role == "regional-manager") {
            $zone = $zone;
            $region = $region;
            $district = "";
        } elseif ($role == "district-user" || $role == "district-supervisor" || $role == "district-manager") {
            $zone = $zone;
            $region = $region;
            $district = $district;
        } else {
            $zone = $zone;
            $region = $region;
            $district = $district;
        }

        $filters = [
            "From_Date" => $fromDate ?? date("01-m-Y"),
            "To_Date" => $toDate ?? date("t-m-Y"),
            "Zone_Name" => $zone,
            "Region_Name" => $region,
            "District_Name" => $district,
            "Service_Name" => $service,
            "Service_Status" => $status,
            "Service_Duration" => $duration,
        ];

        // die(print_r($params));
        $array = Yii::$app->soapclient->getServicesByDuration($filters);

        // die(print_r($array));
        $data = [];
        if (count($array) > 1) {
            foreach (Yii::$app->formatData->xml2array($array)["Service"] as $key => $value) {
                $data[] = Yii::$app->formatData->xml2array($value);
            }
        } elseif (count($array) == 1) {
            $value = Yii::$app->formatData->xml2arrayElements($array);
            $service = Json::encode(Yii::$app->formatData->xml2arrayElements($value)["Service"]);
            $data[] = Json::decode($service);
        }
        // die(print_r($data));

        $dataProvider = new ArrayDataProvider([
            "allModels" => $data,
            "pagination" => [
                "pageSize" => 500,
            ],
            "sort" => [
                "attributes" => [
                    "Service_No",
                    "Service_Name",
                    "Service_Problem",
                    "Service_Status",
                    "Service_Date",
                    "Service_Duration",
                    "Customer_Name",
                    "Mobile_No",
                ],
                "defaultOrder" => [
                    "Service_Duration" => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }
}
