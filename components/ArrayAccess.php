<?php
/**
 * Created by Engr. Kobello
 * Date: 19-June-2019
 * Time: 10:20
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\Inflector;
use yii\filters\AccessControl;
use app\commands\rbac\rules\AccessRule;

/**
 * Description of Traits
 */
class ArrayAccess extends Component
{
    private $model;

    /**
     * Initializing Component
     * @return component
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return array accessible actions
     */
    public function getArrayAccess($post, $only, $uid, $model=null)
    {
        // die(print_r($model));
        $this->model = $model;
        return [
            'class' => AccessControl::className(),
            // We will override the default rule config with the new AccessRule class
            'only' => $only,
            'ruleConfig' => [
                'class' => AccessRule::className(),
            ],
            'rules' => [
                [
                    'actions' => ['error'],
                    'allow' => true,
                ],
                [
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        $module      = Yii::$app->controller->module->id;
                        $action      = Yii::$app->controller->action->id;
                        $controller  = Inflector::singularize(Yii::$app->controller->id);

                        $route       = "$controller.$action";

                        if (Yii::$app->user->can($route, ['user' => $this->model])) {
                            return true;
                        }
                    }
                ],
            ],
        ];
    }
}
