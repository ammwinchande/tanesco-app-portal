<?php
/**
 * Created by Engr. Kobello
 * Date: 19-June-2019
 * Time: 10:18
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\Inflector;
use yii\filters\VerbFilter;

/**
 * Description of Traits
 */
class ActionBehaviors extends Component
{

    /**
     * Initializing Component
     * @return component
     */
    public function init()
    {
        parent::init();
    }

    public function getList($behave)
    {
        return [
            'class' => VerbFilter::className(),
            'actions' => $behave,
        ];
    }

    public function getBehaviors1($behave)
    {
        return [
            'class' => VerbFilter::className(),
            'actions' => [
                'delete' => ['post'],
                'delete-multiple' => ['post'],
            ],
        ];
    }
}
