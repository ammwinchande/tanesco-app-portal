<?php

use app\models\User;
use app\models\Dictionary;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DictionarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dictionary');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'dictionary';
?>
<div class="dictionary-index">
    <?php
        $cols = [
            'en:ntext',
            'sw:ntext',
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'created_by',
                'filter' => User::getUsernames(Dictionary::find()->select(['created_by'])),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Select Creator'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'width' => ''
                    ],
                ],
                'format' => 'raw',
                'value' => 'users.username'
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ],
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'updated_at',
                'format' => 'datetime',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ],
            ],
        ];
        echo $this->render(
            '../common/actions.php',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'title' => $this->title,
                'date' => $date,
                'cols' => $cols,
                'id' => $id
            ]
        );
    ?>
</div>