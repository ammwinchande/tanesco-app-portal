<?php

use app\models\User;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'user';
?>
<div class="user-index">
    <?php
    $cols = [
        'username',
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'email',
            'format' => 'email',
            'visible' => false,
        ],
        'designation',
        'coyno',
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'role',
            'filter' => User::getRoleItems(),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Role'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => ''
                ],
            ],
            'format' => 'raw',
            'value' => 'roles.item_name',
        ],
        // 'auth_key',
        [
            'class' => 'kartik\grid\BooleanColumn',
            'attribute' => 'status',
            'vAlign' => 'middle',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'created_at',
            'format' => 'datetime',
            'filterType' => GridView::FILTER_DATE,
            'filterWidgetOptions' => [
                'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ],
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'updated_at',
            'format' => 'datetime',
            'filterType' => GridView::FILTER_DATE,
            'filterWidgetOptions' => [
                'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ],
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'created_by',
            'filter' => User::getUsernames(User::find()->select(['created_by'])),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Creator'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => ''
                ],
            ],
            'format' => 'raw',
            'value' => 'users.username'
        ]
    ];
    echo $this->render(
        '../common/actions.php',
        [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'title' => $this->title,
            'date' => $date,
            'cols' => $cols,
            'id' => $id
        ]
    );
    ?>
</div>