<?php

use yii\helpers\Html;
use app\models\LocArea;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$fullName = explode('.', $model->username);
$this->title = ucfirst($fullName[0]) . ' ' . ucfirst($fullName[1]);
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Users'), 'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
$id = 'user';
?>
<div class="user-view no-print">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render(
        '../common/top-buttons.php',
        ['id' => $id, 'model' => $model]
    ) ?>

    <?= DetailView::widget(
        [
            'model' => $model,
            'attributes' => [
                'id',
                'username',
                'email:email',
                'designation',
                'coyno',
                [
                    'format' => 'raw',
                    'attribute' => 'status',
                    'value' => $model->status ?
                        "<span class='text-success'>Active</span>" :
                        "<span class='text-danger'>Inactive</span>",
                ],
                [
                    'attribute' => 'zone_id',
                    'value' => ucwords((LocArea::getZone($model->zone_id))->name),
                ],
                [
                    'attribute' => 'region_id',
                    'value' => ucwords((LocArea::getRegion($model->region_id))->name),
                ],
                [
                    'attribute' => 'district_id',
                    'value' => ucwords((LocArea::getDistrict($model->district_id))->name),
                ],
                [
                    'attribute' => 'created_by',
                    'value' => $model->getName($model->created_by),
                ],
                'created_at:datetime',
                'updated_at:datetime',
            ]
        ]
    ); ?>

</div>

<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>