<?php

use yii\helpers\Html;
use app\models\LocArea;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$fullName = explode('.', $model->username);
$this->title = ucfirst($fullName[0]) . ' ' . ucfirst($fullName[1]);
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Users'), 'url' => ['index']
];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'My Profile')];
$id = 'user';
?>
<div class="user-profile no-print">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
        DetailView::widget(
            [
                'model' => $model,
                'attributes' => [
                    'coyno',
                    'username',
                    'email:email',
                    'designation',
                    [
                        'attribute' => 'zone_id',
                        'value' => function ($model) {
                            return $model->zone_id ?
                                ucwords(
                                    (LocArea::getZone($model->zone_id))->name
                                ) : '-';
                        },
                    ],
                    [
                        'attribute' => 'region_id',
                        'value' => function ($model) {
                            return $model->region_id ?
                                ucwords(
                                    (LocArea::getRegion($model->region_id))->name
                                ) : '-';
                        },
                    ],
                    [
                        'attribute' => 'district_id',
                        'value' => function ($model) {
                            return $model->district_id ?
                                ucwords(
                                    (LocArea::getDistrict($model->district_id))->name
                                ) : '-';
                        },
                    ],
                    [
                        'format' => 'raw',
                        'attribute' => 'status',
                        'value' => $model->status ?
                            "<span class='text-success'>Active</span>" :
                            "<span class='text-danger'>Inactive</span>",
                    ],
                    [
                        'attribute' => 'created_by',
                        'value' => $model->getName($model->id),
                    ],
                    'created_at:datetime',
                    'updated_at:datetime',
                ]
            ]
        );
    ?>
</div>

<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>