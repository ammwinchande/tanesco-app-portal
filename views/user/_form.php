<?php

use app\models\Zone;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\LocArea;
use app\models\AuthItem;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-form">
    <?php
    if (empty($model) || $model->isNewRecord) {
        $model->role = null;
        $fname = $lname = $role = '';
        $regionId = $districtId = null;
        $regionData = $districtData = [];
    } else {
        $role = $model->roles->item_name;
        $fname = explode('.', $model->username)[0];
        $lname = explode('.', $model->username)[1];
        $district = empty($model->district_id) ?
            [] : LocArea::getDistrict($model->district_id);
        $region = empty($model->region_id) ?
            [] : LocArea::getRegion($model->region_id);

        $districtId = !empty($district) ? $district->id : null; //32
        $regionId = !empty($region) ? $region->id : null; //7
        $districtData = empty($district) ? [] : [
            $district->id => $district->name,
        ];
        $regionData = empty($district) ? [] : [
            $region->id => $region->name,
        ];
    }
    $url = Url::to(['/user/depdrop']);
    $regionUrl = Url::to(['/user/region']);
    $districtUrl = Url::to(['/user/district']);
    $form = ActiveForm::begin(
        [
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'options' => ['enctype' => 'multipart/form-data']
        ]
    ); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'first_name')
                ->textInput(
                    [
                        'id' => 'fname',
                        'value' => $fname,
                        'maxlength' => true,
                        'style' => 'text-transform:capitalize'
                    ]
                ) ?>
            <?= $form->field($model, 'last_name')
                ->textInput(
                    [
                        'id' => 'lname',
                        'value' => $lname,
                        'maxlength' => true,
                        'style' => 'text-transform:capitalize'
                    ]
                ) ?>
            <?= $form->field($model, 'username')
                ->textInput(
                    [
                        'maxlength' => true, 'readOnly' => false, 'id' => 'username'
                    ]
                ) ?>
            <?= $form->field($model, 'designation')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'zone_id')->widget(
                Select2::classname(),
                [
                    'data' => ArrayHelper::map(
                        Zone::find()->select(['id', 'name'])->all(),
                        'id',
                        'name'
                    ),
                    'theme' => Select2::THEME_KRAJEE,
                    'size' => Select2::MEDIUM,
                    'language' => 'en',
                    'options' => [
                        'id' => 'zone',
                        'placeholder' => 'Select Zone..',
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]
            ); ?>
            <?= $form->field($model, 'region_id')->widget(
                DepDrop::classname(),
                [
                    'data' => $regionData,
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => ['id' => 'region'],
                    'pluginOptions' => [
                        'url' => $regionUrl,
                        'depends' => ['zone'],
                        'loadingText' => 'Loading Regions..',
                    ],
                    'select2Options' => [
                        'pluginOptions' => [
                            'width' => '100%',
                            'size' => Select2::MEDIUM,
                        ]
                    ],
                ]
            ); ?>
            <?= $form->field($model, 'district_id')->widget(
                DepDrop::classname(),
                [
                    'data' => $districtData,
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => ['id' => 'district'],
                    'pluginOptions' => [
                        'initialize' => true,
                        'url' => $districtUrl,
                        'depends' => ['region'],
                        'initDepends' => ['zone'],
                        'loadingText' => 'Loading Districts..'
                    ],
                    'select2Options' => [
                        'pluginOptions' => [
                            'width' => '100%',
                            'size' => Select2::MEDIUM,
                        ]
                    ]
                ]
            ); ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'coyno')
                ->textInput(['maxlength' => true, 'style' => 'text-transform:capitalize']) ?>
            <?=
                $form->field($model, 'role')->widget(
                    Select2::classname(),
                    [
                        'data' => ArrayHelper::map(
                            AuthItem::find()->select(['name', 'name'])
                                ->where(['type' => 1])->all(),
                            'name',
                            'name'
                        ),
                        'theme' => Select2::THEME_KRAJEE,
                        'size' => Select2::MEDIUM,
                        'language' => 'en',
                        'options' => [
                            'id' => 'role',
                            'class' => 'form-control',
                            'placeholder' => 'Select User Role..',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]
                );
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?= Html::submitButton(
                $model->isNewRecord ? Yii::t('app', 'Create') :
                    Yii::t('app', 'Update'),
                [
                    'class' => $model->isNewRecord ?
                        'btn btn-success' : 'btn btn-primary'
                ]
            ) ?>
            <?= Html::resetButton(
                Yii::t('app', 'Reset'),
                [
                    'id' => 'resetButton',
                    'class' => 'btn btn-default'
                ]
            ) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php $script = <<< JS
// JS Code
    var timerid = 0;
    $(function(e) {
        $('#role').val('$role').trigger('change');
        $("#username").prop("readonly", true);
        $("#fname").on("input", function(e) {
            var fName = $(this).val().toLowerCase();
            var lName = $("#lname").val().toLowerCase();
            if ($(this).data("lastval") != fName) {
                $(this).data("lastval", fName);
                clearTimeout(timerid);
                timerid = setTimeout(function(e) {
                    //Change action
                    if(fName && !lName){
                        $("#username").css("color","red").val(fName);
                    }else if(!fName && lName){
                        $("#username").css("color","red").val(lName);
                    }else{
                        $("#username").css("color","green").val(fName + "." + lName);
                    }
                }, 500);
                $("#lname").prop("readonly", false);
            };
        });
        $("#lname").on("input", function(e) {
            var fName = $("#fname").val().toLowerCase();
            var lName = $(this).val().toLowerCase();
            if ($(this).data("lastval") != lName) {

                $(this).data("lastval", lName);
                clearTimeout(timerid);

                timerid = setTimeout(function(e) {
                    //Change action
                    if(fName && !lName){
                        $("#username").css("color","red").val(fName);
                    }else if(!fName && lName){
                        $("#username").css("color","red").val(lName);
                    }else{
                        $("#username").css("color","green").val(fName + "." + lName);
                    }
                }, 500);
            };
        });
        $("#resetButton").on("click", function(e) {
            $("#zone").val(null).trigger('change');
            $("#region").val(null).trigger('change');
            $("#district").val(null).trigger('change');
        });
    });
JS;
$this->registerJs($script);
?>