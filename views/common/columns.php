<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

$columns = [];
    $columns[] = [
        'class' => 'kartik\grid\SerialColumn',
        'order' => DynaGrid::ORDER_FIX_LEFT
    ];
    foreach ($cols as $key => $col) {
        $columns[] = $col;
    }

    $templatesButton = '';

    if ($id <> 'registration-code') {
        if (Yii::$app->user->can($id . '.view')) {
            $templatesButton .= '{view} ';
        }
        if (Yii::$app->user->can($id . '.update')) {
            $templatesButton .= '{update} ';
        }
        if (Yii::$app->user->can($id . '.delete')) {
            $templatesButton .= '{delete} ';
        }
        if (Yii::$app->user->can($id . '.resend-to-sdm')) {
            $templatesButton .= '{resend-to-sdm}';
        }
    }

    if ($id <> 'site') {
        $columns[] = [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'vAlign' => 'middle',
            'template' => $templatesButton,
            'buttons' => $id <> 'service-report' ? [] : [
                'resend-to-sdm' => function ($url, $model) {
                    if ($model->send_to_sdm <> 1) {
                        return Html::a(
                            '<span class="fa fa-share"></span>',
                            $url,
                            [
                                'title' => Yii::t('app', 'Resend to SDM'),
                            ]
                        );
                    }
                }
            ],
        ];
    }

    if ($id == 'service' || $id == 'sub-service' || $id == 'menu-panel'
        || $id == 'user' || $id == 'session' || $id == 'login-history'
        || $id == 'log' || $id == 'department' || $id == 'client'
    ) {
        $columns[] = [
            'class' => '\kartik\grid\CheckboxColumn',
            'checkboxOptions' => [
                'class' => 'simple'
            ],
            //'pageSummary' => true,
            'rowSelectedClass' => GridView::TYPE_SUCCESS,
        ];
    }

    echo $this->render(
        '../common/grid-view.php',
        [
            'templatesButton' => $templatesButton,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'toolbars' => $toolbars,
            'columns' => $columns,
            'panels' => $panels,
            'title' => $title,
            'date' => $date,
            'id' => $id
        ]
    );
