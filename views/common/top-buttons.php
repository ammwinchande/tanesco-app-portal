<?php
use yii\helpers\Html;

?>
<p>
    <?= \Yii::$app->user->can($id . '.create') ? Html::a(\Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-primary']) : '' ?>
    <?= \Yii::$app->user->can($id . '.update') ? Html::a(\Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-success']) : '' ?>
    <?= \Yii::$app->user->can($id . '.delete') ? Html::a(\Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) : '' ?>
    <?= Html::a(Yii::t('app', 'Print Qrcode'), ['print'], ['id' => 'print', 'class' => 'btn btn-secondary']) ?>
</p>