
<?php
use yii\helpers\Html;

$contents_create = Yii::$app->user->can($id . '.create') ?
    Html::a(
        '<i class="fa fa-plus"></i>',
        [$id . '/create'],
        [
            'type' => 'button',
            'id' => 'service_btn',
            'title' => 'Add ' . $title,
            'class' => 'btn btn-outline bg-teal-400 text-teal-400 ' .
            'border-teal-400 border-1'
        ]
    ) . ' ' : '';
$contents_delete_all = Yii::$app->user->can($id . '.delete-all') ?
    Html::button(
        '<i class="fa fa-trash"></i>',
        [
            'type' => 'button',
            'id' => 'deleteSelected',
            'title' => 'Delete Selected ' . $title,
            'class' => 'btn btn-danger border-1 border-warning-400'
        ]
    ) : '';

$url = $id == 'site' ? $id . '/reports' : $id . '/index';
$contents = $contents_create . $contents_delete_all .
    Html::a(
        '<i class="fa fa-history"></i>',
        [
            $url,
            // 'p_reset' => true
        ],
        [
            // 'data-pjax' => 0,
            'title' => 'Reset Grid',
            'class' => 'btn btn-outline-secondary'
        ]
    ) . ' ';
$toolbars = [
    ['content' => $contents],
    ['content' => '{dynagridFilter}{dynagridSort}{dynagrid}'],
    '{export}',
];

$panels = [
    'heading' => '<h3 class="panel-title"><i class="fa fa-user"></i>  ' .
        $title . '</h3>',
    'before' => '<div style="padding-top: 7px;"><em>* ' .
        'Use above options to filter & Search data then personalize & ' .
        'pull report using buttons on right <i class="fa fa-right-arrow"></i>' .
        '</em></div>',
];

echo $this->render(
    '../common/columns.php',
    [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'toolbars' => $toolbars,
        'panels' => $panels,
        'title' => $title,
        'date' => $date,
        'cols' => $cols,
        'id' => $id
    ]
);
?>