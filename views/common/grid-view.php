
<?php

use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

require __DIR__ . '/grid-export-config.php';

$gridOptions = [
    'showPageSummary' => true,
    'responsiveWrap' => false,
    'toolbar' => $toolbars,
    'floatHeader' => false,
    'panel' => $panels,
    'pjax' => true,
    'export' => [
        'fontAwesome' => true,
        'showConfirmAlert' => false,
        'target' => GridView::TARGET_SELF
    ],
    'exportConfig' => $exportConfig
];

$filterModelArray = is_null($searchModel) ? [
    'dataProvider' => $dataProvider,
] : [
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
];
$gridOptions = array_merge($filterModelArray, $gridOptions);

echo DynaGrid::widget(
    [
        'columns' => $columns,
        'theme' => 'panel-default',
        'allowFilterSetting' => true,
        'allowSortSetting' => true,
        'showPersonalize' => true,
        'showFilter' => true,
        'showSort' => true,
        //'maxPageSize' =>500,
        'storage' => 'db',
        'gridOptions' => $gridOptions,
        'options' => [
            'id' => 'dynagrid-services-' .
                Yii::$app->user->identity->id . '_' . strtotime('now')
        ],
    ]
);

$this->registerJs(
    '$("#deleteSelected").on("click",function(){
        var array = "";
        $(".simple").each(function(index){
            if($(this).prop("checked")){
                array += $(this).val()+",";
            }
        })
        if(array==""){
            alert("No data selected?");
        } else {
            if(window.confirm("Are You Sure to delete selected data?")){
                $.ajax({
                    type:"POST",
                    url:"' . Yii::$app->urlManager->createUrl([$id . '/delete-all'])
        . '",
                    data :{pk:array},
                    success:function(){
                        location.href="";
                    }
                });
            }
        }
    });'
);
?>