<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LoginHistory */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Login Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$id = 'login-history';
?>
<div class="login-history-view no-print">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('../common/top-buttons.php', ['id' => $id, 'model' => $model]) ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $all = User::find()->select(['username'])->where(['=', 'id', $model->user_id])->distinct()->one();
                    return  $all['username'];
                },
            ],
            'status',
            'signed_on',
            'signed_off',
        ]]);
    ?>
</div>
<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>