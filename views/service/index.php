<?php

use app\models\User;
use yii\helpers\Html;
use app\models\Service;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Services');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'service';
?>
<div class="service-index">
    <?php
        $cols = [
            [
                'class' => 'kartik\grid\DataColumn',
                'filter' => Service::getServices(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Select Sercive'],
                    'pluginOptions' => [
                        'style' => 'text-transform:capitalize',
                        'allowClear' => true,
                        'width' => '100%'
                    ],
                ],
                'contentOptions' => [
                    'class' => 'text-capitalize',
                ],
                'attribute' => 'name',
                'format' => 'raw',
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'created_by',
                'filter' => User::getUsernames(Service::find()->select(['created_by'])),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Select Creator'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'width' => ''
                    ],
                ],
                'format' => 'raw',
                'value' => 'users.username'
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ],
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'updated_at',
                'format' => 'datetime',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ],
            ],
            [
                'attribute' => 'Service Icon',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->service_icon != '') {
                        return Html::img(
                            '@web/uploads/services/' . $model->service_icon,
                            ['alt' => 'Service Icon', 'style' => 'width:50px; height:auto;']
                        );
                    } else {
                        return 'no icon';
                    }
                },
            ],
        ];
        echo $this->render(
            '../common/actions.php',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'title' => $this->title,
                'date' => $date,
                'cols' => $cols,
                'id' => $id
            ]
        );
    ?>
</div>
