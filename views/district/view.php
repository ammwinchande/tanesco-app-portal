<?php

use app\models\User;
use app\models\Zone;
use app\models\Region;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\District */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Districts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$id = 'district';
?>
<div class="district-view no-print">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('../common/top-buttons.php', ['id' => $id, 'model' => $model]) ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'dis_code',
            [
                'attribute' => 'region_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $region = Region::find()->select(['name'])->where(['=', 'id', $model->region_id])->distinct()->one();
                    return  $region['name'];
                },
            ],
            [
                'attribute' => 'zone_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $zone = Zone::find()->select(['name'])->where(['=', 'id', $model->zone_id])->distinct()->one();
                    return  $zone['name'];
                },
            ],
            [
                'attribute' => 'created_by',
                'format' => 'raw',
                'value' => function ($model) {
                    $all = User::find()->select(['username'])->where(['=', 'id', $model->created_by])->distinct()->one();
                    return  $all['username'];
                },
            ],
            'created_at',
            'updated_at',
        ]]);
    ?>
</div>
<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>