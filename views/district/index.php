<?php

use app\models\User;
use app\models\Zone;
use app\models\Region;
use app\models\District;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DistrictSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Districts');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'district';
?>
<div class="district-index">
    <?php
        $cols = [
            [
                'class' => 'kartik\grid\DataColumn',
                'filter' => District::getDistricts(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Select District'],
                    'pluginOptions' => [
                        'style' => 'text-transform:capitalize',
                        'allowClear' => true,
                        'width' => '100%'
                    ],
                ],
                'contentOptions' => [
                    'class' => 'text-capitalize',
                ],
                'attribute' => 'name',
                'format' => 'raw',
            ],
            'dis_code',
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'region_id',
                'format' => 'raw',
                'value' => 'regions.name',
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'zone_id',
                'format' => 'raw',
                'value' => 'zones.name',
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'created_by',
                'filter' => User::getUsernames(District::find()->select(['created_by'])),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Select Creator'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'width' => ''
                    ],
                ],
                'format' => 'raw',
                'value' => 'users.username',
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ],
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'updated_at',
                'format' => 'datetime',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ],
            ],
        ];
        echo $this->render(
            '../common/actions.php',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'title' => $this->title,
                'date' => $date,
                'cols' => $cols,
                'id' => $id
            ]
        );
    ?>
</div>