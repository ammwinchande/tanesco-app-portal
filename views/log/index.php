<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Logs');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'log';
?>
<div class="log-index">
    <?php
    $cols = [
        'level',
        'category',
        'log_time',
        'prefix:ntext',
        'message:ntext',
    ];
    echo $this->render(
        '../common/actions.php',
        [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'title' => $this->title,
            'date' => $date,
            'cols' => $cols,
            'id' => $id
        ]
    );
    ?>
</div>