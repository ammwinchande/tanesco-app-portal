<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Log */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$id = 'log-view';
?>
<div class="log-view no-print">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('../common/top-buttons.php', ['id' => $id, 'model' => $model]) ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'level',
            'category',
            [
                'attribute' => 'created_by',
                'format' => 'raw',
                'value' => function ($model) {
                    return  date('Y-m-d H:i:s', $model->log_time);
                },
            ],
            'prefix:ntext',
            'message:ntext',
        ]
    ]);
    ?>
</div>
<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>