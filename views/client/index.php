<?php

use app\models\Client;
use app\models\District;
use app\models\LocArea;
use app\models\LocStreet;
use app\models\Region;
use kartik\grid\GridView;
use yii\helpers\Html;

// use kartik\grid\GridViewAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Clients');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'client';
?>

<div class="client-index">
    <?php
    $cols = [
        'meter_no',
        'mobile_no',
        [
            'class' => 'kartik\grid\DataColumn',
            'filter' => Client::getClients(),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Client'],
                'pluginOptions' => [
                    'style' => 'text-transform:capitalize',
                    'allowClear' => true,
                    'width' => '100%'
                ],
            ],
            'contentOptions' => [
                'class' => 'text-capitalize',
            ],
            'attribute' => 'name',
            'format' => 'raw',
        ],
        'device_id',
        [
            'class' => 'kartik\grid\BooleanColumn',
            'attribute' => 'conn_status',
            'vAlign' => 'middle',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'filter' => Region::getRegions(),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Region'],
                'pluginOptions' => [
                    'style' => 'text-transform:capitalize',
                    'allowClear' => true,
                    'width' => '100%'
                ],
            ],
            'contentOptions' => [
                'class' => 'text-capitalize',
            ],
            'attribute' => 'region_id',
            'format' => 'raw',
            'value' => 'regions.name',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'filter' => District::getDistricts(),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select District'],
                'pluginOptions' => [
                    'style' => 'text-transform:capitalize',
                    'allowClear' => true,
                    'width' => '100%'
                ],
            ],
            'contentOptions' => [
                'class' => 'text-capitalize',
            ],
            'attribute' => 'district_id',
            'format' => 'raw',
            'value' => 'districts.name',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'filter' => LocArea::getAreas(),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Area'],
                'pluginOptions' => [
                    'style' => 'text-transform:capitalize',
                    'allowClear' => true,
                    'width' => '100%'
                ],
            ],
            'contentOptions' => [
                'class' => 'text-capitalize',
            ],
            'attribute' => 'area_id',
            'format' => 'raw',
            'value' => 'areas.name',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'filter' => LocStreet::getStreets(),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Street'],
                'pluginOptions' => [
                    'style' => 'text-transform:capitalize',
                    'allowClear' => true,
                    'width' => '100%'
                ],
            ],
            'contentOptions' => [
                'class' => 'text-capitalize',
            ],
            'attribute' => 'street_id',
            'format' => 'raw',
            'value' => 'streets.name',
        ],
        'special_mark',
        [
            'class' => 'kartik\grid\BooleanColumn',
            'attribute' => 'is_blocked',
            'vAlign' => 'middle',
            'trueLabel' => 'Yes',
            'falseLabel' => 'No',
        ],
        [
            'class' => 'kartik\grid\BooleanColumn',
            'attribute' => 'is_notified',
            'vAlign' => 'middle',
            'trueLabel' => 'Yes',
            'falseLabel' => 'No',
        ],
        [
            'class' => 'kartik\grid\BooleanColumn',
            'attribute' => 'is_subscribed',
            'vAlign' => 'middle',
            'trueLabel' => 'Yes',
            'falseLabel' => 'No',
        ],
        'lang_used',
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'created_at',
            'format' => 'datetime',
            'filterType' => GridView::FILTER_DATE,
            'filterWidgetOptions' => [
                'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ],
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'updated_at',
            'format' => 'datetime',
            'filterType' => GridView::FILTER_DATE,
            'filterWidgetOptions' => [
                'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ],
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'deleted_at',
            'format' => 'datetime',
            'filterType' => GridView::FILTER_DATE,
            'filterWidgetOptions' => [
                'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ],
        ],
    ];
    echo $this->render(
        '../common/actions.php',
        [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'title' => $this->title,
            'date' => $date,
            'cols' => $cols,
            'id' => $id
        ]
    );
    ?>
</div>
