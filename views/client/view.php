<?php

use app\models\District;
use app\models\LocArea;
use app\models\LocStreet;
use app\models\Region;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$id = 'client';
?>

<div class="client-view no-print">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('../common/top-buttons.php', ['id' => $id, 'model' => $model]) ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'meter_no',
            'mobile_no',
            'name',
            'device_id',

            [
                'attribute' => 'region_id',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'text-capitalize',
                ],
                'value' => function ($model) {
                    $region = Region::find()->select(['name'])->where(['=', 'id', $model->region_id])->distinct()->one();
                    return  $region['name'];
                },
            ],
            [
                'attribute' => 'district_id',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'text-capitalize',
                ],
                'value' => function ($model) {
                    $district = District::find()->select(['name'])->where(['=', 'id', $model->district_id])->distinct()->one();
                    return  $district['name'];
                },
            ],
            [
                'attribute' => 'area_id',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'text-capitalize',
                ],
                'value' => function ($model) {
                    $area = LocArea::find()->select(['name'])->where(['=', 'id', $model->area_id])->distinct()->one();
                    return  $area['name'];
                },
            ],
            [
                'attribute' => 'street_id',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'text-capitalize',
                ],
                'value' => function ($model) {
                    $street = LocStreet::find()->select(['name'])->where(['=', 'id', $model->street_id])->distinct()->one();
                    return  $street['name'];
                },
            ],
            [
                'attribute' => 'conn_status',
                'format' => 'raw',
                'value' => $model->conn_status ? "<span class='text-success'><i class='fa fa-check'></i></span>" : "<span class='text-danger'><i class='fa fa-times'></i></span>",
            ],
            [
                'attribute' => 'is_blocked',
                'format' => 'raw',
                'value' => $model->is_blocked ? "<span class='text-success'><i class='fa fa-check'></i></span>" : "<span class='text-danger'><i class='fa fa-times'></i></span>",
            ],
            [
                'attribute' => 'is_notified',
                'format' => 'raw',
                'value' => $model->is_notified ? "<span class='text-success'><i class='fa fa-check'></i></span>" : "<span class='text-danger'><i class='fa fa-times'></i></span>",
            ],
            [
                'attribute' => 'is_subscribed',
                'format' => 'raw',
                'value' => $model->is_subscribed ? "<span class='text-success'><i class='fa fa-check '></i></span>" : "<span class='text-danger'><i class='fa fa-times'></i></span>",
            ],
            'lang_used',
            'created_at',
            'updated_at',
            'deleted_at',
        ]]);
    ?>

</div>

<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>
