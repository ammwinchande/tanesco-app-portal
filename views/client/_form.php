<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'options' => ['enctype' => 'multipart/form-data']   // important, needed for file upload
    ]); ?>


    <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, 'mobile_no')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'device_id')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'is_blocked')->textInput() ?>

            <?= $form->field($model, 'is_notified')->textInput() ?>

            <?= $form->field($model, 'is_subscribed')->textInput() ?>

            <?= $form->field($model, 'created_at')->textInput() ?>

            <?= $form->field($model, 'deleted_at')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'meter_no')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'conn_status')->textInput() ?>

            <?= $form->field($model, 'region_id')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'area_id')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'street_id')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'special_mark')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'lang_used')->dropDownList(['English' => 'English', 'Swahili' => 'Swahili',], ['prompt' => '']) ?>

            <?= $form->field($model, 'updated_at')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
