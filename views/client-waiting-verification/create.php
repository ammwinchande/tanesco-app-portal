<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientWaitingVerification */

$this->title = Yii::t('app', 'Create Client Waiting Verification');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Client Waiting Verifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-waiting-verification-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
