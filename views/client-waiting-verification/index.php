<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientWaitingVerificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Client Waiting Verifications');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-waiting-verification-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Client Waiting Verification'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'meter_no',
            'mobile_no',
            'name',
            'device_id',
            //'conn_status',
            //'region_id',
            //'district_id',
            //'area_id',
            //'street_id',
            //'special_mark',
            //'is_blocked',
            //'is_notified',
            //'is_subscribed',
            //'lang_used',
            //'created_at',
            //'session_start_date',
            //'session_end_date',
            //'updated_at',
            //'deleted_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
