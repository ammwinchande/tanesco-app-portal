<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ClientWaitingVerification */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Client Waiting Verifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-waiting-verification-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'meter_no',
            'mobile_no',
            'name',
            'device_id',
            'conn_status',
            'region_id',
            'district_id',
            'area_id',
            'street_id',
            'special_mark',
            'is_blocked',
            'is_notified',
            'is_subscribed',
            'lang_used',
            'created_at',
            'session_start_date',
            'session_end_date',
            'updated_at',
            'deleted_at',
        ],
    ]) ?>

</div>
