<?php

use app\models\User;
use app\models\Zone;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ZoneSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zones');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'zone';
?>
<div class="zone-index">
    <?php
    $cols = [
        [
            'class' => 'kartik\grid\DataColumn',
            'filter' => Zone::getZones(),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Zone'],
                'pluginOptions' => [
                    'style' => 'text-transform:capitalize',
                    'allowClear' => true,
                    'width' => '100%'
                ],
            ],
            'contentOptions' => [
                'class' => 'text-capitalize',
            ],
            'attribute' => 'name',
            'format' => 'raw',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'created_by',
            'filter' => User::getUsernames(Zone::find()->select(['created_by'])),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Creator'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => '100%'
                ],
            ],
            'format' => 'raw',
            'value' => 'users.username'
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'created_at',
            'format' => 'datetime',
            'filterType' => GridView::FILTER_DATE,
            'filterWidgetOptions' => [
                'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ],
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'updated_at',
            'format' => 'datetime',
            'filterType' => GridView::FILTER_DATE,
            'filterWidgetOptions' => [
                'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ],
        ],
    ];
    echo $this->render(
        '../common/actions.php',
        [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'title' => $this->title,
            'date' => $date,
            'cols' => $cols,
            'id' => $id
        ]
    );
    ?>
</div>