<?php

use app\models\User;
use app\models\Session;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SessionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sessions');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'session';
?>
<div class="session-index">
    <?php
        $cols = [
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'user_id',
                'filter' => User::getUsernames(Session::find()->select(['user_id'])),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Select Creator'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'width' => ''
                    ],
                ],
                'format' => 'raw',
                'value' => 'users.username'
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'expire',
                'format' => 'datetime',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ],
            ],
            'data',
        ];
        echo $this->render(
            '../common/actions.php',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'title' => $this->title,
                'date' => $date,
                'cols' => $cols,
                'id' => $id
            ]
        );
    ?>
</div>