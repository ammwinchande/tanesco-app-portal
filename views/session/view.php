<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Session */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sessions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$id = 'session';
?>
<div class="session-view no-print">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('../common/top-buttons.php', ['id' => $id, 'model' => $model]) ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'expire',
            'data',
        ]
    ]);
    ?>
</div>
<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>