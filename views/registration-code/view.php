<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RegistrationCode */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Registration Codes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$id = 'registration-code';
?>
<div class="registration-code-view no-print">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('../common/top-buttons.php', ['id' => $id, 'model' => $model]) ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'mobile_no',
            'reg_code',
        ]
    ]);
    ?>
</div>
<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>