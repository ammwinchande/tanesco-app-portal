<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RegistrationCode */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Registration Code',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Registration Codes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registration-code-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?=
        $this->render('_form', [
            'model' => $model,
        ])
    ?>
</div>
