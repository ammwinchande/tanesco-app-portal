<?php

use kartik\helpers\Html;
use yii\bootstrap4\Modal;
use kartik\select2\Select2;
use kartik\daterange\DateRangePicker;

$table_data = [
    'days' => 'Day Options',
    'yesterday' => 'Yesterday',
    'today' => 'Today'
];
$table_selectData = array_merge($table_data, $selectData);
$table_select2 = Select2::widget(
    [
        'value' => 'month',
        'name' => 'tables-date-selector',
        'data' => $table_selectData,
        'size' => Select2::MEDIUM,
        'options' => [
            'placeholder' => 'Select Date Range.. ',
            'id' => 'tables-date-selector',
            'options' => [
                'days' => ['disabled' => true],
                'weeks' => ['disabled' => true],
                'months' => ['disabled' => true],
                'quarters' => ['disabled' => true],
                'years' => ['disabled' => true]
            ]
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '100%'
        ]
    ]
);
$table_daterange = DateRangePicker::widget(
    [
        'name' => 'tables-daterange-selector',
        'presetDropdown' => true,
        'hideInput' => true
    ]
);
Modal::begin(
    [
        'options' => [
            'id' => 'table-daterange-modal',
            'tabindex' => false
        ],
        'title' => 'Click to Filter Data using Date Range Picker..',
    ]
);
echo $table_select2;
// echo $table_daterange;
Modal::end();

echo Html::tag('hr', '') .
    Html::tag(
        'div',
        Html::tag(
            'div',
            Html::button(
                '<i class="fas fa-calendar-alt"></i>  Filter Data',
                [
                    'type' => 'button',
                    'data-toggle' => 'modal',
                    'data-target' => '#table-daterange-modal',
                    'class' => 'btn btn-outline-secondary btn-md',
                    'title' => 'Filter Data with Date Range Picker'
                ]
            ),
            [
                'style' => 'padding-bottom: 2px;',
                'class' => 'col-2 text-left'
            ]
        ) .
            Html::tag(
                'div',
                Html::tag(
                    'h5',
                    'SERVICES AGAINST RESPONSE TIME FOR ' .
                        Html::tag(
                            'span',
                            '',
                            [
                                'class' => 'services-duration'
                            ]
                        ),
                    [
                        'class' => 'text-center'
                    ]
                ),
                [
                    'class' => 'col-10'
                ]
            ),
        [
            'class' => 'row'
        ]
    );

$table_holder = [
    'pending_' => 'PENDING SERVICES FOR ',
    'resolved_' => 'RESOLVED SERVICES FOR '
];
$table_holder_items = [
    'EMERGENCIES' => [
        'emergency_lteq_1',
        'emergency_lteq_2',
        'emergency_lteq_4',
        'emergency_gt_4',
        'emergency_total',
    ],
    'COMPLAINTS' => [
        'complaints_lteq_1',
        'complaints_lteq_2',
        'complaints_lteq_4',
        'complaints_gt_4',
        'complaints_total'
    ],
    'ENQUIRIES' => [
        'enquiries_lteq_1',
        'enquiries_lteq_2',
        'enquiries_lteq_4',
        'enquiries_gt_4',
        'enquiries_total'
    ],
    'REQUESTS' => [
        'requests_lteq_1',
        'requests_lteq_2',
        'requests_lteq_4',
        'requests_gt_4',
        'requests_total'
    ],
];

$table_card = '';
foreach ($table_holder as $id_key => $title) {
    $bg_color = $id_key == 'pending_' ?
        'background-color: #ffad33; ' :
        'background-color: #006600; ';
    $table_style = $bg_color . 'font-size: 1.0em; font-weight: bold;';
    $table_card_holder = '';
    foreach ($table_holder_items as $serviceName => $array_value) {
        $table_card_items = '';
        foreach ($array_value as $id) {
            $table_card_items .=
                Html::tag(
                    'td',
                    Html::tag(
                        'span',
                        '',
                        [
                            'style' => $table_style,
                            'class' => 'badge badge-pill',
                            'id' => $id_key . $id
                        ]
                    ),
                    [
                        'class' => 'text-center'
                    ]
                );
        }
        $table_card_holder .=
            Html::tag(
                'tr',
                Html::tag(
                    'th',
                    $serviceName,
                    [
                        'class' => '',
                        'scope' => 'row'
                    ]
                ) .
                    $table_card_items,
                [
                    'class' => '',
                ]
            );
    }
    $table_card .=
        Html::tag(
            'div',
            Html::tag(
                'div',
                Html::tag(
                    'table',
                    Html::tag(
                        'caption',
                        $title .
                            Html::tag(
                                'span',
                                '',
                                [
                                    'class' => 'services-duration'
                                ]
                            ),
                        [
                            'class' => 'text-center',
                            'style' => 'caption-side: top; color: #000000;'
                        ]
                    ) .
                        Html::tag(
                            'thead',
                            Html::tag(
                                'tr',
                                Html::tag(
                                    'th',
                                    'Service Name',
                                    [
                                        'style' => 'font-weight: bold;',
                                        'scope' => 'col'
                                    ]
                                ) .
                                    Html::tag(
                                        'th',
                                        '(0 - 1 Day)',
                                        [
                                            'style' => 'font-weight: bold;',
                                            'class' => 'text-center',
                                            'scope' => 'col'
                                        ]
                                    ) .
                                    Html::tag(
                                        'th',
                                        '(1 - 2 Days)',
                                        [
                                            'style' => 'font-weight: bold;',
                                            'class' => 'text-center',
                                            'scope' => 'col'
                                        ]
                                    ) .
                                    Html::tag(
                                        'th',
                                        '(2 - 4 Days)',
                                        [
                                            'style' => 'font-weight: bold;',
                                            'class' => 'text-center',
                                            'scope' => 'col'
                                        ]
                                    ) .
                                    Html::tag(
                                        'th',
                                        '( > 4 Days)',
                                        [
                                            'style' => 'font-weight: bold;',
                                            'class' => 'text-center',
                                            'scope' => 'col'
                                        ]
                                    ) .
                                    Html::tag(
                                        'th',
                                        'Total',
                                        [
                                            'style' => 'font-weight: bold;',
                                            'class' => 'text-center',
                                            'scope' => 'col'
                                        ]
                                    ),
                                [
                                    'class' => 'table table-hover'
                                ]
                            ),
                            [
                                'class' => 'thead-light'
                            ]
                        ) .
                        Html::tag(
                            'tbody',
                            $table_card_holder,
                            [
                                'class' => '',
                            ]
                        ),
                    [
                        'class' => 'table table-hover',
                        'style' => 'width: 98%; margin: 1%;'
                    ]
                ),
                [
                    'class' => 'card'
                ]
            ),
            [
                'class' => 'col'
            ]
        );
}
echo Html::tag(
    'div',
    $table_card,
    [
        'class' => 'row'
    ]
);
