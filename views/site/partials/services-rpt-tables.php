<?php

require __DIR__ . '/srpt-tables-page.php';

$scriptTables = <<< JS
$(function() {
    fetchServicesBasedOnDelays('month');
    $('#tables-date-selector').on('change', function(e){
        var txt = $(this).find("option:selected").text();
        var val = $(this).find("option:selected").val();
        val = val ? val : 'month';
        console.log('Delays 2: ' + val + ' => ' + txt);
        fetchServicesBasedOnDelays(val);
    });
    $('#tables-daterange-selector').on('change', function(e){
        var txt = $(this).find("option:selected").text();
        var val = $(this).find("option:selected").val();
        val = val ? val : 'month';
        console.log('Delays 2: ' + val + ' => ' + txt);
        fetchServicesBasedOnDelays(val);
    });
});

function fetchServicesBasedOnDelays(selectedRange) {
    var delaysDuration = durationFunc(selectedRange);
    $(".services-duration").html('<b>(' + delaysDuration.now + ')</b>');
    $.ajax({
        type: "POST",
        url: "$urlDelayTables",
        data: {
            fromDate: delaysDuration.fromDate,
            toDate: delaysDuration.toDate,
        },
        success: function (results) {
            populateDataToDelays($.parseJSON(results).Service, delaysDuration);
        },
        error: function (exception) {
            console.log(exception);
        }
    });
};

function populateDataToDelays(dataObject, delaysDuration) {
    console.log(dataObject);
    var waitingEmergencyTotal = waitingComplaintTotal =
        waitingEnquiryTotal = waitingRequestTotal = 0;
    var propgrssEmergencyTotal = propgrssComplaintTotal =
        propgrssEnquiryTotal = propgrssRequestTotal = 0;
    var referredEmergencyTotal = referredComplaintTotal =
        referredEnquiryTotal = referredRequestTotal = 0;
    var resolvedEmergencyTotal = resolvedComplaintTotal =
        resolvedEnquiryTotal = resolvedRequestTotal = 0;
    var waitingEmergencyLtEq1 = waitingEmergencyLtEq2 =
        waitingEmergencyLtEq4 = waitingEmergencyGt4 = 0;
    var propgrssEmergencyLtEq1 = propgrssEmergencyLtEq2 =
        propgrssEmergencyLtEq4 = propgrssEmergencyGt4 = 0;
    var referredEmergencyLtEq1 = referredEmergencyLtEq2 =
        referredEmergencyLtEq4 = referredEmergencyGt4 = 0;
    var resolvedEmergencyLtEq1 = resolvedEmergencyLtEq2 =
        resolvedEmergencyLtEq4 = resolvedEmergencyGt4 = 0;
    var waitingComplaintLtEq1 = waitingComplaintLtEq2 =
        waitingComplaintLtEq4 = waitingComplaintGt4 = 0;
    var propgrssComplaintLtEq1 = propgrssComplaintLtEq2 =
        propgrssComplaintLtEq4 = propgrssComplaintGt4 = 0;
    var referredComplaintLtEq1 = referredComplaintLtEq2 =
        referredComplaintLtEq4 = referredComplaintGt4 = 0;
    var resolvedComplaintLtEq1 = resolvedComplaintLtEq2 =
        resolvedComplaintLtEq4 = resolvedComplaintGt4 = 0;
    var waitingEnquiryLtEq1 = waitingEnquiryLtEq2 =
        waitingEnquiryLtEq4 = waitingEnquiryGt4 = 0;
    var propgrssEnquiryLtEq1 = propgrssEnquiryLtEq2 =
        propgrssEnquiryLtEq4 = propgrssEnquiryGt4 = 0;
    var referredEnquiryLtEq1 = referredEnquiryLtEq2 =
        referredEnquiryLtEq4 = referredEnquiryGt4 = 0;
    var resolvedEnquiryLtEq1 = resolvedEnquiryLtEq2 =
        resolvedEnquiryLtEq4 = resolvedEnquiryGt4 = 0;
    var waitingRequestLtEq1 = waitingRequestLtEq2 =
        waitingRequestLtEq4 = waitingRequestGt4 = 0;
    var propgrssRequestLtEq1 = propgrssRequestLtEq2 =
        propgrssRequestLtEq4 = propgrssRequestGt4 = 0;
    var referredRequestLtEq1 = referredRequestLtEq2 =
        referredRequestLtEq4 = referredRequestGt4 = 0;
    var resolvedRequestLtEq1 = resolvedRequestLtEq2 =
        resolvedRequestLtEq4 = resolvedRequestGt4 = 0;

    $.each( dataObject, function( key, value ) {
    if (this.Service_Name == "EMERGENCY") {
        if (this.Service_Status == "WAITING") {
        waitingEmergencyTotal ++;
        this.Service_Duration <= 1 ? waitingEmergencyLtEq1 ++ :
            this.Service_Duration <= 2 ? waitingEmergencyLtEq2 ++ :
                this.Service_Duration <= 4 ? waitingEmergencyLtEq4 ++ :
                    waitingEmergencyGt4 ++;
        } else if (this.Service_Status == "IN PROGRESS") {
        propgrssEmergencyTotal ++;
        this.Service_Duration <= 1 ? propgrssEmergencyLtEq1 ++ :
            this.Service_Duration <= 2 ? propgrssEmergencyLtEq2 ++ :
                this.Service_Duration <= 4 ? propgrssEmergencyLtEq4 ++ :
                    propgrssEmergencyGt4 ++;
        } else if (this.Service_Status == "REFERRED") {
        referredEmergencyTotal ++;
        this.Service_Duration <= 1 ? referredEmergencyLtEq1 ++ :
            this.Service_Duration <= 2 ? referredEmergencyLtEq2 ++ :
                this.Service_Duration <= 4 ? referredEmergencyLtEq4 ++ :
                    referredEmergencyGt4 ++;
        } else if (this.Service_Status == "RESOLVED") {
        resolvedEmergencyTotal ++;
        this.Service_Duration <= 1 ? resolvedEmergencyLtEq1 ++ :
            this.Service_Duration <= 2 ? resolvedEmergencyLtEq2 ++ :
                this.Service_Duration <= 4 ? resolvedEmergencyLtEq4 ++ :
                    resolvedEmergencyGt4 ++;
        }
    } else if (this.Service_Name == "COMPLAINT") {
        if (this.Service_Status == "WAITING") {
        waitingComplaintTotal ++;
        this.Service_Duration <= 1 ? waitingComplaintLtEq1 ++ :
            this.Service_Duration <= 2 ? waitingComplaintLtEq2 ++ :
                this.Service_Duration <= 4 ? waitingComplaintLtEq4 ++ :
                    waitingComplaintGt4 ++;
        } else if (this.Service_Status == "IN PROGRESS") {
        propgrssComplaintTotal ++;
        this.Service_Duration <= 1 ? propgrssComplaintLtEq1 ++ :
            this.Service_Duration <= 2 ? propgrssComplaintLtEq2 ++ :
                this.Service_Duration <= 4 ? propgrssComplaintLtEq4 ++ :
                    propgrssComplaintGt4 ++;
        } else if (this.Service_Status == "REFERRED") {
        referredComplaintTotal ++;
        this.Service_Duration <= 1 ? referredComplaintLtEq1 ++ :
            this.Service_Duration <= 2 ? referredComplaintLtEq2 ++ :
                this.Service_Duration <= 4 ? referredComplaintLtEq4 ++ :
                    referredComplaintGt4 ++;
        } else if (this.Service_Status == "RESOLVED") {
        resolvedComplaintTotal ++;
        this.Service_Duration <= 1 ? resolvedComplaintLtEq1 ++ :
            this.Service_Duration <= 2 ? resolvedComplaintLtEq2 ++ :
                this.Service_Duration <= 4 ? resolvedComplaintLtEq4 ++ :
                    resolvedComplaintGt4 ++;
        }
    } else if (this.Service_Name == "ENQUIRY") {
        if (this.Service_Status == "WAITING") {
        waitingEnquiryTotal ++;
        this.Service_Duration <= 1 ? waitingEnquiryLtEq1 ++ :
            this.Service_Duration <= 2 ? waitingEnquiryLtEq2 ++ :
                this.Service_Duration <= 4 ? waitingEnquiryLtEq4 ++ :
                    waitingEnquiryGt4 ++;
        } else if (this.Service_Status == "IN PROGRESS") {
        propgrssEnquiryTotal ++;
        this.Service_Duration <= 1 ? propgrssEnquiryLtEq1 ++ :
            this.Service_Duration <= 2 ? propgrssEnquiryLtEq2 ++ :
                this.Service_Duration <= 4 ? propgrssEnquiryLtEq4 ++ :
                    propgrssEnquiryGt4 ++;
        } else if (this.Service_Status == "REFERRED") {
        referredEnquiryTotal ++;
        this.Service_Duration <= 1 ? referredEnquiryLtEq1 ++ :
            this.Service_Duration <= 2 ? referredEnquiryLtEq2 ++ :
                this.Service_Duration <= 4 ? referredEnquiryLtEq4 ++ :
                    referredEnquiryGt4 ++;
        } else if (this.Service_Status == "RESOLVED") {
        resolvedEnquiryTotal ++;
        this.Service_Duration <= 1 ? resolvedEnquiryLtEq1 ++ :
            this.Service_Duration <= 2 ? resolvedEnquiryLtEq2 ++ :
                this.Service_Duration <= 4 ? resolvedEnquiryLtEq4 ++ :
                    resolvedEnquiryGt4 ++;
        }
    } else if (this.Service_Name == "REQUEST") {
        if (this.Service_Status == "WAITING") {
        waitingRequestTotal ++;
        this.Service_Duration <= 1 ? waitingRequestLtEq1 ++ :
            this.Service_Duration <= 2 ? waitingRequestLtEq2 ++ :
                this.Service_Duration <= 4 ? waitingRequestLtEq4 ++ :
                    waitingRequestGt4 ++;
        } else if (this.Service_Status == "IN PROGRESS") {
        propgrssRequestTotal ++;
        this.Service_Duration <= 1 ? propgrssRequestLtEq1 ++ :
            this.Service_Duration <= 2 ? propgrssRequestLtEq2 ++ :
                this.Service_Duration <= 4 ? propgrssRequestLtEq4 ++ :
                    propgrssRequestGt4 ++;
        } else if (this.Service_Status == "REFERRED") {
        referredRequestTotal ++;
        this.Service_Duration <= 1 ? referredRequestLtEq1 ++ :
            this.Service_Duration <= 2 ? referredRequestLtEq2 ++ :
                this.Service_Duration <= 4 ? referredRequestLtEq4 ++ :
                    referredRequestGt4 ++;
        } else if (this.Service_Status == "RESOLVED") {
        resolvedRequestTotal ++;
        this.Service_Duration <= 1 ? resolvedRequestLtEq1 ++ :
            this.Service_Duration <= 2 ? resolvedRequestLtEq2 ++ :
                this.Service_Duration <= 4 ? resolvedRequestLtEq4 ++ :
                    resolvedRequestGt4 ++;
        }
    }
    });

    var baseUrl = "$baseUrl" + "&From_Date=" +
        delaysDuration.fromDate + "&To_Date=" + delaysDuration.toDate;

    var pendingEmergencyLtEq1 =
        parseInt(waitingEmergencyLtEq1) + parseInt(propgrssEmergencyLtEq1) +
        parseInt(referredEmergencyLtEq1) + parseInt(resolvedEmergencyLtEq1);

    var pendingEmergencyLtEq2 =
        parseInt(waitingEmergencyLtEq2) + parseInt(propgrssEmergencyLtEq2) +
        parseInt(referredEmergencyLtEq2) + parseInt(resolvedEmergencyLtEq2);

    var pendingEmergencyLtEq4 =
        parseInt(waitingEmergencyLtEq4) + parseInt(propgrssEmergencyLtEq4) +
        parseInt(referredEmergencyLtEq4) + parseInt(resolvedEmergencyLtEq4);

    var pendingEmergencyGt4 =
        parseInt(waitingEmergencyGt4) + parseInt(propgrssEmergencyGt4) +
        parseInt(referredEmergencyGt4) + parseInt(resolvedEmergencyGt4);

    var pendingEmergencyTotal =
        parseInt(waitingEmergencyTotal) + parseInt(propgrssEmergencyTotal) +
        parseInt(referredEmergencyTotal) + parseInt(resolvedEmergencyTotal);

    var pendingComplaintLtEq1 =
        parseInt(waitingComplaintLtEq1) + parseInt(propgrssComplaintLtEq1) +
        parseInt(referredComplaintLtEq1) + parseInt(resolvedComplaintLtEq1);

    var pendingComplaintLtEq2 =
        parseInt(waitingComplaintLtEq2) + parseInt(propgrssComplaintLtEq2) +
        parseInt(referredComplaintLtEq2) + parseInt(resolvedComplaintLtEq2);

    var pendingComplaintLtEq4 =
        parseInt(waitingComplaintLtEq4) + parseInt(propgrssComplaintLtEq4) +
        parseInt(referredComplaintLtEq4) + parseInt(resolvedComplaintLtEq4);

    var pendingComplaintGt4 =
        parseInt(waitingComplaintGt4) + parseInt(propgrssComplaintGt4) +
        parseInt(referredComplaintGt4) + parseInt(resolvedComplaintGt4);

    var pendingComplaintTotal =
        parseInt(waitingComplaintTotal) + parseInt(propgrssComplaintTotal) +
        parseInt(referredComplaintTotal) + parseInt(resolvedComplaintTotal);

    var pendingEnquiryLtEq1 =
        parseInt(waitingEnquiryLtEq1) + parseInt(propgrssEnquiryLtEq1) +
        parseInt(referredEnquiryLtEq1) + parseInt(resolvedEnquiryLtEq1);

    var pendingEnquiryLtEq2 =
        parseInt(waitingEnquiryLtEq2) + parseInt(propgrssEnquiryLtEq2) +
        parseInt(referredEnquiryLtEq2) + parseInt(resolvedEnquiryLtEq2);

    var pendingEnquiryLtEq4 =
        parseInt(waitingEnquiryLtEq4) + parseInt(propgrssEnquiryLtEq4) +
        parseInt(referredEnquiryLtEq4) + parseInt(resolvedEnquiryLtEq4);

    var pendingEnquiryGt4 =
        parseInt(waitingEnquiryGt4) + parseInt(propgrssEnquiryGt4) +
        parseInt(referredEnquiryGt4) + parseInt(resolvedEnquiryGt4);

    var pendingEnquiryTotal =
        parseInt(waitingEnquiryTotal) + parseInt(propgrssEnquiryTotal) +
        parseInt(referredEnquiryTotal) + parseInt(resolvedEnquiryTotal);

    var pendingRequestLtEq1 =
        parseInt(waitingRequestLtEq1) + parseInt(propgrssRequestLtEq1) +
        parseInt(referredRequestLtEq1) + parseInt(resolvedRequestLtEq1);

    var pendingRequestLtEq2 =
        parseInt(waitingRequestLtEq2) + parseInt(propgrssRequestLtEq2) +
        parseInt(referredRequestLtEq2) + parseInt(resolvedRequestLtEq2);

    var pendingRequestLtEq4 =
        parseInt(waitingRequestLtEq4) + parseInt(propgrssRequestLtEq4) +
        parseInt(referredRequestLtEq4) + parseInt(resolvedRequestLtEq4);

    var pendingRequestGt4 =
        parseInt(waitingRequestGt4) + parseInt(propgrssRequestGt4) +
        parseInt(referredRequestGt4) + parseInt(resolvedRequestGt4);

    var pendingRequestTotal =
        parseInt(waitingRequestTotal) + parseInt(propgrssRequestTotal) +
        parseInt(referredRequestTotal) + parseInt(resolvedRequestTotal);

// Pending
    //********************** PENDING EMERGENCIES ********************** */
    $("#pending_emergency_lteq_1").html(
        "<a href='" + baseUrl +
            "&Service_Name=EMERGENCY&Service_Status=PENDING" +
            "&Service_Duration=1' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingEmergencyLtEq1 +
        "</a>"
    );
    $("#pending_emergency_lteq_2").html(
        "<a href='" + baseUrl +
            "&Service_Name=EMERGENCY&Service_Status=PENDING" +
            "&Service_Duration=2' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingEmergencyLtEq2 +
        "</a>"
    );
    $("#pending_emergency_lteq_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=EMERGENCY&Service_Status=PENDING" +
            "&Service_Duration=4' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingEmergencyLtEq4 +
        "</a>"
    );
    $("#pending_emergency_gt_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=EMERGENCY&Service_Status=PENDING" +
            "&Service_Duration=5' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingEmergencyGt4 +
        "</a>"
    );
    $("#pending_emergency_total").html(
        "<a href='" + baseUrl +
            "&Service_Name=EMERGENCY&Service_Status=PENDING" +
            "&Service_Duration=' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingEmergencyTotal +
        "</a>"
    );

    //********************** PENDING COMPLAINTS ********************** */
    $("#pending_complaints_lteq_1").html(
        "<a href='" + baseUrl +
            "&Service_Name=COMPLAINT&Service_Status=PENDING" +
            "&Service_Duration=1' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingComplaintLtEq1 +
        "</a>"
    );
    $("#pending_complaints_lteq_2").html(
        "<a href='" + baseUrl +
            "&Service_Name=COMPLAINT&Service_Status=PENDING" +
            "&Service_Duration=2' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingComplaintLtEq2 +
        "</a>"
    );
    $("#pending_complaints_lteq_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=COMPLAINT&Service_Status=PENDING" +
            "&Service_Duration=4' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingComplaintLtEq4 +
        "</a>"
    );
    $("#pending_complaints_gt_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=COMPLAINT&Service_Status=PENDING" +
            "&Service_Duration=5' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingComplaintGt4 +
        "</a>"
    );
    $("#pending_complaints_total").html(
        "<a href='" + baseUrl +
            "&Service_Name=COMPLAINT&Service_Status=PENDING" +
            "&Service_Duration=' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingComplaintTotal +
        "</a>"
    );

    //********************** PENDING ENQUIRIES ********************** */
    $("#pending_enquiries_lteq_1").html(
        "<a href='" + baseUrl +
            "&Service_Name=ENQUIRY&Service_Status=PENDING" +
            "&Service_Duration=1' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingEnquiryLtEq1 +
        "</a>"
    );
    $("#pending_enquiries_lteq_2").html(
        "<a href='" + baseUrl +
            "&Service_Name=ENQUIRY&Service_Status=PENDING" +
            "&Service_Duration=2' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingEnquiryLtEq2 +
        "</a>"
    );
    $("#pending_enquiries_lteq_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=ENQUIRY&Service_Status=PENDING" +
            "&Service_Duration=4' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingEnquiryLtEq4 +
        "</a>"
    );
    $("#pending_enquiries_gt_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=ENQUIRY&Service_Status=PENDING" +
            "&Service_Duration=5' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingEnquiryGt4 +
        "</a>"
    );
    $("#pending_enquiries_total").html(
        "<a href='" + baseUrl +
            "&Service_Name=ENQUIRY&Service_Status=PENDING" +
            "&Service_Duration=' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingEnquiryTotal +
        "</a>"
    );

    //********************** PENDING REQUESTS ********************** */
    $("#pending_requests_lteq_1").html(
        "<a href='" + baseUrl +
            "&Service_Name=REQUEST&Service_Status=PENDING" +
            "&Service_Duration=1' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingRequestLtEq1 +
        "</a>"
    );
    $("#pending_requests_lteq_2").html(
        "<a href='" + baseUrl +
            "&Service_Name=REQUEST&Service_Status=PENDING" +
            "&Service_Duration=2' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingRequestLtEq2 +
        "</a>"
    );
    $("#pending_requests_lteq_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=REQUEST&Service_Status=PENDING" +
            "&Service_Duration=4' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingRequestLtEq4 +
        "</a>"
    );
    $("#pending_requests_gt_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=REQUEST&Service_Status=PENDING" +
            "&Service_Duration=5' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingRequestGt4 +
        "</a>"
    );
    $("#pending_requests_total").html(
        "<a href='" + baseUrl +
            "&Service_Name=REQUEST&Service_Status=PENDING" +
            "&Service_Duration=' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + pendingRequestTotal +
        "</a>"
    );

// Resolved
    //********************** RESOLVED EMERGENCIES ********************** */
    $("#resolved_emergency_lteq_1").html(
        "<a href='" + baseUrl +
            "&Service_Name=EMERGENCY&Service_Status=RESOLVED" +
            "&Service_Duration=1' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedEmergencyLtEq1 +
        "</a>"
    );
    $("#resolved_emergency_lteq_2").html(
        "<a href='" + baseUrl +
            "&Service_Name=EMERGENCY&Service_Status=RESOLVED" +
            "&Service_Duration=2' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedEmergencyLtEq2 +
        "</a>"
    );
    $("#resolved_emergency_lteq_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=EMERGENCY&Service_Status=RESOLVED" +
            "&Service_Duration=4' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedEmergencyLtEq4 +
        "</a>"
    );
    $("#resolved_emergency_gt_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=EMERGENCY&Service_Status=RESOLVED" +
            "&Service_Duration=5' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedEmergencyGt4 +
        "</a>"
    );
    $("#resolved_emergency_total").html(
        "<a href='" + baseUrl +
            "&Service_Name=EMERGENCY&Service_Status=RESOLVED" +
            "&Service_Duration=' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedEmergencyTotal +
        "</a>"
    );

    //********************** RESOLVED COMPLAINTS ********************** */
    $("#resolved_complaints_lteq_1").html(
        "<a href='" + baseUrl +
            "&Service_Name=COMPLAINT&Service_Status=RESOLVED" +
            "&Service_Duration=1' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedComplaintLtEq1 +
        "</a>"
    );
    $("#resolved_complaints_lteq_2").html(
        "<a href='" + baseUrl +
            "&Service_Name=COMPLAINT&Service_Status=RESOLVED" +
            "&Service_Duration=2' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedComplaintLtEq2 +
        "</a>"
    );
    $("#resolved_complaints_lteq_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=COMPLAINT&Service_Status=RESOLVED" +
            "&Service_Duration=4' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedComplaintLtEq4 +
        "</a>"
    );
    $("#resolved_complaints_gt_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=COMPLAINT&Service_Status=RESOLVED" +
            "&Service_Duration=5' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedComplaintGt4 +
        "</a>"
    );
    $("#resolved_complaints_total").html(
        "<a href='" + baseUrl +
            "&Service_Name=COMPLAINT&Service_Status=RESOLVED" +
            "&Service_Duration=' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedComplaintTotal +
        "</a>"
    );

    //********************** RESOLVED ENQUIRIES ********************** */
    $("#resolved_enquiries_lteq_1").html(
        "<a href='" + baseUrl +
            "&Service_Name=ENQUIRY&Service_Status=RESOLVED" +
            "&Service_Duration=1' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedEnquiryLtEq1 +
        "</a>"
    );
    $("#resolved_enquiries_lteq_2").html(
        "<a href='" + baseUrl +
            "&Service_Name=ENQUIRY&Service_Status=RESOLVED" +
            "&Service_Duration=2' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedEnquiryLtEq2 +
        "</a>"
    );
    $("#resolved_enquiries_lteq_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=ENQUIRY&Service_Status=RESOLVED" +
            "&Service_Duration=4' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedEnquiryLtEq4 +
        "</a>"
    );
    $("#resolved_enquiries_gt_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=ENQUIRY&Service_Status=RESOLVED" +
            "&Service_Duration=5' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedEnquiryGt4 +
        "</a>"
    );
    $("#resolved_enquiries_total").html(
        "<a href='" + baseUrl +
            "&Service_Name=ENQUIRY&Service_Status=RESOLVED" +
            "&Service_Duration=' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedEnquiryTotal +
        "</a>"
    );

    //********************** RESOLVED REQUESTS ********************** */
    $("#resolved_requests_lteq_1").html(
        "<a href='" + baseUrl +
            "&Service_Name=REQUEST&Service_Status=RESOLVED" +
            "&Service_Duration=1' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedRequestLtEq1 +
        "</a>"
    );
    $("#resolved_requests_lteq_2").html(
        "<a href='" + baseUrl +
            "&Service_Name=REQUEST&Service_Status=RESOLVED" +
            "&Service_Duration=2' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedRequestLtEq2 +
        "</a>"
    );
    $("#resolved_requests_lteq_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=REQUEST&Service_Status=RESOLVED" +
            "&Service_Duration=4' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedRequestLtEq4 +
        "</a>"
    );
    $("#resolved_requests_gt_4").html(
        "<a href='" + baseUrl +
            "&Service_Name=REQUEST&Service_Status=RESOLVED" +
            "&Service_Duration=5' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedRequestGt4 +
        "</a>"
    );
    $("#resolved_requests_total").html(
        "<a href='" + baseUrl +
            "&Service_Name=REQUEST&Service_Status=RESOLVED" +
            "&Service_Duration=' class='badge badge-pill badge-light'" +
            "title='Click for actual data'>" + resolvedRequestTotal +
        "</a>"
    );
};
JS;
$this->registerJs($scriptTables);
