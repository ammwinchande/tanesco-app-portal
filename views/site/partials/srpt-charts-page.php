<?php

use kartik\helpers\Html;

$filters = array_merge(
    $filters,
    [
        'Service_Name' => '',
        'Service_Duration' => ''
    ]
);

$charts_card_holders = [
    [
        'bg_color_top' => 'background-color: #0074D9;',
        'bg_color_btm' => 'background-color: #005ce6;',
        'txt_btm' => 'Total Reported Services',
        'items' => [
            [
                'txt_top' => 'Today; ',
                'id_top' => 'today-rpt-services',
                'url' => array_merge(
                    $filters,
                    [
                        'From_Date' => $today,
                        'To_Date' => $today,
                        'Service_Status' => '',
                    ]
                )
            ],
            [
                'txt_top' => 'Yesterday; ',
                'id_top' => 'yesterday-rpt-services',
                'url' => array_merge(
                    $filters,
                    [
                        'From_Date' => $yesterday,
                        'To_Date' => $yesterday,
                        'Service_Status' => '',
                    ]
                )
            ],
            [
                'txt_top' => 'This Week: ',
                'id_top' => 'week-rpt-services',
                'url' => array_merge(
                    $filters,
                    [
                        'From_Date' => $startWeek,
                        'To_Date' => $endWeek,
                        'Service_Status' => ''
                    ]
                )
            ],
            [
                'txt_top' => 'This Month: ',
                'id_top' => 'month-rpt-services',
                'url' => array_merge(
                    $filters,
                    [
                        'From_Date' => $startMonth,
                        'To_Date' => $endMonth,
                        'Service_Status' => ''
                    ]
                )
            ]
        ]
    ],
    [
        'bg_color_top' => '',
        'bg_color_btm' => 'background-color: #006666;',
        'txt_btm' => 'Total Resolved Services',
        'items' => [
            [
                'txt_top' => 'Today; ',
                'id_top' => 'today-rsv-services',
                'url' => array_merge(
                    $filters,
                    [
                        'From_Date' => $today,
                        'To_Date' => $today,
                        'Service_Status' => 'RESOLVED'
                    ]
                )
            ],
            [
                'txt_top' => 'Yesterday; ',
                'id_top' => 'yesterday-rsv-services',
                'url' => array_merge(
                    $filters,
                    [
                        'From_Date' => $yesterday,
                        'To_Date' => $yesterday,
                        'Service_Status' => 'RESOLVED'
                    ]
                )
            ],
            [
                'txt_top' => 'This Week: ',
                'id_top' => 'week-rsv-services',
                'url' => array_merge(
                    $filters,
                    [
                        'From_Date' => $startWeek,
                        'To_Date' => $endWeek,
                        'Service_Status' => 'RESOLVED'
                    ]
                )
            ],
            [
                'txt_top' => 'This Month: ',
                'id_top' => 'month-rsv-services',
                'url' => array_merge(
                    $filters,
                    [
                        'From_Date' => $startMonth,
                        'To_Date' => $endMonth,
                        'Service_Status' => 'RESOLVED'
                    ]
                )
            ]
        ]
    ],
    [
        'bg_color_top' => 'background-color: #ffad33;',
        'bg_color_btm' => 'background-color: #e68a00;',
        'txt_btm' => 'Total Pending Services',
        'items' => [
            [
                'txt_top' => 'Today: ',
                'id_top' => 'today-pnd-services',
                'url' => array_merge(
                    $filters,
                    [
                        'From_Date' => $today,
                        'To_Date' => $today,
                        'Service_Status' => 'PENDING'
                    ]
                )
            ],
            [
                'txt_top' => 'Yesterday: ',
                'id_top' => 'yesterday-pnd-services',
                'url' => array_merge(
                    $filters,
                    [
                        'From_Date' => $yesterday,
                        'To_Date' => $yesterday,
                        'Service_Status' => 'PENDING'
                    ]
                )
            ],
            [
                'txt_top' => 'This Week: ',
                'id_top' => 'week-pnd-services',
                'url' => array_merge(
                    $filters,
                    [
                        'From_Date' => $startWeek,
                        'To_Date' => $endWeek,
                        'Service_Status' => 'PENDING'
                    ]
                )
            ],
            [
                'txt_top' => 'This Month: ',
                'id_top' => 'month-pnd-services',
                'url' => array_merge(
                    $filters,
                    [
                        'From_Date' => $startMonth,
                        'To_Date' => $endMonth,
                        'Service_Status' => 'PENDING'
                    ]
                )
            ]
        ]
    ]
];
$charts_card = '';
$charts_style = 'font-size: 0.8em; width:100%; height:auto; ' .
    'margin-top:-20px; padding-left:20px; ';
foreach ($charts_card_holders as $charts_card_holder) {
    $charts_card_items = '';
    foreach ($charts_card_holder['items'] as $charts_items) {
        $charts_items_style = 'font-size: 0.8em; font-weight: bold;';
        $charts_card_items .=
            Html::tag(
                'div',
                Html::tag(
                    'span',
                    $charts_items['txt_top'],
                    [
                        'style' => 'font-size: 1.0em; padding: 2px 0px 3px 0px;'
                    ]
                ) .
                    Html::tag(
                        'span',
                        Html::a(
                            '',
                            $charts_items['url'],
                            [
                                'class' => 'badge badge-pill badge-light',
                                'id' => $charts_items['id_top'],
                                'style' => $charts_items_style
                            ]
                        ),
                        [
                            'style' => 'margin: 2px 2px 2px',
                            'class' => 'align-self-center ml-auto'
                        ]
                    ),
                [
                    'class' => 'd-flex'
                ]
            );
    }
    $charts_card .=
        Html::tag(
            'div',
            Html::tag(
                'div',
                Html::tag(
                    'div',
                    $charts_card_items,
                    [
                        'class' => 'card-body',
                        'style' => 'padding-top:-20px'
                    ]
                ) .
                    Html::tag(
                        'div',
                        $charts_card_holder['txt_btm'],
                        [
                            'class' => 'card-footer',
                            'style' => $charts_style . $charts_card_holder['bg_color_btm']
                        ]
                    ),
                [
                    'class' => 'card  bg-teal-700',
                    'style' => $charts_card_holder['bg_color_top']
                ]
            ),
            [
                'class' => 'col'
            ]
        );
}
echo Html::tag(
    'div',
    $charts_card,
    [
        'class' => 'row'
    ]
);

$charts_div_ids = [
    'today-reported-services-chart',
    'yesterday-reported-services-chart',
    'week-reported-services-chart',
    'month-reported-services-chart'
];
foreach ($charts_div_ids as $charts_div_id) {
    echo Html::tag('hr', '') .
        Html::tag(
            'div',
            Html::tag(
                'div',
                Html::tag(
                    'div',
                    '',
                    [
                        'class' => 'card',
                        'id' => $charts_div_id
                    ]
                ),
                [
                    'class' => 'col'
                ]
            ),
            [
                'class' => 'row'
            ]
        );
}
