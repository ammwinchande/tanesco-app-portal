<?php

require __DIR__ . '/srpt-charts-page.php';

$script = <<< JS
$(function() {
    var emergencyMonth, complaintsMonth, enquiriesMonth, requestsMonth;
    var emergencyWeek, complaintsWeek, enquiriesWeek, requestsWeek;
    var emergencyToday, complaintsToday, enquiriesToday, requestsToday;
    var emergency, complaints, enquiries, requests;
    dataFetcher();
});

function dataFetcher() {
    var chartStartOfMonth = moment().startOf('month').format("DD-MM-YYYY");
    var chartEndOfMonth = moment().endOf('month').format("DD-MM-YYYY");
    var chartStartOfWeek = moment().startOf('week').format("DD-MM-YYYY");
    var chartEndOfWeek = moment().endOf('week').format("DD-MM-YYYY");
    var chartYesterday = moment().subtract(1, 'days').format("DD-MM-YYYY");
    var chartToday = moment().format("DD-MM-YYYY");
    var chartScope = "";
    $.ajax({
        type: "POST",
        url: "$urlCharts",
        data: {
            zone: "$zoneName",
            region: "$regionName",
            district: "$districtName",
            startOfMonth: chartStartOfMonth,
            endOfMonth: chartEndOfMonth,
            startOfWeek: chartStartOfWeek,
            endOfWeek: chartEndOfWeek,
            yesterday: chartYesterday,
            today: chartToday,
            scope: chartScope
        },
        success: function (summaryCharts) {
            var summaries = $.parseJSON(summaryCharts);
            if (summaries.hq && (
                "$role" == "sysadmin" || "$role" == "admin"
                || "$role" == "hq-supervisor" ||  "$role" == "hq-user"
                )
            ) {
                dataAnalyser(summaries.hq);
            } else if (summaries.z && (
                "$role" == "zonal-user" || "$role" == "zonal-supervisor"
                || "$role" == "zonal-manager"
                )
            ) {
                dataAnalyser(summaries.z);
            } else if (summaries.r && (
                "$role" == "regional-user" || "$role" == "regional-supervisor"
                || "$role" == "regional-manager"
                )
            ) {
                dataAnalyser(summaries.r);
            } else if (summaries.d && (
                "$role" == "district-user" || "$role" == "district-supervisor"
                || "$role" == "district-manager"
                )
            ) {
                dataAnalyser(summaries.d);
            }
        },
        error: function (exception) {
            console.log(exception);
        }
    });
};

function dataAnalyser(summar) {
    console.log(summar);
    emergencyMonth = summar.month.Emergencies;
    complaintsMonth = summar.month.Complaints;
    enquiriesMonth = summar.month.Enquiries;
    requestsMonth = summar.month.Requests;

    emergencyWeek = summar.week.Emergencies;
    complaintsWeek = summar.week.Complaints;
    enquiriesWeek = summar.week.Enquiries;
    requestsWeek = summar.week.Requests;

    emergency = summar.yesterday.Emergencies;
    complaints = summar.yesterday.Complaints;
    enquiries = summar.yesterday.Enquiries;
    requests = summar.yesterday.Requests;

    emergencyToday = summar.today.Emergencies;
    complaintsToday = summar.today.Complaints;
    enquiriesToday = summar.today.Enquiries;
    requestsToday = summar.today.Requests;

    var totalReportedMonth =
        Number(emergencyMonth.Total) + Number(complaintsMonth.Total) +
        Number(enquiriesMonth.Total) + Number(requestsMonth.Total);
    var totalResolvedMonth =
        Number(emergencyMonth.Resolved) + Number(complaintsMonth.Resolved) +
        Number(enquiriesMonth.Resolved) + Number(requestsMonth.Resolved);
    var totalPendingMonth =
        Number(emergencyMonth.Pending) + Number(complaintsMonth.Pending) +
        Number(enquiriesMonth.Pending) + Number(requestsMonth.Pending);

    $("#month-rpt-services").text(totalReportedMonth);
    $("#month-rsv-services").text(totalResolvedMonth);
    $("#month-pnd-services").text(totalPendingMonth);

    var totalReportedWeek =
        Number(emergencyWeek.Total) + Number(complaintsWeek.Total) +
        Number(enquiriesWeek.Total) + Number(requestsWeek.Total);
    var totalResolvedWeek =
        Number(emergencyWeek.Resolved) + Number(complaintsWeek.Resolved) +
        Number(enquiriesWeek.Resolved) + Number(requestsWeek.Resolved);
    var totalPendingWeek =
        Number(emergencyWeek.Pending) + Number(complaintsWeek.Pending) +
        Number(enquiriesWeek.Pending) + Number(requestsWeek.Pending);

    $("#week-rpt-services").text(totalReportedWeek);
    $("#week-rsv-services").text(totalResolvedWeek);
    $("#week-pnd-services").text(totalPendingWeek);

    var totalReported =
        Number(emergency.Total) + Number(complaints.Total) +
        Number(enquiries.Total) + Number(requests.Total);
    var totalResolved =
        Number(emergency.Resolved) + Number(complaints.Resolved) +
        Number(enquiries.Resolved) + Number(requests.Resolved);
    var totalPending =
        Number(emergency.Pending) + Number(complaints.Pending) +
        Number(enquiries.Pending) + Number(requests.Pending);

    $("#yesterday-rpt-services").text(totalReported);
    $("#yesterday-rsv-services").text(totalResolved);
    $("#yesterday-pnd-services").text(totalPending);

    var totalReportedToday =
        Number(emergencyToday.Total) + Number(complaintsToday.Total) +
        Number(enquiriesToday.Total) + Number(requestsToday.Total);
    var totalResolvedToday =
        Number(emergencyToday.Resolved) +Number(complaintsToday.Resolved) +
        Number(enquiriesToday.Resolved) + Number(requestsToday.Resolved);
    var totalPendingToday =
        Number(emergencyToday.Pending) +Number(complaintsToday.Pending) +
        Number(enquiriesToday.Pending) + Number(requestsToday.Pending);

    $("#today-rpt-services").text(totalReportedToday);
    $("#today-rsv-services").text(totalResolvedToday);
    $("#today-pnd-services").text(totalPendingToday);

    var designMonthly = {
        type:"bar", corner:"rounded",
        colors: ['#0074D9', '#3c763d', '#5cb85c'],
        title:"MONTHLY SUMMARY REPORT AS PER SERVICE",
        subtitle:"This Month Summary"
    };
    chartDrawer(
        emergencyMonth, complaintsMonth, enquiriesMonth,
        requestsMonth, "#month-reported-services-chart", designMonthly
    );

    var designWeekly = {
        type:"bar", corner:"rounded",
        colors: ['#B6B6B4', '#D1D0CE', '#E5E4E2'],
        title:"WEEKLY SUMMARY REPORT AS PER SERVICE",
        subtitle:"This Week Summary"
    };
    chartDrawer(
        emergencyWeek, complaintsWeek, enquiriesWeek,
        requestsWeek, "#week-reported-services-chart", designWeekly
    );

    var designYesterday = {
        type:"bar", corner:"rounded",
        colors: ['#493D26', '#483C32', '#6F4E37'],
        title:"YESTERDAY SUMMARY REPORT AS PER SERVICE",
        subtitle:"Yesterday's Summary"
    };
    chartDrawer(
        emergency, complaints, enquiries, requests,
        "#yesterday-reported-services-chart", designYesterday
    );

    var designToday = {
        type:"bar", corner:"rounded",
        colors: ['#1569C7', '#306754', '#FFA62F'],
        title:"TODAY SUMMARY REPORT AS PER SERVICE",
        subtitle:"Today's Summary"
    };
    chartDrawer(
        emergencyToday, complaintsToday, enquiriesToday,
        requestsToday, "#today-reported-services-chart", designToday
    );
};

function chartDrawer(
    emergencies, complaint, enquiry, request, selectorId, design
) {
    var chartSummaryOptions = {
        chart: {
            height: 350,
            type: design.type,
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '60%',
                endingShape: design.corner,
            },
        },
        colors: design.colors,
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        series: [
            {
                name: 'Reported Services',
                data: [
                    emergencies.Total, complaint.Total,
                    enquiry.Total, request.Total
                ],
            },
            {
                name: 'Resolved Services',
                data: [
                    emergencies.Resolved, complaint.Resolved,
                    enquiry.Resolved, request.Resolved
                ],
            },
            {
                name: 'Pending Services',
                data: [
                    emergencies.Pending, complaint.Pending,
                    enquiry.Pending, request.Pending
                ],
            }
        ],
        xaxis: {
            categories: ["Emergencies", "Complaints", "Enquiries", "Requests"],
        },
        yaxis: {
            title: {
                text: 'Number of Services'
            }
        },
        fill: {
            opacity: 1
        },
        title: {
            text: design.title,
            align: 'center',
            floating: true
        },
        subtitle: {
            text: design.subtitle,
            align: 'center',
        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return val;
                }
            }
        }
    }
    var chartSummary = new ApexCharts(
        document.querySelector(selectorId),
        chartSummaryOptions
    );
    chartSummary.render();
}
JS;
$this->registerJs($script);
