<?php

require __DIR__ . '/prob-freq-page.php';

$scriptFrequently = <<< JS
$(function() {
    var probemergency, probcomplaints, probenquiries, probrequests;
    fetchFrequentlyReported('month');
    $('#frequently-date-selector').on('change', function(e){
        var txt = $(this).find("option:selected").text();
        var val = $(this).find("option:selected").val();
        val = val ? val : 'month';
        console.log('Frequently 2: ' + val + ' => ' + txt);
        fetchFrequentlyReported(val);
    });
    $('#frequently-daterange-selector').on('apply', function(e){
        var txt = $(this).find("option:selected").text();
        var val = $(this).find("option:selected").val();
        val = val ? val : 'month';
        console.log('Frequently 2: ' + val + ' => ' + txt);
        fetchFrequentlyReported(val);
    });
});

function fetchFrequentlyReported(selectedRange) {
    var arrayDuration = durationFunc(selectedRange);
    $(".frequently-duration").html('<b>(' + arrayDuration.now + ')</b>');
    $.ajax({
        type: "POST",
        url: "$urlProFreqency",
        data: {
            from: arrayDuration.fromDate,
            to: arrayDuration.toDate,
            scope: "",
            status: ""
        },
        success: function (frequently) {
            populateDataToFrequently(frequently);
        },
        error: function (exception) {
            console.log(exception);
        }
    });
};

function populateDataToFrequently(frequently) {
    probemergency = $.parseJSON(frequently).Emergencies;
    probcomplaints = $.parseJSON(frequently).Complaints;
    probenquiries = $.parseJSON(frequently).Enquiries;
    probrequests = $.parseJSON(frequently).Requests;
    // console.log(frequently);
    // console.log(probemergency);
    // console.log(probcomplaints);
    // console.log(probenquiries);
    // console.log(probrequests);

    var eme_ids = 0;
    var com_ids = 0;
    var enq_ids = 0;
    var req_ids = 0;

    $.each( probemergency, function( key, prob_emergency ) {
        eme_ids = parseInt(eme_ids) + parseInt(1);
        $("#id_pro_emergency_" + eme_ids).text(eme_ids);
        $("#pro_emergency_" + eme_ids).text(this.Problem);
        $("#fre_emergency_" + eme_ids).text(this.Total);
    });
    $.each( probcomplaints, function( key, prob_complaints ) {
        com_ids = parseInt(com_ids) + parseInt(1);
        $("#id_pro_complaints_" + com_ids).text(com_ids);
        $("#pro_complaints_" + com_ids).text(this.Problem);
        $("#fre_complaints_" + com_ids).text(this.Total);
    });
    $.each( probenquiries, function( key, prob_enquiries ) {
        enq_ids = parseInt(enq_ids) + parseInt(1);
        $("#id_pro_enquiries_" + enq_ids).text(enq_ids);
        $("#pro_enquiries_" + enq_ids).text(this.Problem);
        $("#fre_enquiries_" + enq_ids).text(this.Total);
    });
    $.each( probrequests, function( key, prob_requests ) {
        req_ids = parseInt(req_ids) + parseInt(1);
        $("#id_pro_requests_" + req_ids).text(req_ids);
        $("#pro_requests_" + req_ids).text(this.Problem);
        $("#fre_requests_" + req_ids).text(this.Total);
    });
};

function durationFunc(range) {
    var now, fromDate, toDate;
    var endYear = moment().endOf('year');
    var startYear = moment().startOf('year');
    if (range == 'month') {
        var startOfMonth = moment().startOf('month');
        var endOfMonth = moment().endOf('month');
        now = startOfMonth.format("MMMM").toUpperCase() + ', ' +
            startOfMonth.format("YYYY");
        fromDate = startOfMonth.format("DD-MM-YYYY");
        toDate = endOfMonth.format("DD-MM-YYYY");
    } else if (range == 'last-month') {
        var lastMonth = moment().subtract(1, 'months');
        var startOflMonth = lastMonth.startOf('month');
        var endOflMonth = lastMonth.endOf('month');
        now = startOflMonth.format("MMMM").toUpperCase() + ', ' +
            startOflMonth.format("YYYY");
        fromDate = startOflMonth.format("DD-MM-YYYY");
        toDate = endOflMonth.format("DD-MM-YYYY");
    } else if (range == 'last-two-months') {
        var startOflTwoMonths = moment().subtract(2, 'months').
            startOf('month');
        var endOflTwoMonths = moment().subtract(1, 'months').
            endOf('month');
        now = startOflTwoMonths.format("MMMM").toUpperCase() + ', ' +
            startOflTwoMonths.format("YYYY") + ' TO ' +
            endOflTwoMonths.format("MMMM").toUpperCase() + ', ' +
            endOflTwoMonths.format("YYYY");
        fromDate = startOflTwoMonths.format("DD-MM-YYYY");
        toDate = endOflTwoMonths.format("DD-MM-YYYY");
    } else if (range == 'last-three-months') {
        var startOflThreeMonths = moment().subtract(3, 'months').
            startOf('month');
        var endOflThreeMonths = moment().subtract(1, 'months').
            endOf('month');
        now = startOflThreeMonths.format("MMMM").toUpperCase() + ', ' +
            startOflThreeMonths.format("YYYY") + ' TO ' +
            endOflThreeMonths.format("MMMM").toUpperCase() + ', ' +
            endOflThreeMonths.format("YYYY");
        fromDate = startOflThreeMonths.format("DD-MM-YYYY");
        toDate = endOflThreeMonths.format("DD-MM-YYYY");
    } else if (range == 'last-six-months') {
        var startOflSixMonths = moment().subtract(6, 'months').
            startOf('month');
        var endOflSixMonths = moment().subtract(1, 'months').
            endOf('month');
        now = startOflSixMonths.format("MMMM").toUpperCase() + ', ' +
            startOflSixMonths.format("YYYY") + ' TO ' +
            endOflSixMonths.format("MMMM").toUpperCase() + ', ' +
            endOflSixMonths.format("YYYY");
        fromDate = startOflSixMonths.format("DD-MM-YYYY");
        toDate = endOflSixMonths.format("DD-MM-YYYY");
    } else if (range == 'quarter') {
        var startOfQuarter = moment().startOf('quarter');
        var endOfQuarter = moment().endOf('quarter');
        now = startOfQuarter.format("Qo") + ' QUARTER OF ' +
            startOfQuarter.format("YYYY");
        fromDate = startOfQuarter.format("DD-MM-YYYY");
        toDate = endOfQuarter.format("DD-MM-YYYY");
    } else if (range == 'last-quarter') {
        var startOflQuarter = moment().subtract(1, 'quarters')
            .startOf('quarter');
        var endOflQuarter = moment().subtract(1, 'quarters')
            .endOf('quarter');
        now = startOflQuarter.format("Qo") + ' QUARTER OF ' +
            startOflQuarter.format("YYYY");
        fromDate = startOflQuarter.format("DD-MM-YYYY");
        toDate = endOflQuarter.format("DD-MM-YYYY");
    } else if (range == 'last-two-quarters') {
        var startOflTwoQuarters = moment().subtract(2, 'quarters')
            .startOf('quarter');
        var endOflTwoQuarters = moment().subtract(1, 'quarters')
            .endOf('quarter');
        now = startOflTwoQuarters.format("Qo") + ' QUARTER OF ' +
            startOflTwoQuarters.format("YYYY") + ' TO ' +
            endOflTwoQuarters.format("Qo") + ' QUARTER OF ' +
            endOflTwoQuarters.format("YYYY");
        fromDate = startOflTwoQuarters.format("DD-MM-YYYY");
        toDate = endOflTwoQuarters.format("DD-MM-YYYY");
    } else if (range == 'last-three-quarters') {
        var startOflThreeQuarters = moment().subtract(3, 'quarters')
            .startOf('quarter');
        var endOflThreeQuarters = moment().subtract(1, 'quarters')
            .endOf('quarter');
        now = startOflThreeQuarters.format("Qo") + ' QUARTER OF ' +
            startOflThreeQuarters.format("YYYY") + ' TO ' +
            endOflThreeQuarters.format("Qo") + ' QUARTER OF ' +
            endOflThreeQuarters.format("YYYY");
        fromDate = startOflThreeQuarters.format("DD-MM-YYYY");
        toDate = endOflThreeQuarters.format("DD-MM-YYYY");
    } else if (range == 'year') {
        var startOfYear = startYear.startOf('year').format("DD-MM-YYYY");
        var endOfYear = endYear.endOf('year').format("DD-MM-YYYY");
        now = startOfYear + ' TO ' + endOfYear;
        fromDate = startOfYear;
        toDate = endOfYear;
    } else if (range == 'last-year') {
        var startOfYear = startYear.subtract(1, 'years')
            .startOf('year').format("DD-MM-YYYY");
        var endOfYear = endYear.subtract(1, 'years')
            .endOf('year').format("DD-MM-YYYY");
        now = startOfYear + ' TO ' + endOfYear;
        fromDate = startOfYear;
        toDate = endOfYear;
    } else if (range == 'today') {
        var todayTime = moment().format("DD-MM-YYYY");
        now = moment().startOf('day').format("dddd").toUpperCase();
        fromDate = todayTime;
        toDate = todayTime;
    } else if (range == 'yesterday') {
        var yesterdayTime = moment().subtract(1, 'days');
        now = yesterdayTime.format("dddd").toUpperCase();
        fromDate = yesterdayTime.format("DD-MM-YYYY");
        toDate = yesterdayTime.format("DD-MM-YYYY");
    } else if (range == 'week') {
        var startOfWeek = moment().startOf('week');
        var endOfWeek = moment().endOf('week');
        now = startOfWeek.format("Wo") + ' WEEK OF ' +
            startOfWeek.format("YYYY");
        fromDate = startOfWeek.format("DD-MM-YYYY");
        toDate = endOfWeek.format("DD-MM-YYYY");
    } else if (range == 'last-week') {
        var startWeek = moment().startOf('week');
        var endWeek = moment().endOf('week');
        var startOflWeek = startWeek.subtract(7, 'days').startOf('week');
        var endOflWeek = endWeek.subtract(7, 'days').endOf('week');
        now = startOflWeek.format("Wo") + ' WEEK OF ' +
            startOflWeek.format("YYYY");
        fromDate = startOflWeek.format("DD-MM-YYYY");
        toDate = endOflWeek.format("DD-MM-YYYY");
    } else if (range == 'last-two-weeks') {
        var startWeek = moment().startOf('week');
        var endWeek = moment().endOf('week');
        var startOflTwoWeeks = startWeek.subtract(14, 'days').startOf('week');
        var endOflTwoWeeks = endWeek.subtract(14, 'days').endOf('week');
        now = startOflTwoWeeks.format("Wo") + ' WEEK OF ' +
            startOflTwoWeeks.format("YYYY") + ' TO ' +
            endOflTwoWeeks.format("Wo") + ' WEEK OF ' +
            endOflTwoWeeks.format("YYYY");
        fromDate = startOflTwoWeeks.format("DD-MM-YYYY");
        toDate = endOflTwoWeeks.format("DD-MM-YYYY");
    }else {
        var dareRange = range.split(" - ");
        fromDate = dareRange[0];
        toDate = dareRange[1];
        now = 'THE DATE FROM ' + fromDate + ' TO ' + toDate;
    }
    console.log('Selected Range: ' + range);
    console.log(fromDate + ' => ' + toDate);

    return {
        fromDate: fromDate, toDate: toDate, now: now
    };
};
JS;
$this->registerJs($scriptFrequently);
