<?php

use kartik\helpers\Html;
use yii\bootstrap4\Modal;
use kartik\select2\Select2;
use kartik\daterange\DateRangePicker;

$frequency_select2 = Select2::widget(
    [
        'value' => 'month',
        'name' => 'frequently-date-selector',
        'data' => $selectData,
        'size' => Select2::MEDIUM,
        'options' => [
            'placeholder' => 'Select Date Range.. ',
            'id' => 'frequently-date-selector',
            'options' => [
                'weeks' => ['disabled' => true],
                'months' => ['disabled' => true],
                'quarters' => ['disabled' => true],
                'years' => ['disabled' => true]
            ]
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '100%'
        ]
    ]
);

$frequency_daterange = DateRangePicker::widget(
    [
        'name' => 'frequency-daterange-selector',
        'options' => [
            'class' => 'drp-container form-group'
        ],
        'presetDropdown' => true,
        'hideInput' => true
    ]
);
Modal::begin(
    [
        'options' => [
            'id' => 'frequency-daterange-modal',
            'tabindex' => false
        ],
        'title' => 'Click to Filter Data using Date Range Picker..',
    ]
);
echo $frequency_select2;
// echo $frequency_daterange;
Modal::end();

echo Html::tag('hr', '') .
    Html::tag(
        'div',
        Html::tag(
            'div',
            Html::button(
                '<i class="fas fa-calendar"></i>  Filter Data',
                // Html::icon('calender-alt') . 'Filter Data',
                [
                    'type' => 'button',
                    'data-toggle' => 'modal',
                    'class' => 'btn btn-outline-secondary btn-lg',
                    'data-target' => '#frequency-daterange-modal',
                    'title' => 'Filter Data with Date Range Picker'
                ]
            ),
            [
                'style' => 'padding-bottom: 2px;',
                'class' => 'col-2 text-left'
            ]
        ) .
            Html::tag(
                'div',
                Html::tag(
                    'h5',
                    'TOP FIVE(5) FREQUENTLY REPORTED RECURRING ISSUES FOR ' .
                        Html::tag(
                            'span',
                            '',
                            [
                                'class' => 'frequently-duration'
                            ]
                        ),
                    [
                        'class' => 'text-center'
                    ]
                ),
                [
                    'class' => 'col-10'
                ]
            ),
        [
            'class' => 'row'
        ]
    );
$array = [
    'Emergencies' => [
        'pro_emergency_1' => 'fre_emergency_1',
        'pro_emergency_2' => 'fre_emergency_2',
        'pro_emergency_3' => 'fre_emergency_3',
        'pro_emergency_4' => 'fre_emergency_4',
        'pro_emergency_5' => 'fre_emergency_5'
    ],
    'Complaints' => [
        'pro_complaints_1' => 'fre_complaints_1',
        'pro_complaints_2' => 'fre_complaints_2',
        'pro_complaints_3' => 'fre_complaints_3',
        'pro_complaints_4' => 'fre_complaints_4',
        'pro_complaints_5' => 'fre_complaints_5'
    ],
    'Enquiries' => [
        'pro_enquiries_1' => 'fre_enquiries_1',
        'pro_enquiries_2' => 'fre_enquiries_2',
        'pro_enquiries_3' => 'fre_enquiries_3',
        'pro_enquiries_4' => 'fre_enquiries_4',
        'pro_enquiries_5' => 'fre_enquiries_5'
    ],
    'Requests' => [
        'pro_requests_1' => 'fre_requests_1',
        'pro_requests_2' => 'fre_requests_2',
        'pro_requests_3' => 'fre_requests_3',
        'pro_requests_4' => 'fre_requests_4',
        'pro_requests_5' => 'fre_requests_5'
    ],
];

$frequently_card = '';
foreach ($array as $key => $value) {
    $frequently_card_items = '';
    foreach ($value as $problem => $frequency) {
        $frequently_card_items .=
            Html::tag(
                'tr',
                Html::tag(
                    'th',
                    '',
                    [
                        'id' => 'id_' . $problem,
                        'scope' => 'row'
                    ]
                ) .
                    Html::tag(
                        'td',
                        '',
                        [
                            'id' => $problem
                        ]
                    ) .
                    Html::tag(
                        'td',
                        '',
                        [
                            'class' => 'text-right',
                            'id' => $frequency
                        ]
                    ),
                [
                    'class' => 'table table-hover',
                ]
            );
    }

    $frequently_card .=
        Html::tag(
            'div',
            Html::tag(
                'div',
                Html::tag(
                    'table',
                    Html::tag(
                        'thead',
                        Html::tag(
                            'tr',
                            Html::tag(
                                'th',
                                '#',
                                [
                                    'style' => 'font-weight: bold;' .
                                        'text-align: left;',
                                    'scope' => 'col'
                                ]
                            ) .
                                Html::tag(
                                    'th',
                                    $key,
                                    [
                                        'style' => 'font-weight: bold;' .
                                            'text-align: left;',
                                        'scope' => 'col'
                                    ]
                                ) .
                                Html::tag(
                                    'th',
                                    'Frequecy',
                                    [
                                        'style' => 'font-weight: bold;' .
                                            'text-align: right;',
                                        'scope' => 'col'
                                    ]
                                ),
                            [
                                'class' => 'table table-hover',
                            ]
                        ),
                        [
                            'class' => 'thead-light',
                        ]
                    ) .
                        Html::tag(
                            'tbody',
                            $frequently_card_items,
                            [
                                'class' => ''
                            ]
                        ),
                    [
                        'class' => 'table table-hover',
                        'style' => 'width: 98%; margin: 1%;'
                    ]
                ),
                [
                    'class' => 'card',
                ]
            ),
            [
                'class' => 'col-6',
            ]
        );
}
echo Html::tag(
    'div',
    $frequently_card,
    [
        'class' => 'row',
    ]
);
