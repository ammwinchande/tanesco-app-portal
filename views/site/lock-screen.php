<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login to Unlock';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Content area -->
<div class="content d-flex justify-content-center align-items-center">

    <!-- Login form -->
    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form',
        'action' => Url::to(['site/login']),
        'options' => ['class' => 'login-form'],
        //'layout' => 'horizontal',
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            //'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]);
    ?>
    <div style="width: 300px" class="card mb-0">
        <div class="card-body">
            <div class="text-center mb-3">
                <!--<i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>-->
                <img src="<?php echo Yii::getAlias('@web/images/tanesco-logo-login.png') ?>" width='100' height='90'>
                <br /><br />
                <h5 class="mb-0"><?= Yii::$app->name ?></h5>
                <span class="d-block text-muted">Dear <b class="text-warning"><?= $model ? $model->username : ''; ?></b><br />PLease, Enter your Password to unlock
                    <!--Enter your credentials below--></span>
            </div>

            <div class="form-group form-group-feedback form-group-feedback-left" style="bgcolor:#ffcc00">
                <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'form-control', 'placeholder' => 'Username'])->label(false) ?>
                <div class="form-control-feedback">
                    <i class="icon-user text-muted"></i>
                </div>
            </div>

            <div class="form-group form-group-feedback form-group-feedback-left">
                <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control', 'placeholder' => 'Password', 'value' => null])->label(false)  ?>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Unlock <i class="icon-circle-right2 ml-2"></i>', ['class' => 'btn btn-success btn-block', 'name' => 'login-button']) ?>
            </div>

            <div class="col-md-12" align="center">OR</div>

            <?php ActiveForm::end(); ?>
            <!-- /login form -->

            <div class="col-md-12" align="center" style="margin: 10px auto 10px auto;">
                <a href="<?= \yii\helpers\Url::to(['site/login']) ?>">Sign In as a Different User</a>
            </div>
            <div class="lockscreen-footer text-center" style="margin-top: 20px;">
                Copyright &copy; <?= date('Y') ?> <br>
                All rights reserved
            </div>
        </div>
    </div>

</div>
<!-- /content area -->