<?php

use app\models\User;
use app\models\Zone;
use app\models\Region;
use app\models\District;
use app\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
$directoryAsset = Yii::$app->assetManager->getPublishedUrl(
    '@app/themes/limitless/assets'
);
$this->title = Yii::t('app', 'Dashboard');
$this->params['breadcrumbs'][] = $this->title;
$usesrId = Yii::$app->user->getId();
$role = User::getRole();
$user = User::find()
    ->select(['zone_id', 'region_id', 'district_id'])
    ->where(['=', 'id', $usesrId])
    ->one();
$district = District::find()
    ->select(['name'])
    ->where(['=', 'id', $user->district_id])
    ->one();
$region = Region::find()
    ->select(['name'])
    ->where(['=', 'id', $user->region_id])
    ->one();
$zone = Zone::find()
    ->select(['name'])
    ->where(['=', 'id', $user->zone_id])
    ->one();
$districtName = $district ? ucwords($district->name) : '';
$regionName = $region ? ucwords($region->name) : '';
$zoneName = $zone ? ucwords($zone->name) : '';
$urlCharts = Yii::$app->getUrlManager()->createUrl(
    [
        '/site/services-summary'
    ]
);
$urlProFreqency = Yii::$app->getUrlManager()->createUrl(
    [
        '/site/problem-frequency'
    ]
);
$urlDelayTables = Yii::$app->getUrlManager()->createUrl(
    [
        '/site/services-by-duration'
    ]
);
$filters = [
    '/site/reports',
    'Zone_Name' => $zoneName,
    'Region_Name' => $regionName,
    'District_Name' => $districtName,
];

$baseUrl = Yii::$app->urlManager->createUrl($filters);

$selectData = [
    'weeks' => 'Weekly Options',
    'week' => 'This Week',
    'last-week' => 'Last Week',
    'last-two-weeks' => 'Last Two Weeks',
    'months' => 'Monthly Options',
    'month' => 'This Month',
    'last-month' => 'Last Month',
    'last-two-months' => 'Last Two Months',
    'last-three-months' => 'Last Three Months',
    'last-six-months' => 'Last Six Months',
    'quarters' => 'Quarterly  Options',
    'quarter' => 'This Quarter',
    'last-quarter' => 'Last Quarter',
    'last-two-quarters' => 'Last Two Quarters',
    'last-three-quarters' => 'Last Three Quarters',
    'years' => 'Yearly Options',
    'year' => 'This Year',
    'last-year' => 'Last Year'
];
// $selectData = [
//     'results' => [
//         [
//             'text' => 'Weekly Options',
//             'children' => [
//                 [
//                     'id' => 'week',
//                     'text' => 'This Week',
//                 ],
//                 [
//                     'id' => 'last-week',
//                     'text' => 'Last Week',
//                 ],
//                 [
//                     'id' => 'last-two-weeks',
//                     'text' => 'Last Two Weeks',
//                 ]
//             ],
//         ],
//         [
//             'text' => 'Monthly Options',
//             'children' => [
//                 [
//                     'id' => 'month',
//                     'text' => 'This Month',
//                 ],
//                 [
//                     'id' => 'last-month',
//                     'text' => 'Last Month',
//                 ],
//                 [
//                     'id' => 'last-two-months',
//                     'text' => 'Last Two Months',
//                 ],
//                 [
//                     'id' => 'last-three-months',
//                     'text' => 'Last Three Months',
//                 ],
//                 [
//                     'id' => 'last-six-months',
//                     'text' => 'Last Six Months',
//                 ]
//             ],
//         ],
//         [
//             'text' => 'Quarterly Options',
//             'children' => [
//                 [
//                     'id' => 'quarter',
//                     'text' => 'This Quarter',
//                 ],
//                 [
//                     'id' => 'last-quarter',
//                     'text' => 'Last Quarter',
//                 ],
//                 [
//                     'id' => 'last-two-quarters',
//                     'text' => 'Last Two Quarters',
//                 ],
//                 [
//                     'id' => 'last-three-quarters',
//                     'text' => 'Last Three Quarters',
//                 ]
//             ],
//         ],
//         [
//             'text' => 'Yearly Options',
//             'children' => [
//                 [
//                     'id' => 'year',
//                     'text' => 'This Year',
//                 ],
//                 [
//                     'id' => 'last-year',
//                     'text' => 'Last Year',
//                 ]
//             ],
//         ],
//     ]
// ];

$now = new DateTime();
$today = $now->format('d-m-Y');
$yesterday = $now
    ->sub(new DateInterval('P1D'))
    ->format('d-m-Y');

$nbDay = date('N', strtotime($today));
$monday = new DateTime($today);
$sunday = new DateTime($today);

$startWeek = $monday
    ->modify('-' . ($nbDay - 1) . ' days')
    ->format('d-m-Y');
$endWeek = $sunday
    ->modify('+' . (7 - $nbDay) . ' days')
    ->format('d-m-Y');

$startMonth = date('01-m-Y');
$endMonth = date('t-m-Y');

//* Reported Services Summary Charts/Graphs */
require 'partials/services-rpt-charts.php';

//* Frequently Reported Issues */
require 'partials/problem-frequency.php';

//* Reported Services Summary Tables with Links */
require 'partials/services-rpt-tables.php';
