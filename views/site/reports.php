<?php

use yii\widgets\Pjax;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportsSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = Yii::t('app', 'Services Report');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'site';

$addon = <<< HTML
<div class="input-group-append">
    <span class="input-group-text">
        <i class="fas fa-calendar-alt"></i>
    </span>
</div>
HTML;

$dateRange = <<< HTML
<div class="input-group-prepend">
    <span class="input-group-text">From Date</span>
</div>
{input1}
<!-- {separator} -->
<div class="input-group-prepend">
    <span class="input-group-text">To Date</span>
</div>
{input2}
<div class="input-group-append">
    <span class="input-group-text kv-date-remove">
        <i class="fas fa-times kv-dp-icon"></i>
    </span>
</div>
HTML;
?>
<?php
    Pjax::begin();
    $cols = [
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'Service_No',
            'format' => 'raw',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'Service_Name',
            'format' => 'raw',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'Service_Problem',
            'format' => 'raw',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'Service_Status',
            'format' => 'raw',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'Service_Date',
            'format' => 'raw',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'Service_Duration',
            'format' => 'raw',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'Customer_Name',
            'format' => 'raw',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'Mobile_No',
            'format' => 'raw',
        ],
    ];

    echo Html::tag(
        'div',
        Html::tag(
            'div',
            Html::tag(
                'div',
                Html::tag(
                    'div',
                    $this->render(
                        '_search.php',
                        [
                            'title' => $this->title,
                            'model' => $model,
                            'id' => $id
                        ]
                    ) .
                    Html::tag('hr', ''),
                    ['class' => 'card-header']
                ) .
                Html::tag(
                    'div',
                    $this->render(
                        '../common/actions.php',
                        [
                            'dataProvider' => $dataProvider,
                            'title' => $this->title,
                            'searchModel' => null,
                            'date' => $date,
                            'cols' => $cols,
                            'id' => $id
                        ]
                    ),
                    [
                        'class' => 'card-body',
                        'id' => 'search_options'
                    ]
                ),
                ['class' => 'card']
            ),
            ['class' => 'col']
        ),
        ['class' => 'row']
    );
    Pjax::end();?>