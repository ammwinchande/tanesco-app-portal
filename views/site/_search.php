<?php

use app\models\Zone;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\LocArea;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportsSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */

if (empty($model)) {
    $regionData = $districtData = [];
} else {
    $district = empty($model->District_Name) ?
        [] : LocArea::getDistrict($model->District_Name);
    $region = empty($model->Region_Name) ?
        [] : LocArea::getRegion($model->Region_Name);

    $districtData = empty($district) ? [] : [
        $district->id => $district->name,
    ];
    $regionData = empty($district) ? [] : [
        $region->id => $region->name,
    ];
}
$url = Url::to(['/user/depdrop']);
$regionUrl = Url::to(['/user/region']);
$districtUrl = Url::to(['/user/district']);
// die(print_r($district));
$this->title = Yii::t('app', 'Services Report');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'reports';

$addon = <<< HTML
<div class="input-group-append">
    <span class="input-group-text">
        <i class="fas fa-calendar-alt"></i>
    </span>
</div>
HTML;

$dateRange = <<< HTML
<div class="input-group-prepend">
    <span class="input-group-text">From Date</span>
</div>
{input1}
<!-- {separator} -->
<div class="input-group-prepend">
    <span class="input-group-text">To Date</span>
</div>
{input2}
<div class="input-group-append">
    <span class="input-group-text kv-date-remove">
        <i class="fas fa-times kv-dp-icon"></i>
    </span>
</div>
HTML;
?>

<?php
$form = ActiveForm::begin(
    [
        'action' => ['reports'],
        'method' => 'post'
    ]
);

echo Html::tag(
    'div',
    Html::tag(
        'div',
        $form->field($model, 'Service_Name')->widget(
            Select2::classname(),
            [
                'data' => [
                    'EMERGENCY' => 'EMERGENCY',
                    'COMPLAINT' => 'COMPLAINT',
                    'ENQUIRY' => 'ENQUIRY',
                    'REQUEST' => 'REQUEST'
                ],
                'theme' => Select2::THEME_KRAJEE,
                'size' => Select2::MEDIUM,
                'language' => 'en',
                'options' => [
                    'value' => $model->Service_Name,
                    'id' => 'serviceName',
                    'placeholder' => 'Select Service..',
                    'class' => 'form-control'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]
        ),
        [
            'class' => 'col form-group',
            'style' => 'padding: 2px 5px 2px 5px'
        ]
    ) .
        Html::tag(
            'div',
            $form->field($model, 'Service_Status')->widget(
                Select2::classname(),
                [
                    'data' => [
                        'WAITING' => 'WAITING',
                        'IN PROGRESS' => 'IN PROGRESS',
                        'REFERRED' => 'REFERRED',
                        'RESOLVED' => 'RESOLVED',
                        'PENDING' => 'PENDING'
                    ],
                    'theme' => Select2::THEME_KRAJEE,
                    'size' => Select2::MEDIUM,
                    'language' => 'en',
                    'options' => [
                        'value' => $model->Service_Status,
                        'id' => 'serviceStatus',
                        'placeholder' => 'Select Status..',
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]
            ),
            [
                'class' => 'col form-group',
                'style' => 'padding: 2px 5px 2px 5px'
            ]
        ) .
        Html::tag(
            'div',
            $form->field($model, 'Service_Duration')->widget(
                Select2::classname(),
                [
                    'data' => [
                        1 => '(0-1) Day',
                        2 => '(1-2) Days',
                        4 => '(2-4) Days',
                        5 => ' > 4 Days'
                    ],
                    'theme' => Select2::THEME_KRAJEE,
                    'size' => Select2::MEDIUM,
                    'language' => 'en',
                    'options' => [
                        'value' => $model->Service_Duration,
                        'id' => 'serviceDuration',
                        'placeholder' => 'Select Duration group..',
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]
            ),
            [
                'class' => 'col form-group',
                'style' => 'padding: 2px 5px 2px 5px'
            ]
        ),
    [
        'class' => 'row',
        'style' => 'padding: 0px 2px 0px 2px'
    ]
) .
    Html::tag(
        'div',
        Html::tag(
            'div',
            $form->field($model, 'Zone_Name')->widget(
                Select2::classname(),
                [
                    'data' => ArrayHelper::map(
                        Zone::find()->select(['id', 'name'])->all(),
                        'id',
                        'name'
                    ),
                    'theme' => Select2::THEME_KRAJEE,
                    'size' => Select2::MEDIUM,
                    'language' => 'en',
                    'options' => [
                        'id' => 'zone',
                        'placeholder' => 'Select Zone..',
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]
            ),
            [
                'class' => 'col form-group',
                'style' => 'padding: 2px 5px 2px 5px'
            ]
        ) .
            Html::tag(
                'div',
                $form->field($model, 'Region_Name')->widget(
                    DepDrop::classname(),
                    [
                        'type' => DepDrop::TYPE_SELECT2,
                        'data' => $regionData,
                        'options' => [
                            'id' => 'region'
                        ],
                        'pluginOptions' => [
                            'url' => $regionUrl,
                            'depends' => ['zone'],
                            'placeholder' => 'Select Region..',
                        ],
                        'select2Options' => [
                            'pluginOptions' => [
                                'width' => '100%',
                                'size' => Select2::MEDIUM,
                            ]
                        ],
                    ]
                ),
                [
                    'class' => 'col form-group',
                    'style' => 'padding: 2px 5px 2px 5px'
                ]
            ) .
            Html::tag(
                'div',
                $form->field($model, 'District_Name')->widget(
                    DepDrop::classname(),
                    [
                        'type' => DepDrop::TYPE_SELECT2,
                        'data' => $districtData,
                        'options' => [
                            'id' => 'district'
                        ],
                        'pluginOptions' => [
                            'initialize' => true,
                            'url' => $districtUrl,
                            'depends' => ['region'],
                            'initDepends' => ['zone'],
                            'placeholder' => 'Select District..',
                        ],
                        'select2Options' => [
                            'pluginOptions' => [
                                'width' => '100%',
                                'size' => Select2::MEDIUM,
                            ]
                        ],
                    ]
                ),
                [
                    'class' => 'col form-group',
                    'style' => 'padding: 2px 5px 2px 5px'
                ]
            ),
        [
            'class' => 'row',
            'style' => 'padding: 0px 2px 0px 2px'
        ]
    ) .
    Html::tag(
        'div',
        Html::tag(
            'div',
            'Date Range: ' . DatePicker::widget(
                [
                    'type' => DatePicker::TYPE_RANGE,
                    'model' => $model,
                    'name' => 'From_Date',
                    'value' => $model->From_Date,
                    'name2' => 'To_Date',
                    'value2' => $model->To_Date,
                    'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                    'layout' => $dateRange,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy'
                    ],
                    'options' => [
                        'id' => 'dateRange'
                    ],
                ]
            ),
            [
                'class' => 'col-8 form-group',
                'style' => 'padding: 10px 5px 2px 5px'
            ]
        ) .
            Html::tag(
                'div',
                Html::submitButton(
                    Yii::t('app', 'Search'),
                    [
                        'class' => 'btn btn-success form-control',
                        'style' => 'padding: 8px; margin: 27px 2px 1px 2px'
                    ]
                ),
                [
                    'class' => 'col-2 text-right form-group',
                    'style' => 'padding: 2px 5px 2px 5px'
                ]
            ) .
            Html::tag(
                'div',
                Html::resetButton(
                    Yii::t('app', 'Reset'),
                    [
                        'id' => 'resetButton',
                        'class' => 'btn btn-secondary form-control',
                        'style' => 'padding: 8px; margin: 27px 2px 1px 2px'
                    ]
                ),
                [
                    'class' => 'col-2 text-right form-group',
                    'style' => 'padding: 2px 5px 2px 5px'
                ]
            ),
        [
            'class' => 'row form-group',
            'style' => 'padding: 0px 2px 0px 2px'
        ]
    );
ActiveForm::end(); ?>

<?php
$jsScript = <<< JS
    // JS Code
    var timerid = 0;
    $(function(e) {
        $("#resetButton").on("click", function(e) {
            $("#zone").val(null).trigger('change');
            $("#region").val(null).trigger('change');
            $("#district").val(null).trigger('change');
            $("#serviceName").val(null).trigger('change');
            $("#serviceStatus").val(null).trigger('change');
            $("#serviceDuration").val(null).trigger('change');
            $("#dateRange").val(null).trigger('change');
        });
    });
JS;
$this->registerJs($jsScript);
