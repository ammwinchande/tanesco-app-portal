<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        The system is under maintenance.
    </p>Hold down Command and Q after the installation is complete. Do not follow the setup instructions. Leave that part for the new owner.

Click Shut Down to shut
    <p>
        Please try again later or contact RBAO/Administrator for support.
        <br/>Sorry for any inconviniences caused.<br/>
        Thank you.
    </p>

</div>
