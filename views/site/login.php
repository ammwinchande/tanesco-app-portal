<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .help-block-error {
        color: red;
    }
</style>



<!-- Content area -->
<div class="content d-flex justify-content-center align-items-center">

    <!-- Login form -->
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'login-form'],
        //'layout' => 'horizontal',
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            //'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
    <div style="width: 300px" class="card mb-0">
        <div class="card-body">
            <div class="text-center mb-3">
                <!--<i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>-->
                <img src="<?php echo Yii::getAlias('@web/images/tanesco-logo-login.png') ?>" width='100' height='90'>
                <br /><br />
                <h5 class="mb-0">TANESCO App Portal
                    <!--Login to your account-->
                </h5>
                <span class="d-block text-muted">Enter your credentials to login</span>
            </div>

            <div class="form-group form-group-feedback form-group-feedback-left" style="background-color:#ffcc00">
                <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'form-control', 'placeholder' => 'Username'])->label(false) ?>
                <div class="form-control-feedback">
                    <i class="icon-user text-muted"></i>
                </div>
            </div>

            <div class="form-group form-group-feedback form-group-feedback-left">
                <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control', 'placeholder' => 'Password'])->label(false)  ?>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
            </div>

            <div class="form-group">

                <?= Html::submitButton('Login <i class="icon-circle-right2 ml-2"></i>', ['class' => 'btn btn-success btn-block', 'name' => 'login-button']) ?>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <!-- /login form -->

</div>
<!-- /content area -->
