<?php

use app\models\User;
use app\models\Region;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RegionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Regions');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'region';
?>
<div class="region-index">
    <?php
    $cols = [
        [
            'class' => 'kartik\grid\DataColumn',
            'filter' => Region::getRegions(),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Regoin'],
                'pluginOptions' => [
                    'style' => 'text-transform:capitalize',
                    'allowClear' => true,
                    'width' => '100%'
                ],
            ],
            'contentOptions' => [
                'class' => 'text-capitalize',
            ],
            'attribute' => 'name',
            'format' => 'raw',
        ],
        'reg_code',
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'zone_id',
            'format' => 'raw',
            'value' => 'zones.name',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'created_by',
            'filter' => User::getUsernames(Region::find()->select(['created_by'])),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Creator'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => ''
                ],
            ],
            'format' => 'raw',
            'value' => 'users.username'
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'created_at',
            'format' => 'datetime',
            'filterType' => GridView::FILTER_DATE,
            'filterWidgetOptions' => [
                'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ],
        ],
    ];
    echo $this->render(
        '../common/actions.php',
        [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'title' => $this->title,
            'date' => $date,
            'cols' => $cols,
            'id' => $id
        ]
    );
    ?>
</div>