<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Department */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Departments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$id = 'department';
?>
<div class="department-view no-print">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('../common/top-buttons.php', ['id' => $id, 'model' => $model]) ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'created_by',
                'format' => 'raw',
                'value' => function ($model) {
                    $all = User::find()->select(['username'])->where(['=', 'id', $model->created_by])->distinct()->one();
                    return  $all['username'];
                },
            ],
            'created_at',
            'updated_at',
        ]]);
    ?>
</div>
<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>