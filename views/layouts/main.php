<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\MyAppAsset;
use app\assets\MyCustomAppAsset;

MyAppAsset::register($this);
MyCustomAppAsset::register($this);

// $session_timeout = \app\models\Settings::getSessionTimeout()?:Yii::$app->params['sessionTimeoutSeconds']; //320000
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@app/themes/limitless/assets');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <?= $this->render('navbar.php', ['directoryAsset' => $directoryAsset, 'user' => '', 'logo_icon' => '', 'logo' => '', 'company' => '']) ?>
	<!-- Page content -->
	<div class="page-content">
        <?= $this->render('sidebar.php', ['directoryAsset' => $directoryAsset, 'user' => '', 'logo_icon' => '', 'logo' => '', 'company' => '']) ?>
        <!-- Main content -->
        <div class="content-wrapper" style="overflow-x: hidden;">
            <?= $this->render('content.php', ['content' => $content, 'directoryAsset' => $directoryAsset]) ?>
		</div>
        <!-- /main content -->
	</div>
    <!-- /page content -->
    <!-- BEGIN: PAGE SCRIPTS -->
<?php
$role = \Yii::$app->user->isGuest ? null : \app\models\User::getRole();
if ($role == 'admin' || $role == 'sysadmin') {
    $sess_timeout = 270000;
} else {
    $now = new \DateTime('now');
    $tomorrow = new \DateTime('tomorrow');
    $start = $now->getTimestamp();
    $end = $tomorrow->getTimestamp();
    $sess_timeout = $end - $start;
}
$timeout_ms = $sess_timeout + 30000;
$timeout = $timeout_ms / (1000 * 2 * 5);
$keep_alive = \Yii::$app->request->absoluteUrl;
$sign_out = \Yii::$app->urlManager->createUrl(['/site/logout']);
$lock_screen = \Yii::$app->urlManager->createUrl(['/site/lock-screen']);

$scripts = <<< JS
$(function() {
    // alert("$sess_timeout");
    $.sessionTimeout({
        message: ' You will be Logged out automatically in <b>".$timeout." Seconds</b>.!<br/>Click Stay Connected to continue with your Current Session.!',
        keepAliveUrl: '$keep_alive',
        logoutUrl: '$sign_out',
        redirUrl: '$lock_screen',
        warnAfter: '$sess_timeout',
        redirAfter: '$timeout_ms',
        countdownMessage: 'Redirecting in $timeout seconds.',
        countdownBar: true
    });
});
JS;
$this->registerJs($scripts);
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
