<?php

use kartik\nav\NavX;
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>

<?php
    $date = date('l, F d, Y');
    $navBarOptions = [
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => ['class' => 'navbar navbar-expand-lg navbar-light bg-light'],
        'renderInnerContainer' => false,
        // 'headerContent' => '<span id="toggle_sidemenu_l" class="ad ad-lines"></span>',
    ];
    // NavBar::begin($navBarOptions);

    $itemsX = [
        [
            'label' => '<i class="ad ad-ruby fs18"></i>', 'url' => '#',
            'linkOptions' => ['class' => 'sidebar-menu-toggle']
        ],
        [
            'label' => '<i class="ad ad-wand fs16"></i>', 'url' => '#',
            'linkOptions' => ['class' => 'topbar-menu-toggle']
        ],
        [
            'label' => '<i class="ad ad-screen-full fs18"></i>', 'url' => '#',
            'linkOptions' => ['class' => 'request-fullscreen toggle-active']
        ],
    ];
    $itemsY = [
        [
            'label' => '<i class="ad ad-ruby fs18">TANZANIA ELECTRIC SUPPLY COMPANY LIMITED</i>', 'url' => '#',
            'linkOptions' => ['style' => 'color: #060; font-weight:bold;font-size: 16px;margin-top:9px; margin-left:80px']
        ],
        [
            'label' => '<i class="ad ad-ruby fs18">[ <?= $date ?> ]</i>', 'url' => '#',
            'linkOptions' => ['style' => 'color: #000;font-size: 16px;margin-top:9px; margin-left: 30px;']
        ],
        [
            'label' => '<i class="ad ad-ruby fs18"></i>', 'url' => '#',
            'linkOptions' => ['style' => 'color: #060; font-weight:bold;font-size: 16px;margin-top:9px; margin-left:80px']
        ],
        [
            'label' => '<i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Register',
            'url' => Url::to(['/user/registration/register']),
            'visible' => false//Yii::$app->user->isGuest?false:(Yii::$app->user->identity->isAdmin)
        ],
        Yii::$app->user->isGuest ?
        [
            'label' => '<i class="fa fa-power-off"></i>&nbsp;&nbsp;&nbsp;Sign In (Guest)',
            'url' => Url::to(['/user/security/login']),
            //'linkOptions' => ['data-method' => 'post']
        ]
        :
        [
            'label' => '<i class="fa fa-power-off"></i>&nbsp;&nbsp;&nbsp;Sign Off (<b> '
                . Yii::$app->user->identity->username . '</b>)',
            'url' => Url::to(['/user/security/logout']),
            'linkOptions' => ['data-method' => 'post']
        ],
        [
            'label' => 'Submenu 1', 'active' => true,
            'items' => [
                [
                    'label' => 'Action', 'url' => '#'
                ],
                [
                    'label' => 'Another action', 'url' => '#'
                ],
                [
                    'label' => 'Something else here', 'url' => '#'
                ],
                '<div class="dropdown-divider"></div>',
                ['label' => 'Submenu 2',
                    'items' => [
                        ['label' => 'Action', 'url' => '#'],
                        ['label' => 'Another action', 'url' => '#'],
                        ['label' => 'Something else here', 'url' => '#'],
                        '<div class="dropdown-divider"></div>',
                        ['label' => 'Separated link', 'url' => '#'],
                    ]
                ],
            ]
        ],
    ];
?>
<?php
    $items = [
        ['label' => 'Action', 'url' => '#'],
        ['label' => 'Submenu 1', 'active' => true, 'items' => [
            ['label' => 'Action', 'url' => '#'],
            ['label' => 'Another action', 'url' => '#'],
            ['label' => 'Something else here', 'url' => '#'],
            '<div class="dropdown-divider"></div>',
            ['label' => 'Submenu 2', 'items' => [
                ['label' => 'Action', 'url' => '#'],
                ['label' => 'Another action', 'url' => '#'],
                ['label' => 'Something else here', 'url' => '#'],
                '<div class="dropdown-divider"></div>',
                ['label' => 'Separated link', 'url' => '#'],
            ]],
        ]],
        ['label' => 'My Link', 'url' => '#'],
        ['label' => 'Disabled', 'linkOptions' => ['class' => 'disabled'], 'url' => '#'],
    ];
    $navBarOptions = [];
    NavBar::begin($navBarOptions);
    echo NavX::widget([
        'options' => [
            'renderInnerContainer' => false,
            'class' => 'nav-pills'
        ],
        'items' => $items,
        'activateParents' => true,
        'encodeLabels' => false
    ]);
?>

<?php NavX::widget([
    'options' => ['class' => 'navbar navbar-expand-md navbar-default navbar-left'],
    'activateParents' => true,
    'encodeLabels' => false,
    'items' => $itemsX,
]);
?>

<?php NavX::widget([
    'options' => ['class' => 'navbar navbar-expand-md navbar-default navbar-right'],
    'activateParents' => true,
    'encodeLabels' => false,
    'items' => $itemsY,
]);
?>
<?php NavBar::end(); ?>
