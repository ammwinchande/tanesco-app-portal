<?php

use kartik\nav\NavX;
use yii\helpers\Url;
use \yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>

<?php
	$date = date('l, F d, Y');
	$navBarOptions = [
		'brandLabel' => Yii::$app->name,
		'brandUrl' => Yii::$app->homeUrl,
		'options' => ['class' => 'navbar navbar-expand-lg navbar-light bg-light'],
		'renderInnerContainer' => false,
	];
?>

<?php
echo NavX::widget([
    'options'=>['class'=>'nav nav-pills'],
    $items = [
        ['label' => 'Action', 'url' => '#'],
        ['label' => 'Submenu 1', 'active'=>true, 'items' => [
            ['label' => 'Action', 'url' => '#'],
            ['label' => 'Another action', 'url' => '#'],
            ['label' => 'Something else here', 'url' => '#'],
            '<div class="dropdown-divider"></div>',
            ['label' => 'Submenu 2', 'items' => [
                ['label' => 'Action', 'url' => '#'],
                ['label' => 'Another action', 'url' => '#'],
                ['label' => 'Something else here', 'url' => '#'],
                '<div class="dropdown-divider"></div>',
                ['label' => 'Separated link', 'url' => '#'],
            ]],
        ]],
        ['label' => 'My Link', 'url' => '#'],
        ['label' => 'Disabled', 'linkOptions' => ['class' => 'disabled'], 'url' => '#'],
    ]
]);
 
// Usage with bootstrap navbar
NavBar::begin($navBarOptions);
echo NavX::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => $items,
    'activateParents' => true,
    'encodeLabels' => false
]);
NavBar::end();