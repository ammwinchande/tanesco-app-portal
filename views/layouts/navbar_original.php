<?php
use \yii\helpers\Html;

use app\assets\AppAsset;
AppAsset::register($this);

?>
	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-default">
		
			
			<a href="index.html" class="d-inline-block">
				<img src="<?=$directoryAsset?>/images/logo_light.png" alt="Tanesco Mobile App - Portal" style="width: 60px;">
			</a>
			<div style="padding-top: 10px">
				<p>Tanesco Mobile Portal</p>
			</div>
					

		<div class="collapse navbar-collapse" id="navbar-mobile">
		
					
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
		

			</div>
			<ul class="navbar-nav">

				<?php
                    if (!\Yii::$app->user->isGuest) {
					$role = \app\models\User::getRole();
				?>
				<div style="color: #060; font-weight:bold;font-size: 16px;margin-top:9px; margin-left:80px">TANZANIA ELECTRIC SUPPLY COMPANY LIMITED</div>
				
				<div style="width:150px"></div>
					<li style="float:right" class="nav-item dropdown dropdown-user">
						<a href="#" style="float: right" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
							<img src="<?=$directoryAsset?>/images/demo/users/face11.png" class="rounded-circle mr-2" height="34" alt="">
							<span style="color: #000"><?=\Yii::$app->user->identity->username; ?></span>
							<span style="color: #f90" class="text-warningx"><?="&nbsp[".$role."]"; ?></span>
						</a>

						<div class="dropdown-menu dropdown-menu-right">
							<a href="<?=\Yii::$app->urlManager->createUrl(['/user/profile'])?>" class="dropdown-item">
								<i class="icon-user-plus"></i> My profile
							</a>
							<a href="<?=\Yii::$app->urlManager->createUrl(['/login-history/index'])?>" class="dropdown-item">
								<i class="icon-coins"></i>Login  History
							</a>
							<div class="dropdown-divider"></div>
							<!-- <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Change Password</a> -->
							<?= Html::a(
								'<i class="icon-switch2"></i> Logout',
								// ['/site/logout', 'linkOptions' => ['data-method' => 'post']],
								['/site/logout', 'data-method' => 'post'],
								['class' => 'dropdown-item']
                            ) ?>
						</div>
					</li>
				<?php
                    }
                ?>

			</ul>
		</div>
	</div>
	<!-- /main navbar -->
