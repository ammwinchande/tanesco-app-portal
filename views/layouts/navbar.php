<?php

use kartik\nav\NavX;
use yii\helpers\Url;
use Yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\NavBar;
// use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php
if (!\Yii::$app->user->isGuest) {
  $role = \app\models\User::getRole();
?>

  <!--Navbar -->
  <nav class="mb-1 navbar navbar-expand-lg navbar-light default-color lighten-4">
    <p style="margin: 10px 10px 0 0"><?= Yii::$app->name ?></p>
    <div class="navbar-collapse">

      <a href="#" class="sidebar-control sidebar-main-toggle d-none d-md-block" style="color: #060;">
        <i class="icon-paragraph-justify3"></i>
      </a>

    </div>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          TANZANIA ELECTRIC SUPPLY COMPANY LIMITED
        </li>
      </ul>
      <ul class="navbar-nav ml-auto nav-flex-icons">
        <li class="nav-item">
          <a class="nav-link waves-effect waves-light">
            <!--<i class="fab fa-twitter"></i>-->
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link waves-effect waves-light">
            <!--<i class="fab fa-google-plus-g"></i>-->
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user"></i>
            &nbsp;
            <span style="color: #000"><?= \Yii::$app->user->identity->username; ?></span>
            <span style="color: #f90" class="text-warningx"><?= "&nbsp[" . $role . "]"; ?></span>
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
            <a href="<?= \Yii::$app->urlManager->createUrl(['/user/profile']) ?>" class="dropdown-item">
              <i class="icon-user-plus"></i> My profile
            </a>
            <a href="<?= \Yii::$app->urlManager->createUrl(['/login-history/index']) ?>" class="dropdown-item">
              <i class="icon-coins"></i>Login History
            </a>
            <?= Html::a(
              '<i class="icon-switch2"></i> Logout',
              ['/site/logout', 'data-method' => 'post'],
              ['class' => 'dropdown-item']
            );
            ?>
          </div>
        </li>
      </ul>
    </div>
  </nav>
  <!--/.Navbar -->
<?php
}
?>