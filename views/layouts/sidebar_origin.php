<?php
?>
<style>
    /* Sidebar Styles */

.sidebar {
    max-width: 14rem !important;
    width: 100%;
    min-height: 100vh;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    z-index: 1;
}
.sidebar #sidebarToggleHolder {
    font-size: 10px !important;
    margin: 7px 5px;
}
.nav ul li a.current {
    background: black;
}
</style>
<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md customized-sidebarx">

    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="<?=$directoryAsset?>/images/demo/users/face11.png" width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold"><?=\Yii::$app->user->identity->username;?></div>
                        <div class="font-size-xs opacity-50">
                            <i class="icon-pin font-size-sm"></i> &nbsp;<?=\Yii::$app->user->identity->designation?>
                        </div>
                    </div>

                    <div class="ml-3 align-self-center">
                    <span class="badge <?php echo (!\Yii::$app->user->isGuest) ? 'bg-success' : 'bg-warning'?>"><?php echo (!\Yii::$app->user->isGuest) ? 'Online' : 'Offline'?></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <?=app\models\NavWidget::widget([
                'encodeLabels' => false,
                'options'=>['class'=>'nav nav-sidebar','data-nav-type' => 'accordion' ],
                'labelTemplate' => '<a href="#">{icon}<span> {label}</span>{right-icon}{badge}</a>',
                'linkTemplate' => '<a href="{url}">{icon}<span> {label}</span>{right-icon}{badge}</a>',
                'submenuTemplate'=>"\n<ul class='nav nav-group-sub' data-submenu-title=''>\n{items}\n</ul>\n",
                // 'activateParents'=>true,
                'items'=>\app\models\MenuHelper::getMenu()
            ]) ?>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->