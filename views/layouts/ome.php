<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-default">
		<div class="navbar-brandx">
			
			<a href="index.html" class="d-inline-block">
				<img src="<?=$directoryAsset?>/images/logo_light.png" style="width: 100px; height: 40px; margin-top: -10px; margin-bottom: -10px" alt="Tanesco Mobile App - Portal">
			</a>
			
			<div style="color:#060;font-size: 16px;margin-left:-18px;margin-top:9px">Mobile App - Portal </div>
		</div>
		&nbsp;
		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					
					<a  style="background-color:#ccc" href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
					
				</li>

			</ul>

			<span class="customized-badge badge <?php //echo (!\Yii::$app->user->isGuest) ? 'bg-success' : 'bg-warning'?> ml-md-3 mr-md-auto"><?php //echo (!\Yii::$app->user->isGuest) ? 'Online' : 'Offline'?></span>
			<ul class="navbar-nav">

				<?php
                    if (!\Yii::$app->user->isGuest) {
					$role = \app\models\User::getRole();
				?>
				<div style="color: #060; font-weight:bold;font-size: 16px;margin-top:9px; margin-left:80px">TANZANIA ELECTRIC SUPPLY COMPANY LIMITED</div>
				<div style="color: #000;font-size: 16px;margin-top:9px; margin-left: 30px;">[ <?= date('l, F d, Y') ?> ]</div>
				<div style="width:150px"></div>
					<li style="float:right" class="nav-item dropdown dropdown-user">
						<a href="#" style="float: right" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
							<img src="<?=$directoryAsset?>/images/demo/users/face11.png" class="rounded-circle mr-2" height="34" alt="">
							<span style="color: #000"><?=\Yii::$app->user->identity->username; ?></span>
							<span style="color: #f90" class="text-warningx"><?="&nbsp[".$role."]"; ?></span>
						</a>

						<div class="dropdown-menu dropdown-menu-right">
							<a href="<?=\Yii::$app->urlManager->createUrl(['/user/profile'])?>" class="dropdown-item">
								<i class="icon-user-plus"></i> My profile
							</a>
							<a href="<?=\Yii::$app->urlManager->createUrl(['/login-history/index'])?>" class="dropdown-item">
								<i class="icon-coins"></i>Login  History
							</a>
							<div class="dropdown-divider"></div>
							<!-- <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Change Password</a> -->
							<?= Html::a(
								'<i class="icon-switch2"></i> Logout',
								// ['/site/logout', 'linkOptions' => ['data-method' => 'post']],
								['/site/logout', 'data-method' => 'post'],
								['class' => 'dropdown-item']
                            ) ?>
						</div>
					</li>
				<?php
                    }
                ?>

			</ul>
		</div>
	</div>