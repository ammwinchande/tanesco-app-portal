<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\MyAppAsset;

MyAppAsset::register($this);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@app/themes/limitless/assets');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <?php //echo $this->render('navbar.php',['directoryAsset' => $directoryAsset,'user'=>'','logo_icon'=>'','logo'=>'','company'=>''])?>
	<!-- Page content -->
	<div class="page-content">
        <!-- Main content -->
        <div class="content-wrapper">
        <?=  $content ?>
        <?php //echo $this->render('footer.php',['directoryAsset' => $directoryAsset])?>
		</div>
        <!-- /main content -->
	</div>
	<!-- /page content -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
