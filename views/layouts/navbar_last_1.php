<?php

use kartik\nav\NavX;
use yii\helpers\Url;
use Yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\NavBar;
// use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php
if (!Yii::$app->user->isGuest) {
	$role = \app\models\User::getRole();
?>
<?php
	$date = date('l, F d, Y');
	$itemsX = [
		[
			'label' => '<a href="#" class="sidebar-main-toggle"><i class="icon-paragraph-justify3"></i></a>',
		],
		// [
		// 	'label' => '<i class="ad ad-ruby fs18">Left part</i>', 'url' => '#',
		// 	'linkOptions' => ['class' => 'sidebar-menu-toggle']
		// ],
		// [
		// 	'label' => '<i class="ad ad-wand fs16">sdfgkj</i>', 'url' => '#',
		// 	'linkOptions' => ['class' => 'topbar-menu-toggle']
		// ],
		// [
		// 	'label' => '<i class="ad ad-screen-full fs18">ersetrdfyghjkj</i>', 'url' => '#',
		// 	'linkOptions' => ['class' => 'request-fullscreen toggle-active']
		// ],
	];
    $itemsY = [
		// [
		// 	'label' => 'TANZANIA ELECTRIC SUPPLY COMPANY LIMITED',
		// 	'linkOptions' => ["style" => "font-weight:bold;font-size: 16px; padding-left: 1px; padding-right: 1px;"]
		// ],
		// [
		// 	'label' => '<i class="ad ad-ruby fs18">[ '.$date.' ]</i>', 'url' => '#',
		// 	'linkOptions' => ["style" => "font-size: 16px; padding-left: 1px;"]
		// ],
		// [
		// 	'label' => '<a href="#" class="sidebar-main-toggle"><i class="icon-paragraph-justify3"></i></a>',
		// ],
		[
			'label' => '<i class="fa fa-user"></i>&nbsp;'.Yii::$app->user->identity->username.' (<b>'.$role.'</b>)',
			'items' => [
				[
					'label' => '<i class="icon-profile"></i> My profile', 'url' => '/user/profile',
				],
				[
					'label' => '<i class="icon-watch"></i> Login  History', 'url' => '/login-history/index',
				],
                '<div class="dropdown-divider"></div>',
				[
                    'label' => '<i class="icon-switch2"></i> Logout', 'url' => ['/site/logout', 'data-method' => 'post']
                ],
			],
			'linkOptions' => ["style" => "color: #000;font-size: 16px;"]
		],
	];
?>
<!-- <div class="container-fluid"> -->
    <?php
	$navBarOptions = [
		'brandLabel' => 'APP',//Yii::$app->name,
		'brandUrl' => Yii::$app->homeUrl,
		'options' => ['class' => 'navbar navbar-fixed-top'],
		// 'options' => ['class' => 'navbar navbar-expand-lg navbar-light bg-light mx-0'],
		'renderInnerContainer' => false,
		// 'headerContent' => '<span id="toggle_sidemenu_l" class="ad ad-lines"></span>',
	];
        NavBar::begin($navBarOptions);
    ?>

    <?=
        NavX::widget([
            'options' => ['class' => 'navbar navbar-nav navbar-left'],
            // 'options' => ['class' => 'navbar-nav mr-auto'],
            'activateParents' => true,
            'encodeLabels' => false,
            'items' => $itemsX,
        ]);
    ?>

    <?= 
        NavX::widget([
            'options' => ['class' => 'navbar navbar-nav navbar-right'],
            // 'options' => ['class' => 'navbar-nav ml-auto'],
            'activateParents' => true,
            'encodeLabels' => false,
            'items' => $itemsY,
        ]);
    ?>
    <?php 
        NavBar::end(); 
        }
    ?>
<!-- </div> -->