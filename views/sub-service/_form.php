<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Service;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\SubService */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="sub-service-form">
    <?php
    $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'options' => ['enctype' => 'multipart/form-data']   // important, needed for file upload
    ]);
    ?>
    <div class="row">
        <div class="col-md-12">
            <?=
                $form->field($model, 'service_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Service::find()->select(['id', 'name'])->all(), 'id', 'name'),
                    'theme' => Select2::THEME_KRAJEE,
                    'size' => Select2::MEDIUM,
                    'language' => 'en',
                    'options' => ['placeholder' => 'Select Service..', 'class' => 'form-control'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
            ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?php
            echo $form->field($model, 'is_meter_required')->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                    'handleWidth' => 60,
                    'onText' => 'Active',
                    'offText' => 'InActive'
                ]
            ]);
            ?>
            <?= $form->field($model, 'created_by')->hiddenInput(['value' => \Yii::$app->user->identity->id, 'readonly' => 'readonly'])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>