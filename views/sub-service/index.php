<?php

use app\models\User;
use app\models\Service;
use app\models\SubService;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sub Services');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'sub-service';
?>
<div class="sub-service-index">
    <?php
        $cols = [
            [
                'class' => 'kartik\grid\DataColumn',
                'contentOptions' => [
                    'class' => 'text-capitalize',
                ],
                'attribute' => 'name',
                'format' => 'raw',
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'filter' => Service::getServices(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Select Service'],
                    'pluginOptions' => [
                        'style' => 'text-transform:capitalize',
                        'allowClear' => true,
                        'width' => '100%'
                    ],
                ],
                'attribute' => 'service_id',
                'format' => 'raw',
                'value' => 'services.name',
            ],
            [
                'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'is_meter_required',
                'vAlign' => 'middle',
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'created_by',
                'filter' => User::getUsernames(SubService::find()->select(['created_by'])),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Select Creator'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'width' => ''
                    ],
                ],
                'format' => 'raw',
                'value' => 'users.username'
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ],
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'updated_at',
                'format' => 'datetime',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ],
            ],
        ];
        echo $this->render(
            '../common/actions.php',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'title' => $this->title,
                'date' => $date,
                'cols' => $cols,
                'id' => $id
            ]
        );
    ?>
</div>
