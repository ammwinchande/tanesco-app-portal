<?php

use yii\helpers\Html;
use app\models\User;
use app\models\Service;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SubCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sub Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$id = 'sub-category';
?>
<div class="sub-category-view no-print">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('../common/top-buttons.php', ['id' => $id, 'model' => $model]) ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'service_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $service = Service::find()->select(['name'])->where(['=', 'id', $model->service_id])->distinct()->one();
                    return  $service['name'];
                },
            ],
            [
                'attribute' => 'is_meter_required',
                'format' => 'raw',
                'value' => $model->is_meter_required ? "<span class='text-success'>Active</span>" : "<span class='text-danger'>Inactive</span>",
            ],
            [
                'attribute' => 'created_by',
                'format' => 'raw',
                'value' => function ($model) {
                    $all = User::find()->select(['username'])->where(['=', 'id', $model->created_by])->distinct()->one();
                    return  $all['username'];
                },
            ],
            'created_at',
            'updated_at',
        ]]);
    ?>
</div>
<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>
