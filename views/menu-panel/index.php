<?php

use app\models\User;
use app\models\MenuPanel;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MenuPanelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Menu Panels');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'menu-panel';
?>
<div class="menu-panel-index">
    <?php
    $cols = [
        'type',
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'parent_id',
            'filter' => MenuPanel::getParentMenu(MenuPanel::find()->select(['parent_id'])),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Creator'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => ''
                ],
            ],
            'format' => 'raw',
            'value' => 'parents.name'
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'filter' => MenuPanel::getMenu(),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Menu'],
                'pluginOptions' => [
                    'style' => 'text-transform:capitalize',
                    'allowClear' => true,
                    'width' => '100%'
                ],
            ],
            'contentOptions' => [
                'class' => 'text-capitalize',
            ],
            'attribute' => 'name',
            'format' => 'raw',
        ],
        'redirect',
        'params',
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'created_by',
            'filter' => User::getUsernames(MenuPanel::find()->select(['created_by'])),
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Select Creator'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => ''
                ],
            ],
            'format' => 'raw',
            'value' => 'users.username'
        ],
        [
            'class' => 'kartik\grid\BooleanColumn',
            'attribute' => 'status',
            'vAlign' => 'middle',
        ],
        'order_index',
        'icon',
    ];
    echo $this->render(
        '../common/actions.php',
        [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'title' => $this->title,
            'date' => $date,
            'cols' => $cols,
            'id' => $id
        ]
    );
    ?>
</div>