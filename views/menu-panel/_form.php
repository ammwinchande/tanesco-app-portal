<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\select2\Select2;
use kartik\widgets\TouchSpin;
use app\models\MenuPanel;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MenuPanel */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="menu-panel-form">
    <?php
    $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'options' => ['enctype' => 'multipart/form-data']   // important, needed for file upload
    ]);
    ?>
    <div class="row">
        <div class="col-md-6">
            <?php
            $menu = MenuPanel::find()->select(['id', 'parent_id', 'name'])->where(['=', 'parent_id', 0])->all();
            $menuItems = ArrayHelper::map(\app\models\MenuPanel::find()->all(), 'id', 'name');
            array_unshift_assoc($menuItems, 0, 'Menu Item');
            function array_unshift_assoc(&$arr, $key, $val)
            {
                $arr = array_reverse($arr, true);
                $arr[$key] = $val;
                $arr = array_reverse($arr, true);
                return count($arr);
            }
            $listData = \yii\helpers\ArrayHelper::map($menu, 'id', 'name');
            echo $form->field($model, 'parent_id')->widget(Select2::classname(), [
                'data' => $menuItems,
                'theme' => Select2::THEME_KRAJEE,
                'size' => Select2::MEDIUM,
                'language' => 'en',
                'options' => ['placeholder' => 'Select Parent ...', 'class' => 'form-control'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
            <?= $form->field($model, 'redirect')->textInput(['maxlength' => true]) ?>
            <?php
            echo $form->field($model, 'order_index')->widget(TouchSpin::className(), [
                'pluginOptions' => [
                    'initval' => 0,
                    'min' => 0,
                    'max' => 15,
                    'step' => 1,
                    //'decimals' => 2,
                    'boostat' => 5,
                    'maxboostedstep' => 10,
                    //'prefix' => 'units',
                ],
                //'options'=>["style"=>"width:70%"]
            ]);
            ?>
            <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'created_by')->hiddenInput(['value' => \Yii::$app->user->identity->id])->label(false) ?>
        </div>
        <div class="col-md-6">
            <?php
            // Normal select with ActiveForm & model
            echo $form->field($model, 'type')->widget(Select2::classname(), [
                'data' => ['MENU' => 'MENU', 'SUBMENU' => 'SUBMENU'],
                'language' => 'en',
                'theme' => Select2::THEME_KRAJEE,
                'size' => Select2::MEDIUM,
                'options' => ['placeholder' => 'Select type ...', 'class' => 'form-control'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'params')->textInput(['maxlength' => true]) ?>
            <?php
            echo $form->field($model, 'status')->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                    'handleWidth' => 60,
                    'onText' => 'Active',
                    'offText' => 'InActive'
                ]
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>