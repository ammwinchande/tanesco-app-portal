<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use app\models\MenuPanel;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\MenuPanel */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menu Panels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$id = 'menu-panel';
?>
<div class="menu-panel-view no-print">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('../common/top-buttons.php', ['id' => $id, 'model' => $model]) ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            [
                'attribute' => 'parent_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $allParents = MenuPanel::find()->select(['name'])->where(['=', 'id', $model->parent_id])->distinct()->one();
                    return  $allParents['name'];
                },
            ],
            'name',
            'redirect',
            'params',
            [
                'attribute' => 'created_by',
                'format' => 'raw',
                'value' => function ($model) {
                    $all = User::find()->select(['username'])->where(['=', 'id', $model->created_by])->distinct()->one();
                    return  $all['username'];
                },
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => $model->status ? "<span class='text-success'>Active</span>" : "<span class='text-danger'>Inactive</span>",
            ],
            'order_index',
            'icon',
        ]]);
    ?>
</div>
<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>