<?php

use app\models\Region;
use app\models\District;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LocArea */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Loc Areas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$id = 'loc-area';
?>
<div class="loc-area-view no-print">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('../common/top-buttons.php', ['id' => $id, 'model' => $model]) ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'area_code',
            [
                'attribute' => 'district_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $district = District::find()->select(['name'])->where(['=', 'id', $model->district_id])->distinct()->one();
                    return  $district['name'];
                },
            ],
            [
                'attribute' => 'region_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $region = Region::find()->select(['name'])->where(['=', 'id', $model->region_id])->distinct()->one();
                    return  $region['name'];
                },
            ],
        ]]);
    ?>
</div>
<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>