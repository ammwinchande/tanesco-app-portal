<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\LocAreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Loc Areas');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'loc-area';
?>
<div class="loc-area-index">
    <?php
    $cols = [
        [
            'class' => 'kartik\grid\DataColumn',
            'contentOptions' => [
                'class' => 'text-capitalize',
            ],
            'attribute' => 'name',
            'format' => 'raw',
        ],
        'area_code',
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'district_id',
            'format' => 'raw',
            'value' => 'districts.name',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'region_id',
            'format' => 'raw',
            'value' => 'regions.name',
        ],
    ];
    echo $this->render(
        '../common/actions.php',
        [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'title' => $this->title,
            'date' => $date,
            'cols' => $cols,
            'id' => $id
        ]
    );
    ?>
</div>