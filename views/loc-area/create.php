<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LocArea */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Loc Area',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Loc Areas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loc-area-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?=
        $this->render('_form', [
            'model' => $model,
        ])
    ?>
</div>
