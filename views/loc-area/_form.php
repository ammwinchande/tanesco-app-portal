<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LocArea */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="loc-area-form">
    <?php
    $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'options' => ['enctype' => 'multipart/form-data']   // important, needed for file upload
    ]);
    ?>
    <?php //echo $form->field($model, 'id')->textInput()
    ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'id')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'area_code')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'district_id')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'region_id')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>