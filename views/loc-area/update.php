<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LocArea */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Loc Area',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Loc Areas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="loc-area-update">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?=
        $this->render('_form', [
            'model' => $model,
        ])
    ?>
</div>
