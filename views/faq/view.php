<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Faqs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$id = 'faq';
?>

<div class="faq-view no-print">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('../common/top-buttons.php', ['id' => $id, 'model' => $model]) ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'question:ntext',
            'answer:ntext',
            'created_by',
            'created_at',
            'updated_at',
        ]]);
    ?>

</div>

<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>