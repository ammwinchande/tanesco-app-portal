<?php

use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LocStreetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Loc Streets');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'loc-street';
?>
<div class="loc-street-index">>
    <?php
    $cols = [
        [
            'class' => 'kartik\grid\DataColumn',
            'contentOptions' => [
                'class' => 'text-capitalize',
            ],
            'attribute' => 'name',
            'format' => 'raw',
        ],
        'street_code',
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'area_id',
            'format' => 'raw',
            'value' => 'areas.name',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'district_id',
            'format' => 'raw',
            'value' => 'districts.name',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'region_id',
            'format' => 'raw',
            'value' => 'regions.name',
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'created_at',
            'format' => 'datetime',
            'filterType' => GridView::FILTER_DATE,
            'filterWidgetOptions' => [
                'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ],
        ],
    ];
    echo $this->render(
        '../common/actions.php',
        [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'title' => $this->title,
            'date' => $date,
            'cols' => $cols,
            'id' => $id
        ]
    );
    ?>
</div>