<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LocStreet */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Loc Street',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Loc Streets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loc-street-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?=
        $this->render('_form', [
            'model' => $model,
        ])
    ?>
</div>
