<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LocStreet */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Loc Street',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Loc Streets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="loc-street-update">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?=
        $this->render('_form', [
            'model' => $model,
        ])
    ?>
</div>
