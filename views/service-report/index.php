<?php

use yii\widgets\Pjax;
use yii\helpers\Html;
use app\models\Client;
use app\models\District;
use app\models\LocArea;
use app\models\LocStreet;
use app\models\Region;
use app\models\Service;
use app\models\SubService;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServiceReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Service Reports');
$this->params['breadcrumbs'][] = $this->title;
$date = date('Y-m-dTH:i:s');
$id = 'service-report';

$cols = [
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'details',
        'headerOptions' => [
            'style' => 'width:25%'
        ],
        'format' => 'raw',
        // 'width' => '25%',
    ],
    'meter_no',
    [
        'attribute' => 'coverage',
        'visible' => false
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'headerOptions' => [
            'style' => 'width:25%'
        ],
        'attribute' => 'service_no',
        'contentOptions' => [
            'class' => 'font-weight-bold',
        ],
        'format' => 'raw',
    ],
    [
        'attribute' => 'picture',
        'visible' => false
    ],
    [
        'attribute' => 'video',
        'visible' => false
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'remarks',
        'headerOptions' => [
            'style' => 'width:15%'
        ],
        'format' => 'raw',
        'visible' => false,
    ],
    [
        'attribute' => 'rating',
        'visible' => false
    ],
    [
        'attribute' => 'feedback',
        'visible' => false
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'filter' => Service::getServices(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Select Service'],
            'pluginOptions' => [
                'style' => 'text-transform:capitalize',
                'allowClear' => true,
                'width' => '100%'
            ],
        ],
        'contentOptions' => [
            'class' => 'text-capitalize',
        ],
        'attribute' => 'service_id',
        'format' => 'raw',
        'value' => 'services.name',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'filter' => SubService::getSubServices(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Select Sub-Service'],
            'pluginOptions' => [
                'style' => 'text-transform:capitalize',
                'allowClear' => true,
                'width' => '100%'
            ],
        ],
        'attribute' => 'sub_service_id',
        'contentOptions' => [
            'class' => 'text-capitalize',
        ],
        'format' => 'raw',
        'value' => 'subServices.name',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'filter' => Client::getClients(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Select Customer'],
            'pluginOptions' => [
                'style' => 'text-transform:capitalize',
                'allowClear' => true,
                'width' => '100%'
            ],
        ],
        'contentOptions' => [
            'class' => 'text-capitalize',
        ],
        'attribute' => 'customer_id',
        'format' => 'raw',
        'value' => 'clients.name',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'filter' => Region::getRegions(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Select Region'],
            'pluginOptions' => [
                'style' => 'text-transform:capitalize',
                'allowClear' => true,
                'width' => '100%'
            ],
        ],
        'contentOptions' => [
            'class' => 'text-capitalize',
        ],
        'attribute' => 'region',
        'format' => 'raw',
        'value' => 'regions.name',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'filter' => District::getDistricts(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Select District'],
            'pluginOptions' => [
                'style' => 'text-transform:capitalize',
                'allowClear' => true,
                'width' => '100%'
            ],
        ],
        'contentOptions' => [
            'class' => 'text-capitalize',
        ],
        'attribute' => 'district',
        'format' => 'raw',
        'value' => 'districts.name',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'filter' => LocArea::getAreas(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Select Area'],
            'pluginOptions' => [
                'style' => 'text-transform:capitalize',
                'allowClear' => true,
                'width' => '100%'
            ],
        ],
        'contentOptions' => [
            'class' => 'text-capitalize',
        ],
        'attribute' => 'area',
        'format' => 'raw',
        'value' => 'areas.name',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'filter' => LocStreet::getStreets(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Select Street'],
            'pluginOptions' => [
                'style' => 'text-transform:capitalize',
                'allowClear' => true,
                'width' => '100%'
            ],
        ],
        'contentOptions' => [
            'class' => 'text-capitalize',
        ],
        'attribute' => 'street',
        'format' => 'raw',
        'value' => 'streets.name',
    ],
    [
        'attribute' => 'special_mark',
        'visible' => false
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'contentOptions' => [
            'class' => 'text-uppercase',
        ],
        'attribute' => 'status',
    ],
    [
        'class' => 'kartik\grid\BooleanColumn',
        'attribute' => 'send_to_sdm',
        'vAlign' => 'middle',
        'trueLabel' => 'Yes',
        'falseLabel' => 'No',
    ],
    [
        'attribute' => 'reported_id',
        'visible' => false
    ],
    [
        'attribute' => 'resolved_id',
        'visible' => false
    ],
];
Pjax::begin();

echo Html::tag(
    'div',
    Html::tag(
        'div',
        Html::tag(
            'div',
            Html::tag(
                'div',
                $this->render(
                    '_search.php',
                    [
                        'title' => $this->title,
                        'model' => $searchModel,
                        'id' => $id
                    ]
                ) .
                    Html::tag('hr', ''),
                ['class' => 'card-header']
            ) .
                Html::tag(
                    'div',
                    $this->render(
                        '../common/actions.php',
                        [
                            'dataProvider' => $dataProvider,
                            'searchModel' => $searchModel,
                            'title' => $this->title,
                            'date' => $date,
                            'cols' => $cols,
                            'id' => $id
                        ]
                    ),
                    [
                        'class' => 'card-body',
                        'id' => 'search_options'
                    ]
                ),
            ['class' => 'card']
        ),
        ['class' => 'col']
    ),
    ['class' => 'row']
);
Pjax::end();
