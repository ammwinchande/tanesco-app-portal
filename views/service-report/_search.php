<?php

use app\models\ServiceReport;
use app\models\SubService;
use app\models\Service;
use app\models\LocArea;
use app\models\Region;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServiceReportSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */

// die(print_r($model));
// die(print_r($model->role));
if (empty($model) || $model->isNewRecord) {
    $fname = $lname = $role = '';
    $districtId = $areaId = $streetId = null;
    $districtData = $areaData = $streetData = [];
} else {
    $role = $model->roles->item_name;
    $fname = explode('.', $model->username)[0];
    $lname = explode('.', $model->username)[1];
    $district = empty($model->district) ?
        [] : LocArea::getDistrict($model->district);
    $street = empty($model->street) ?
        [] : LocArea::getStreet($model->street);
    $area = empty($model->area) ?
        [] : LocArea::getArea($model->area);

    $districtId = !empty($district) ? $district->id : null; //32
    $streetId = !empty($street) ? $street->id : null; //7
    $areaId = !empty($area) ? $area->id : null; //7

    $districtData = empty($district) ? [] : [
        $district->id => $district->name,
    ];
    $streetData = empty($street) ? [] : [
        $street->id => $street->name,
    ];
    $areaData = empty($area) ? [] : [
        $area->id => $area->name,
    ];
}
$this->title = Yii::t('app', 'Services Report');
$this->params['breadcrumbs'][] = $this->title;
$url = Url::to(['/service-report/dep-drop']);
$districtUrl = Url::to(['/service-report/district']);
$streetUrl = Url::to(['/service-report/street']);
$areaUrl = Url::to(['/service-report/area']);
$date = date('Y-m-dTH:i:s');
$id = 'reports';

$form = ActiveForm::begin(
    [
        'action' => ['index'],
        'method' => 'get'
    ]
);

echo
    Html::tag(
        'div',
        Html::tag(
            'div',
            $form->field($model, 'region')->widget(
                Select2::classname(),
                [
                    'data' => ArrayHelper::map(
                        Region::find()->select(['id', 'name'])->asArray()->all(),
                        'id',
                        'name'
                    ),
                    'theme' => Select2::THEME_KRAJEE,
                    'size' => Select2::MEDIUM,
                    'language' => 'en',
                    'options' => [
                        'id' => 'region'
                    ],
                    'pluginOptions' => [
                        'placeholder' => 'Select Region..'
                    ],
                ]
            ),
            [
                'class' => 'col form-group',
                'style' => 'padding: 2px 5px 2px 5px'
            ]
        ) .
            Html::tag(
                'div',
                $form->field($model, 'district')->widget(
                    DepDrop::classname(),
                    [
                        'type' => DepDrop::TYPE_SELECT2,
                        'data' => $districtData,
                        'options' => [
                            'id' => 'district'
                        ],
                        'pluginOptions' => [
                            'url' => $districtUrl,
                            'depends' => ['region'],
                            'loadingText' => 'Loading Districts..',
                        ],
                        'select2Options' => [
                            'pluginOptions' => [
                                'width' => '100%',
                                'size' => Select2::SMALL,
                            ]
                        ]
                    ]
                ),
                [
                    'class' => 'col form-group',
                    'style' => 'padding: 2px 5px 2px 5px'
                ]
            ) .
            Html::tag(
                'div',
                $form->field($model, 'area')->widget(
                    DepDrop::classname(),
                    [
                        'type' => DepDrop::TYPE_SELECT2,
                        'data' => $areaData,
                        'options' => [
                            'id' => 'area'
                        ],
                        'pluginOptions' => [
                            'url' => $areaUrl,
                            'depends' => ['district'],
                            'loadingText' => 'Loading Areas..',
                        ],
                        'select2Options' => [
                            'pluginOptions' => [
                                'width' => '100%',
                                'size' => Select2::SMALL,
                            ]
                        ]
                    ]
                ),
                [
                    'class' => 'col form-group',
                    'style' => 'padding: 2px 5px 2px 5px'
                ]
            ) .
            Html::tag(
                'div',
                $form->field($model, 'street')->widget(
                    DepDrop::classname(),
                    [
                        'type' => DepDrop::TYPE_SELECT2,
                        'data' => $streetData,
                        'options' => [
                            'id' => 'street'
                        ],
                        'pluginOptions' => [
                            'url' => $streetUrl,
                            'initialize' => true,
                            'depends' => ['area'],
                            'initDepends' => ['region'],
                            'loadingText' => 'Loading Streets..',
                        ],
                        'select2Options' => [
                            'pluginOptions' => [
                                'width' => '100%',
                                'size' => Select2::SMALL,
                            ]
                        ]
                    ]
                ),
                [
                    'class' => 'col form-group',
                    'style' => 'padding: 2px 5px 2px 5px'
                ]
            ),
        [
            'class' => 'row',
            'style' => 'padding: 0px 2px 0px 2px'
        ]
    ) .
        Html::tag(
            'div',
            Html::tag(
                'div',
                $form->field($model, 'service_no'),
                [
                    'class' => 'col form-group',
                    'style' => 'padding: 2px 5px 2px 5px'
                ]
            ) .
                Html::tag(
                    'div',
                    $form->field($model, 'customer_id')->widget(
                        Select2::classname(),
                        [
                            'data' => ServiceReport::getAllClients(),
                            'theme' => Select2::THEME_KRAJEE,
                            'size' => Select2::MEDIUM,
                            'language' => 'en',
                            'options' => [
                                'id' => 'customer_id'
                            ],
                            'pluginOptions' => [
                                'placeholder' => 'Reported By'
                            ],
                        ]
                    ),
                    [
                        'class' => 'col form-group',
                        'style' => 'padding: 2px 5px 2px 5px'
                    ]
                ) .
                Html::tag(
                    'div',
                    $form->field($model, 'service_id')->widget(
                        Select2::classname(),
                        [
                            'data' => ArrayHelper::map(
                                Service::find()
                                    ->select(['id', 'name'])->asArray()->all(),
                                'id',
                                'name'
                            ),
                            'theme' => Select2::THEME_KRAJEE,
                            'size' => Select2::MEDIUM,
                            'language' => 'en',
                            'options' => [
                                'id' => 'service_id'
                            ],
                            'pluginOptions' => [
                                'placeholder' => 'Select Service..'
                            ],
                        ]
                    ),
                    [
                        'class' => 'col form-group',
                        'style' => 'padding: 2px 5px 2px 5px'
                    ]
                ) .
                Html::tag(
                    'div',
                    $form->field($model, 'sub_service_id')->widget(
                        Select2::classname(),
                        [
                            'data' => ArrayHelper::map(
                                SubService::find()
                                    ->select(['id', 'name'])->asArray()->all(),
                                'id',
                                'name'
                            ),
                            'theme' => Select2::THEME_KRAJEE,
                            'size' => Select2::MEDIUM,
                            'language' => 'en',
                            'options' => [
                                'id' => 'sub_service_id'
                            ],
                            'pluginOptions' => [
                                'placeholder' => 'Select Problem..'
                            ],
                        ]
                    ),
                    [
                        'class' => 'col form-group',
                        'style' => 'padding: 2px 5px 2px 5px'
                    ]
                ),
            [
                'class' => 'row',
                'style' => 'padding: 0px 2px 0px 2px'
            ]
        ) .
        Html::tag(
            'div',
            Html::tag(
                'div',
                $form->field($model, 'meter_no'),
                [
                    'class' => 'col form-group',
                    'style' => 'padding: 2px 5px 2px 5px'
                ]
            ) .
                Html::tag(
                    'div',
                    $form->field($model, 'status')->widget(
                        Select2::classname(),
                        [
                            'data' => [
                                'RECEIVED' => 'RECEIVED',
                                'WAITING' => 'WAITING',
                                'IN PROGRESS' => 'IN PROGRESS',
                                'REFERRED' => 'REFERRED',
                                'RESOLVED' => 'RESOLVED',
                                'CLOSED' => 'CLOSED'
                            ],
                            'theme' => Select2::THEME_KRAJEE,
                            'size' => Select2::MEDIUM,
                            'language' => 'en',
                            'options' => [
                                'id' => 'status'
                            ],
                            'pluginOptions' => [
                                'placeholder' => 'Select Status..'
                            ],
                        ]
                    ),
                    [
                        'class' => 'col form-group',
                        'style' => 'padding: 2px 5px 2px 5px'
                    ]
                ) .
                Html::tag(
                    'div',
                    $form->field($model, 'rating')->widget(
                        Select2::classname(),
                        [
                            'data' => [
                                1 => 'Very Poor (1 Star)',
                                2 => 'Poor (2 Stars)',
                                3 => 'Satisfactory (3 Stars)',
                                4 => 'Good (4 Stars)',
                                5 => 'Very Good (5 Stars)'
                            ],
                            'theme' => Select2::THEME_KRAJEE,
                            'size' => Select2::MEDIUM,
                            'language' => 'en',
                            'options' => [
                                'id' => 'rating'
                            ],
                            'pluginOptions' => [
                                'placeholder' => 'Select Ratings..'
                            ],
                        ]
                    ),
                    [
                        'class' => 'col form-group',
                        'style' => 'padding: 2px 5px 2px 5px'
                    ]
                ) .
                Html::tag(
                    'div',
                    $form->field($model, 'send_to_sdm')->widget(
                        Select2::classname(),
                        [
                            'data' => [
                                0 => 'No',
                                1 => 'Yes'
                            ],
                            'theme' => Select2::THEME_KRAJEE,
                            'size' => Select2::MEDIUM,
                            'language' => 'en',
                            'options' => [
                                'id' => 'send_to_sdm',
                                'placeholder' => 'Sent to SDM.?',
                                'class' => 'form-control'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]
                    ),
                    [
                        'class' => 'col form-group',
                        'style' => 'padding: 2px 5px 2px 5px'
                    ]
                ),
            [
                'class' => 'row',
                'style' => 'padding: 0px 2px 0px 2px'
            ]
        ) .
        Html::tag(
            'div',
            Html::tag(
                'div',
                '',
                [
                    'class' => 'col-8 form-group',
                    'style' => 'padding: 2px 5px 2px 5px'
                ]
            ) .
                Html::tag(
                    'div',
                    Html::submitButton(
                        Yii::t('app', 'Search'),
                        [
                            'class' => 'btn btn-success form-control',
                            'style' => 'padding: 8px; margin: 5px 2px 1px 2px'
                        ]
                    ),
                    [
                        'class' => 'col-2 text-right form-group',
                        'style' => 'padding: 2px 5px 2px 5px'
                    ]
                ) .
                Html::tag(
                    'div',
                    Html::resetButton(
                        Yii::t('app', 'Reset'),
                        [
                            'id' => 'resetButton',
                            'class' => 'btn btn-secondary form-control',
                            'style' => 'padding: 8px; margin: 5px 2px 1px 2px'
                        ]
                    ),
                    [
                        'class' => 'col-2 text-right form-group',
                        'style' => 'padding: 2px 5px 2px 5px'
                    ]
                ),
            [
                'class' => 'row',
                'style' => 'padding: 0px 2px 0px 2px'
            ]
        );
ActiveForm::end();

$jsScript = <<< JS
// JS Code
    var timerid = 0;
    $(function(e) {
        $("#resetButton").on("click", function(e) {
            $("#region").val(null).trigger('change');
            $("#district").val(null).trigger('change');
            $("#area").val(null).trigger('change');
            $("#street").val(null).trigger('change');
            $("#customer_id").val(null).trigger('change');
            $("#service_id").val(null).trigger('change');
            $("#sub_service_id").val(null).trigger('change');
            $("#status").val(null).trigger('change');
            $("#rating").val(null).trigger('change');
            $("#send_to_sdm").val(null).trigger('change');
        });
    });
JS;
$this->registerJs($jsScript);
