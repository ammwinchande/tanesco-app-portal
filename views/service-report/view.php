<?php

use yii\helpers\Html;
use app\models\Service;
use app\models\SubService;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ServiceReport */

$service = Service::find()->select(['name'])->where(['=', 'id', $model->service_id])->distinct()->one();
$sub_service = SubService::find()->select(['name'])->where(['=', 'id', $model->sub_service_id])->distinct()->one();

$this->title = $service['name'] . ' - ' . ucwords($sub_service['name']) . ' / ' . $model->service_no;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service Reports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$id = 'service-report';
?>
<div class="service-report-view no-print">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('../common/top-buttons.php', ['id' => $id, 'model' => $model]) ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'details:ntext',
            'meter_no',
            'coverage',
            'service_no',
            'picture',
            'video',
            'remarks',
            'rating',
            'feedback',
            'services.name',
            'subServices.name',
            'clients.name',
            'regions.name',
            'districts.name',
            'areas.name',
            'streets.name',
            'special_mark',
            'status',
            [
                'attribute' => 'send_to_sdm',
                'format' => 'raw',
                'value' => $model->send_to_sdm == 0 ? 'No' : 'Yes',
            ],
            'reported_id',
            'resolved_id',
        ]
    ]);
    ?>
</div>
<?= $this->render('../common/qr-code.php', ['model' => $model]) ?>
