<?php

namespace app\modules\api\controllers;

use Yii;
use yii\helpers\Json;
use app\models\Client;
use app\models\Region;
use app\models\District;
use app\models\LocArea;
use app\models\LocStreet;
use app\models\Service;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;
use app\models\ServiceReport;
use DateTime;
use yii\db\Expression;

/**
 * Service controller for the `api` module
 */
class ServiceController extends ActiveController
{
    public $modelClass = "\app\models\Service";
    public $isMobile = 'TRUE';

    /**
     * Renders the index view for the module
     * @return string
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionIndex()
    {
        return new DateTime('now');
        return Service::find()->select(['id', 'name', 'name_sw', 'service_icon'])->all();
    }

    /**
     * Get service names from SDM
     * store them to localDB
     *
     * @return \yii\helpers\Json $response
     */
    public function actionGetServices()
    {
        $services = Yii::$app->soapclient->getServiceNames();
        return $services;
    }


    /**
     * Submitting report for getting Reference number from
     * SDM and store them locally
     *
     * @return \yii\helpers\Json $response
     */
    public function actionReport()
    {
        if (!Yii::$app->request->isPost) {
            throw new MethodNotAllowedHttpException('Error! Method not allowed', 1);
        } else {
            $request = Yii::$app->request->getRawBody();
            $reported_issue = Json::decode($request);

            $client = Client::find()
                ->where(['=', 'device_id', $reported_issue['device_id']])
                ->andWhere(['=', 'mobile_no', $reported_issue['mobile_no']])
                ->one();

            if (!$client) {
                throw new ForbiddenHttpException('Error! You are not authorised to access this service', 1); //not valid client
            } else {
                $service_report = new ServiceReport();

                // GET correct location names for sending to SDM
                $region = null;
                $district = null;
                $area = null;
                $street = null;
                $special_mark = $reported_issue['special_mark'];
                $service = ServiceReport::getService($reported_issue['service_id']);
                $sub_service = null;

                $service_name = explode(" ", $service->name, 2)[0];

                if (strtolower($service_name) != 'complaint') {
                    // UPDATE to check if service already reported by the same customer
                    // with status not resolved/completed
                    $exists_in_reported = ServiceReport::find()
                        ->where(['=', 'customer_id', $client->id])
                        ->andWhere(['=', 'service_id', $reported_issue['service_id']])
                        ->andWhere(['=', 'sub_service_id', $reported_issue['sub_service_id']])
                        ->andWhere(['=', 'region', $reported_issue['region']])
                        ->andWhere(['=', 'district', $reported_issue['district']])
                        ->andWhere(['=', 'area', $reported_issue['area']])
                        ->andWhere(['=', 'street', $reported_issue['street']])
                        ->andWhere(['in', 'status', ['received', 'waiting', 'in progress', 'referred']])
                        ->one();

                    if ($exists_in_reported) {
                        $reference_no = $exists_in_reported->service_no;
                        $status = $this->customerStatus($exists_in_reported->status);
                        return Json::encode(['status' => 'success', 'data' => $reference_no . ' : ' . $status], JSON_FORCE_OBJECT);
                    }

                    $sub_service = ServiceReport::getSubService($reported_issue['sub_service_id']);
                    // check if meter number is to be set as primary
                    if ($reported_issue['is_primary']) {
                        $client->meter_no = $reported_issue['meter_number'];
                        $client->save(false);
                    }

                    $region = Region::find()->select(['name'])->where('id=:id', [':id' => $reported_issue['region']])->scalar();
                    $district = District::find()->select(['name'])->where('id=:id', [':id' => $reported_issue['district']])->scalar();
                    $area = LocArea::find()->select(['name'])->where('id=:id', [':id' => $reported_issue['area']])->scalar();
                    $street = LocStreet::find()->select(['name'])->where('id=:id', [':id' => $reported_issue['street']])->scalar();
                    $special_mark = $reported_issue['special_mark'];

                    $service_report->customer_id = $client->id;
                    $service_report->region = $reported_issue['region'];
                    $service_report->district = $reported_issue['district'];
                    $service_report->area = $reported_issue['area'];
                    $service_report->street = $reported_issue['street'];
                    $service_report->special_mark = $reported_issue['special_mark'];
                    // $service_report->coverage = $reported_issue['coverage']; // this should be based on pre-defined ones from DB
                    $service_report->service_id = $reported_issue['service_id'];
                    $service_report->sub_service_id = $reported_issue['sub_service_id'];
                    $service_report->details = $reported_issue['details'];
                    $service_report->picture = $reported_issue['picture'];
                    $service_report->video = $reported_issue['video'];
                    $service_report->meter_no = $reported_issue['meter_number'];
                } else {
                    $previous_service_report = ServiceReport::find()->where(['=', 'service_no', $reported_issue['service_no']])->one();

                    // UPDATE to check if service already reported by the same customer with status not resolved/completed
                    $exists_in_reported = ServiceReport::find()
                        ->where(['=', 'customer_id', $client->id])
                        ->andWhere(['=', 'service_id', $reported_issue['service_id']])
                        ->andWhere(['=', 'sub_service_id', $previous_service_report->sub_service_id])
                        ->andWhere(['=', 'region', $reported_issue['region']])
                        ->andWhere(['=', 'district', $reported_issue['district']])
                        ->andWhere(['=', 'area', $reported_issue['area']])
                        ->andWhere(['=', 'street', $reported_issue['street']])
                        ->andWhere(['in', 'status', ['received', 'waiting', 'in progress', 'referred']])
                        ->one();

                    if ($exists_in_reported) {
                        $reference_no = $exists_in_reported->service_no;
                        $status = $this->customerStatus($exists_in_reported->status);
                        return Json::encode(['status' => 'success', 'data' => $reference_no . ' : ' . $status], JSON_FORCE_OBJECT);
                    }

                    $region = Region::find()->select(['name'])->where('id=:id', [':id' => $previous_service_report->region])->scalar();
                    $district = District::find()->select(['name'])->where('id=:id', [':id' => $previous_service_report->district])->scalar();
                    $area = LocArea::find()->select(['name'])->where('id=:id', [':id' => $previous_service_report->area])->scalar();
                    $street = LocStreet::find()->select(['name'])->where('id=:id', [':id' => $previous_service_report->street])->scalar();
                    $special_mark = $previous_service_report->special_mark;

                    $sub_service = ServiceReport::getSubService($previous_service_report->sub_service_id);

                    $service_report->customer_id = $client->id;
                    $service_report->region = $previous_service_report->region;
                    $service_report->district = $previous_service_report->district;
                    $service_report->area = $previous_service_report->area;
                    $service_report->street = $previous_service_report->street;
                    $service_report->special_mark = $previous_service_report->special_mark;
                    $service_report->coverage = $previous_service_report->coverage;
                    $service_report->service_id = $reported_issue['service_id'];
                    $service_report->sub_service_id = $previous_service_report->sub_service_id;
                    $service_report->details = $reported_issue['details'];
                    $service_report->picture = $previous_service_report->picture;
                    $service_report->video = $previous_service_report->video;
                    $service_report->meter_no = $previous_service_report->meter_no;
                }

                $service_report_data = [
                    'Service_Name' => $service_name,
                    'Region' => strtoupper($region),
                    'District' => strtoupper($district),
                    'Area' => strtoupper($area),
                    'Street' => strtoupper($street),
                    'Special_Mark' => $special_mark,
                    'Problem' => $sub_service->name,
                    'Problem_Details' => $reported_issue['details'],
                    'Mobile_Number' => $client->mobile_no,
                    'Customer_Name' => $client->name,
                    'Meter_Number' => $reported_issue['meter_number'] == null ? ($client->meter_no != null ? $client->meter_no : '') : $reported_issue['meter_number'],
                    'Previous_ServiceNumber' => $reported_issue['service_no'] == null ? '' : $reported_issue['service_no'],
                    'Is_Mobile' => $this->isMobile
                ];

                $reference_no = Yii::$app->toSdm->send($service_report_data);

                if ($reference_no) {
                    $service_report->service_no = $reference_no;
                    $service_report->status = 'received';
                    $service_report->send_to_sdm = 1;
                    $service_report->save(false);
                    // $this->pushSMS($client->mobile_no, $reference_no);
                    return Json::encode(['status' => 'success', 'data' => $reference_no . ':'], JSON_FORCE_OBJECT);
                } else if (empty($reference_no)) {
                    return Json::encode(['status' => 'fail', 'data' => 'validate meter number, then try again!:'], JSON_FORCE_OBJECT);
                } else {
                    return Json::encode(['status' => 'fail', 'data' => ''], JSON_FORCE_OBJECT);
                }
            }
        }
    }

    /**
     * Store reported complaint
     *
     * @return \yii\helpers\Json $response
     */
    public function actionReportComplaint()
    {
        if (!Yii::$app->request->isPost) {
            throw new MethodNotAllowedHttpException('Error! Method not allowed', 1);
        } else {
            $request = Yii::$app->request->getRawBody();
            $reported_issue = Json::decode($request);
            $client_exists = Client::find()
                ->where(['=', 'device_id', $reported_issue['device_id']])
                ->andWhere(['=', 'mobile_no', $reported_issue['mobile_no']])
                ->exists();

            if (!$client_exists) {
                throw new ForbiddenHttpException('Error! You are not authorised to access this service', 1);
            } else {
                $previous_service_report = ServiceReport::find()->where(['=', 'service_no', $reported_issue['service_no']])->one();

                $client_id = Client::find()
                    ->select('id')
                    ->where(['=', 'device_id', $reported_issue['device_id']])
                    ->andWhere(['=', 'mobile_no', $reported_issue['mobile_no']])
                    ->scalar();

                // UPDATE to check if service already reported by the same customer with status not resolved/completed
                $exists_in_reported = ServiceReport::find()
                    ->where(['=', 'customer_id', $client_id])
                    ->andWhere(['=', 'sub_service_id', $reported_issue['sub_service_id']])
                    ->andWhere(['=', 'region', $reported_issue['region']])
                    ->andWhere(['=', 'district', $reported_issue['district']])
                    ->andWhere(['=', 'area', $reported_issue['area']])
                    ->andWhere(['=', 'street', $reported_issue['street']])
                    ->andWhere(['in', 'status', ['received', 'waiting', 'in progress', 'referred']])
                    ->one();

                if ($exists_in_reported) {
                    $reference_no = $exists_in_reported->service_no;
                    $status = $this->customerStatus($exists_in_reported->status);
                    return Json::encode(['status' => 'success', 'data' => $reference_no . ' : ' . $status], JSON_FORCE_OBJECT);
                }

                $service_report = new ServiceReport();
                $service_report->customer_id = Client::find()->select('id')->where(['=', 'device_id', $reported_issue['device_id']])->scalar();
                $service_report->region = $previous_service_report->region;
                $service_report->district = $previous_service_report->district;
                $service_report->area = $previous_service_report->area;
                $service_report->street = $previous_service_report->street;
                $service_report->special_mark = $previous_service_report->special_mark;
                // $service_report->coverage = $reported_issue['coverage']; // this should be based on pre-defined ones from DB
                $service_report->service_id = $reported_issue['service_id'];
                $service_report->sub_service_id = $previous_service_report->sub_service_id;
                $service_report->details = $reported_issue['details'];
                $service_report->picture = $previous_service_report->picture;
                $service_report->video = $previous_service_report->video;
                $service_report->meter_no = $previous_service_report->meter_no;
                $service_report->status = 'received';

                // Save to API DB
                $save = $service_report->save(false);
                $service_report_id = $service_report->id;

                // send to SDM
                // GET correct location names for sending to SDM
                $region = Region::find()->select(['name'])->where('id=:id', [':id' => $service_report->region])->scalar();
                $district = District::find()->select(['name'])->where('id=:id', [':id' => $service_report->district])->scalar();
                $area = LocArea::find()->select(['name'])->where('id=:id', [':id' => $service_report->area])->scalar();
                $street = LocStreet::find()->select(['name'])->where('id=:id', [':id' => $service_report->street])->scalar();
                $special_mark = $service_report->special_mark;
                $client = ServiceReport::getClient($service_report->customer_id);
                $sub_service = ServiceReport::getSubService($service_report->sub_service_id);
                $service = ServiceReport::getService($service_report->service_id);

                $service_report_data = [
                    'Service_Name' => $service->name,
                    'Region' => strtoupper($region),
                    'District' => strtoupper($district),
                    'Area' => strtoupper($area),
                    'Street' => strtoupper($street),
                    'Special_Mark' => $special_mark == null ? '' : $special_mark,
                    'Problem' => $sub_service->name,
                    'Problem_Details' => $service_report->details,
                    'Mobile_Number' => $client->mobile_no,
                    'Customer_Name' => $client->name,
                    'Meter_Number' => $previous_service_report->meter_no == null ? ($client->meter_no != null ? $client->meter_no : '') : $previous_service_report->meter_no,
                    'Previous_ServiceNumber' => $reported_issue['service_no'] == null ? '' : $reported_issue['service_no'],
                    'Is_Mobile' => $this->isMobile
                ];

                $reference_no = Yii::$app->toSdm->send($service_report_data);

                if ($save && $reference_no) {
                    $service_report = ServiceReport::findOne($service_report_id);
                    $service_report->service_no = $reference_no;
                    $service_report->send_to_sdm = 1;
                    $service_report->save(false);
                    // send message to user with TB Number
                    // $this->pushSMS($client->mobile_no, $reference_no);
                    return Json::encode(['status' => 'success', 'data' => $reference_no . ':'], JSON_FORCE_OBJECT);
                } else {
                    return Json::encode(['status' => 'fail', 'data' => ''], JSON_FORCE_OBJECT);
                }
            }
        }
    }

    /**
     * Get status from SDM
     *
     * @param
     * @return Json    $response
     */
    public function actionGetStatus()
    {
        if (!Yii::$app->request->isPost) {
            throw new MethodNotAllowedHttpException('Error! Method not allowed', 1);
        } else {
            $request = Yii::$app->request->getRawBody();
            $service = Json::decode($request);
            $device_id = $service['device_id'];
            $mobile_no = $service['mobile_no'];
            $service_no = $service['service_no'];

            if (!Client::find()->where(['=', 'device_id', $device_id])->andWhere(['=', 'mobile_no', $mobile_no])->exists()) {
                //not valid client
                throw new ForbiddenHttpException('Error! You are not authorised to access this service', 1);
            } else {
                $service_report = ServiceReport::find()->where('service_no = :sno', [':sno' => $service_no])->one();
                if ($service_report) {
                    $status_from_sdm = strtolower(Yii::$app->soapclient->getServiceNumberStatus($service_no)->Service_Status);
                    // capture the remarks that comes with status, it contains -
                    if (strpos($status_from_sdm, '-')) {
                        $statement = explode('-', $status_from_sdm);
                        $service_report->status = $statement[0];
                        $service_report->remarks = empty($statement[1]) ? null : $statement[1];
                    } else {
                        $service_report->status = $status_from_sdm;
                    }
                    $service_report->updated_at = date('Y-m-d H:i:s');
                    strtolower($service_report->status) == 'resolved' ? $service_report->resolved_at = date('Y-m-d H:i:s') : null;
                    if ($service_report->save(false)) {
                        return Json::encode(['status' => 'success', 'data' => $this->customerStatus($service_report->status)], JSON_FORCE_OBJECT);
                    } else {
                        return Json::encode(['status' => 'fail', 'data' => ''], JSON_FORCE_OBJECT);
                    }
                } else {
                    return Json::encode(['status' => 'fail', 'data' => ''], JSON_FORCE_OBJECT);
                }
            }
        }
    }

    /**
     * Create correct status for customers
     *
     * @param String $status
     * @return String $status
     */
    private function customerStatus($status)
    {
        switch (strtolower($status)) {
            case 'waiting':
                return 'in progress';
                break;
            case 'in progress':
                return strtolower($status);
                break;
            case 'referred':
                return 'in progress';
                break;
            case 'resolved':
                return strtolower($status);
                break;
            case 'closed':
                return strtolower($status);
                break;
            default:
                return 'received';
                break;
        }
    }

    /**
     * Store Feedback from client
     * @return Json $response
     */
    public function actionAddFeedback()
    {
        if (!Yii::$app->request->isPost) {
            throw new MethodNotAllowedHttpException('Error! Method not allowed', 1);
        } else {
            $request = Yii::$app->request->getRawBody();

            $service = Json::decode($request);
            $device_id = $service['device_id'];
            $service_no = $service['service_no'];
            $rating = $service['rating'];
            $feedback = $service['feedback'];

            if (!Client::find()->where(['=', 'device_id', $device_id])->exists()) {
                //not valid client
                throw new ForbiddenHttpException('Error! You are not authorised to access this service', 1);
            } else {
                $service_report = ServiceReport::find()->where('service_no = :sno', [':sno' => $service_no])->one();
                if ($service_report) {
                    $service_report->rating = $rating;
                    $service_report->feedback = $feedback;
                    $service_report->updated_at = date('Y-m-d H:i:s');
                    $service_report->status = 'closed';
                    if ($service_report->save(false)) {
                        return Json::encode(['status' => 'success', 'data' => 'feedback successfully updated'], JSON_FORCE_OBJECT);
                    } else {
                        return Json::encode(['status' => 'fail', 'data' => 'failt to store your feedback'], JSON_FORCE_OBJECT);
                    }
                } else {
                    return Json::encode(['status' => 'fail', 'data' => 'Something went really wrong'], JSON_FORCE_OBJECT);
                }
            }
        }
    }

    /**
     * List all Reported Services/Emergencies/Requests etc.
     * @return Json $response
     */
    public function actionServicesSummary()
    {
        $post = Yii::$app->request->post();
        if (isset($post)) {
            $monthly = ['FromDate' => $post['from'], 'ToDate' => $post['to']];
            $today = ['FromDate' => $post['today'], 'ToDate' => $post['today']];
            $array = [$monthly, $today];
            $jsonDada = [];
            $i = 0;
            while ($i <= 1) {
                $data = Yii::$app->soapclient->getServicesSummary($array[$i]);
                $services = [];
                $list = [];
                $attr = [];
                foreach ($data as $key => $service) {
                    $services[$key] = Yii::$app->formatData->xml2array($service);
                    foreach ($services[$key] as $k => $v) {
                        $attr[$k] = $v;
                    }
                    $list[$key] = $attr;
                }
                ($i == 0) ? $jsonDada['monthly'] = $list : $jsonDada['today'] = $list;
                $i++;
            }
        } else {
            $jsonDada = 'Something went wrong!';
        }
        return Json::encode($jsonDada);
    }

    public function actionTest()
    {
        // $params = [
        //     "FromDate" => "16-08-2019",
        //     "ToDate" => "16-08-2019",
        //     "Scope"   => ""
        // ];
        $params = ['DODOMA', 'MOROGORO', 'TANGA', 'MWANZA', 'TEMEKE'];
        foreach ($params as $param) {
            $data[$param] = Yii::$app->soapclient->getDistricts(['Region' => $param]);
        }
        return $data;
    }

    /**
     * Return the summary of all reported TB's and requests
     * by a specific user - convert to post
     *
     * @return Json $response
     */
    public function actionReportSummary($mobile_no, $days = null)
    {
        $client_ids = [];
        $client_ids = Client::find()
            ->select(['id'])
            ->where(['=', 'mobile_no', $mobile_no])
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $report_summary = [];

        foreach ($client_ids as $client) {
            $service_reports = ServiceReport::find()
                ->select(['id', 'details', 'service_id', 'sub_service_id',  'service_no', 'status', 'remarks', 'feedback', 'rating', 'created_at', 'resolved_at'])->where(['=', 'customer_id', $client->id])
                ->orderBy(['id' => SORT_DESC])
                ->all();
            foreach ($service_reports as $service_report) {
                $service = ServiceReport::getService($service_report->service_id);
                $sub_service = ServiceReport::getSubService($service_report->sub_service_id);

                // return all services that are within $days from now for complaint
                if ($days != null && $days != 0) {
                    if (strtotime($this->createFormattedDate($service_report->created_at)) > strtotime("-{$days} days")) {
                        array_push($report_summary, [
                            'id' => $service_report->id,
                            'service_id' => $service_report->service_id,
                            'service_name' => $service->name,
                            'sub_service_id' => $service_report->sub_service_id,
                            'sub_service_name' => $sub_service->name,
                            'is_meter_required' => $sub_service->is_meter_required,
                            'details' => $service_report->details,
                            'service_no' => $service_report->service_no,
                            'status' => $this->customerStatus($service_report->status),
                            'remarks' => $service_report->remarks,
                            'feedback' => $service_report->feedback,
                            'rating' => $service_report->rating,
                            'created_at' => $service_report->created_at == null ? null : $this->createFormattedDate($service_report->created_at),
                            'resolved_at' => $service_report->resolved_at == null ? null : $this->createFormattedDate($service_report->resolved_at),
                        ]);
                    }
                } else {
                    array_push($report_summary, [
                        'id' => $service_report->id,
                        'service_id' => $service_report->service_id,
                        'service_name' => $service->name,
                        'sub_service_id' => $service_report->sub_service_id,
                        'sub_service_name' => $sub_service->name,
                        'is_meter_required' => $sub_service->is_meter_required,
                        'details' => $service_report->details,
                        'service_no' => $service_report->service_no,
                        'status' => $this->customerStatus($service_report->status),
                        'remarks' => $service_report->remarks,
                        'feedback' => $service_report->feedback,
                        'rating' => $service_report->rating,
                        'created_at' => $service_report->created_at == null ? null : $this->createFormattedDate($service_report->created_at),
                        'resolved_at' => $service_report->resolved_at == null ? null : $this->createFormattedDate($service_report->resolved_at),
                    ]);
                }
            }
        }

        return $report_summary;
    }

    private function createFormattedDate($time)
    {
        $time = strtotime($time);
        $correct_date = date('Y-m-d H:i:s', $time);
        return $correct_date;
    }

    /**
     * This is testing for reported services
     * return all reported services with status of resolved and
     * having updated at i.e. within 24 hours from now()
     * set the cronjob to run every hour
     *
     * @return
     */
    public function actionCloseResolvedServices()
    {
        $data = [];
        $service_reports = ServiceReport::find()
            ->where(['=', 'status', 'resolved'])
            ->andWhere(['<=', 'updated_at', new Expression('DATE_SUB(NOW(), INTERVAL 24 HOUR)')])
            ->all();
        if ($service_reports != null) {
            foreach ($service_reports as $service_report) {
                array_push($data, $service_report);
            }
        }
        return $data;
    }


    /**
     * Sends message to client based on mobile number
     *
     * @return Json $result
     */
    private function pushSMS($mobile_no, $message)
    {
        $url = 'http://192.168.1.92/v1/push/send-sms.html?mobileno=' . $mobile_no . '&message=' . $message;
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = Json::decode(curl_exec($ch), true);
        return json_decode($result);
    }
}
