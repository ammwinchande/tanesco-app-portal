<?php

namespace app\modules\api\controllers;

use Yii;
use yii\helpers\Json;
use app\models\Client;
use app\models\ClientDevice;
use app\models\ClientWaitingVerification;
use yii\rest\ActiveController;
use yii\web\MethodNotAllowedHttpException;
use app\models\RegistrationCode as VerificationToken;

/**
 * Default controller for the `api` module
 */
class ClientController extends ActiveController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public $modelClass = "app\models\Client";
    public $registered_message = 'registered';
    public $waiting_message = 'waiting';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = \yii\web\Response::FORMAT_JSON;
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    /**
     * Test distict
     *
     * @return \yii/web/Response::json $response
     */
    public function actionDistinctClients()
    {
        $client_ids = $this->modelClass::find()->select(['id', 'name'])->distinct()->all();
        return $client_ids;
    }


    public function actionIndex($device_id = null)
    {
        $device_id == null ? $clients = $this->modelClass::find()->orderBy('id')->all() : $clients = $this->modelClass::find()->where(['=', 'device_id', $device_id])->one();
        return $clients;
    }

    /**
     * Return verification message
     *
     * @return /yii/web/Response::json $response
     */
    public function actionVerifyMeter($meter_number)
    {
        $response = Yii::$app->soapclient->getMeterNoVerification($meter_number);
        return strlen($response) == 0 ? null : [$response];
    }

    public function actionRequestVerificationToken($mobile_no)
    {
        return VerificationToken::sendVerificationToken($mobile_no);
    }

    public function actionVerifyToken($token, $mobile_no, $device_id)
    {
        if (VerificationToken::validateVerificationToken($token, $mobile_no)) {
            $client_waiting_verification = ClientWaitingVerification::find()
                ->where(['=', 'mobile_no', $mobile_no])
                ->andWhere(['=', 'device_id', $device_id])
                ->one();

            $registered_client = Client::find()
                ->where(['=', 'mobile_no', $mobile_no])
                ->one();

            if ($registered_client) {
                $client_device = new ClientDevice();

                $client_device->client_id = $registered_client->id;
                $client_device->device_id = $client_waiting_verification->device_id;
                if ($client_device->save(false)) {
                    $client_waiting_verification->delete($mobile_no, $device_id);
                    VerificationToken::delete($client_waiting_verification->mobile_no);
                    return Json::encode(['status' => 'success', 'data' => $registered_client], JSON_FORCE_OBJECT);
                } else {
                    return Json::encode(['status' => 'success', 'data' => null], JSON_FORCE_OBJECT);
                }
            }
        } else {
            return Json::encode(['status' => 'fail', 'data' => 'Token is not Vaild']);
        }
    }

    /**
     * PERFORM user login
     *
     * @return \yii\helpers\Json $json
     */
    public function actionLogin()
    {
        $now = (new \DateTime('now'));

        if (!\yii::$app->request->isPost) {
            throw new \yii\web\MethodNotAllowedHttpException('Error! Method not allowed', 1);
        } else {
            $request = \Yii::$app->request->getRawBody();
            if (is_null($request)) {
                return Json::encode(['status' => 'fail', 'data' => null], JSON_FORCE_OBJECT);
            }
            $client_info = Json::decode($request);

            $mobile_no = $client_info['mobile_no'];
            $device_id = $client_info['device_id'];

            $session_start = $now->format('Y-m-d H:i:s');
            $session_end = $now->modify('+180 days')->format('Y-m-d H:i:s');

            // check if client exists
            $client = Client::find()
                ->where(['=', 'mobile_no', $mobile_no])
                ->andWhere(['=', 'conn_status', 1])
                ->one();


            // check if that client has that device
            $client_device = ClientDevice::find()
                ->where(['=', 'client_id', $client->id])
                ->andWhere(['=', 'device_id', $device_id])
                ->one();

            if ($client_device) {
                // $this->pushSMS($client->mobile_no, VerificationToken::GenerateVerificationToken($client->mobile_no));
                return Json::encode(['status' => 'success', 'data' => $client], JSON_FORCE_OBJECT);
            } else {
                $previous_client_info = Client::find()
                    ->where(['=', 'mobile_no', $mobile_no])
                    ->one();

                if ($previous_client_info) {
                    // check if client exists in waiting verification
                    $client_exists_in_waiting = ClientWaitingVerification::find()
                        ->where(['=', 'mobile_no', $mobile_no])
                        ->andWhere(['=', 'device_id', $device_id])
                        ->one();

                    if ($client_exists_in_waiting) {
                        $client_exists_in_waiting->session_start_date = $session_start;
                        $client_exists_in_waiting->session_end_date = $session_end;
                        if ($client_exists_in_waiting->save(false)) {
                            // $this->pushSMS($client_exists_in_waiting->mobile_no, VerificationToken::GenerateVerificationToken($client_exists_in_waiting->mobile_no));
                            VerificationToken::GenerateVerificationToken($client_exists_in_waiting->mobile_no);
                            return Json::encode(['status' => 'success', 'data' => $this->waiting_message], JSON_FORCE_OBJECT);
                        }
                    }

                    $client_waiting_verification = new ClientWaitingVerification();

                    $client_waiting_verification->meter_no = $previous_client_info->meter_no;
                    $client_waiting_verification->mobile_no = $mobile_no;
                    $client_waiting_verification->name = $previous_client_info->name;
                    $client_waiting_verification->device_id = $device_id;
                    $client_waiting_verification->region_id = $previous_client_info->region_id;
                    $client_waiting_verification->district_id = $previous_client_info->district_id;
                    $client_waiting_verification->area_id = $previous_client_info->area_id;
                    $client_waiting_verification->street_id = $previous_client_info->street_id;
                    $client_waiting_verification->special_mark = $previous_client_info->special_mark;
                    $client_waiting_verification->session_start_date = $session_start;
                    $client_waiting_verification->session_end_date = $session_end;
                    $client_waiting_verification->lang_used   = $previous_client_info->lang_used;
                    $client_waiting_verification->is_blocked = $previous_client_info->is_blocked;
                    $client_waiting_verification->conn_status = $previous_client_info->conn_status;
                    if ($client_waiting_verification->save(false)) {
                        // $this->pushSMS($client_waiting_verification->mobile_no, VerificationToken::GenerateVerificationToken($client_waiting_verification->mobile_no));
                        VerificationToken::GenerateVerificationToken($client_waiting_verification->mobile_no);
                        return Json::encode(['status' => 'success', 'data' => $this->waiting_message], JSON_FORCE_OBJECT);
                    } else {
                        return Json::encode(['status' => 'fail', 'data' => null], JSON_FORCE_OBJECT);
                    }
                } else {
                    return Json::encode(['status' => 'fail', 'data' => null], JSON_FORCE_OBJECT);
                }
            }
        }
    }

    public function actionRegister()
    {
        $now = (new \DateTime('now'));

        if (!\yii::$app->request->isPost) {
            throw new \yii\web\MethodNotAllowedHttpException('Error! Method not allowed', 1);
        } else {
            $request = \Yii::$app->request->getRawBody();
            if (is_null($request)) {
                return Json::encode(['status' => 'fail', 'data' => null], JSON_FORCE_OBJECT);
            }
            $client_info = Json::decode($request);

            $meter_no = $client_info['meter_no'];
            $mobile_no = $client_info['mobile_no'];
            $name = $client_info['name'];
            $device_id = $client_info['device_id'];
            $region_id = $client_info['region_id'];
            $district_id = $client_info['district_id'];
            $area_id = $client_info['area_id'];
            $street_id = $client_info['street_id'];
            $special_mark = $client_info['special_mark'];
            $lang_used   = $client_info["lang_used"];
            $session_start = $now->format('Y-m-d H:i:s');
            $session_end = $now->modify('+180 days')->format('Y-m-d H:i:s');

            // check if already registered
            $client = Client::find()
                ->where(['=', 'mobile_no', $mobile_no])
                ->one();

            $device_registered = ClientDevice::find()
                ->where(['=', 'client_id', $client->id])
                ->andWhere(['=', 'device_id', $device_id])
                ->one();

            if ($device_registered) {
                return Json::encode(['status' => 'success', 'data' => $this->registered_message], JSON_FORCE_OBJECT);
            } else {
                // check if mobile_no exists with other devices
                if ($client) {
                    return Json::encode(['status' => 'success', 'data' => $this->registered_message], JSON_FORCE_OBJECT);
                }

                // check if client exists in waiting verification
                $client_exists_in_waiting = ClientWaitingVerification::find()
                    ->where(['=', 'mobile_no', $client->mobile_no])
                    ->andWhere(['=', 'device_id', $client->device_id])
                    ->one();

                if ($client_exists_in_waiting) {
                    $client_exists_in_waiting->session_start_date = $session_start;
                    $client_exists_in_waiting->session_end_date = $session_end;
                    if ($client_exists_in_waiting->save(false)) {
                        // $this->pushSMS($client_exists_in_waiting->mobile_no, VerificationToken::GenerateVerificationToken($client_exists_in_waiting->mobile_no));
                        VerificationToken::GenerateVerificationToken($client_exists_in_waiting->mobile_no);
                        return Json::encode(['status' => 'success', 'data' => $client_exists_in_waiting], JSON_FORCE_OBJECT);
                    }
                }

                $client_waiting_verification = new ClientWaitingVerification();

                $client_waiting_verification->name = $name;
                $client_waiting_verification->meter_no = $meter_no;
                $client_waiting_verification->mobile_no = $mobile_no;
                $client_waiting_verification->device_id = $lang_used;
                $client_waiting_verification->device_id = $device_id;
                $client_waiting_verification->region_id = $region_id;
                $client_waiting_verification->district_id = $district_id;
                $client_waiting_verification->area_id = $area_id;
                $client_waiting_verification->street_id = $street_id;
                $client_waiting_verification->special_mark = $special_mark;
                $client_waiting_verification->session_start_date = $session_start;
                $client_waiting_verification->session_end_date = $session_end;
                $client_waiting_verification->conn_status = 1;

                if ($client_waiting_verification->save(false)) {
                    // $this->pushSMS($client_waiting_verification->mobile_no, VerificationToken::GenerateVerificationToken($client_waiting_verification->mobile_no));
                    VerificationToken::GenerateVerificationToken($client_waiting_verification->mobile_no);
                    return Json::encode(['status' => 'success', 'data' => $this->waiting_message], JSON_FORCE_OBJECT);
                } else {
                    return Json::encode(['status' => 'fail', 'data' => null], JSON_FORCE_OBJECT);
                }
            }
        }
    }

    /**
     * Update Client location details
     *
     * @return \yii\helpers\Json $response
     */
    public function actionUpdateProfile()
    {
        if (!Yii::$app->request->isPost) {
            throw new MethodNotAllowedHttpException('Error! Method not allowed', 1);
        } else {
            $request = Yii::$app->request->getRawBody();
            if (is_null($request)) {
                return 'NULL';
            }
            $client_info = Json::decode($request);
            $meter_no = $client_info['meter_no'];
            $mobile_no = $client_info['mobile_no'];
            $device_id = $client_info['device_id'];
            $region_id = $client_info['region_id'];
            $district_id = $client_info['district_id'];
            $area_id = $client_info['area_id'];
            $street_id = $client_info['street_id'];
            $special_mark = $client_info['special_mark'];

            $client = Client::find()->where(['=', 'mobile_no', $mobile_no])->one();

            $registered_device = ClientDevice::find()
                ->where(['=', 'client_id', $client->id])
                ->andWhere(['=', 'device_id', $device_id])
                ->one();

            if (!$registered_device) {
                return Json::encode(['fail' => 'success', 'data' => 'Client doesn\'t exist']);
            }
            $client->meter_no = $meter_no;
            $client->region_id = $region_id;
            $client->district_id = $district_id;
            $client->area_id = $area_id;
            $client->street_id = $street_id;
            $client->special_mark = $special_mark;

            if ($client->save(false)) {
                return Json::encode(['status' => 'success', 'data' => 'profile has been updated']);
            }
            return Json::encode(['status' => 'fail', 'data' => 'failed to updated']);
        }
    }

    /**
     * Update Client meter number details
     *
     * @return \yii\helpers\Json $response
     */
    public function actionUpdateMeterNumber()
    {
        if (!Yii::$app->request->isPost) {
            throw new MethodNotAllowedHttpException('Error! Method not allowed', 1);
        } else {
            $request = Yii::$app->request->getRawBody();
            if (is_null($request)) {
                return 'NULL';
            }
            $client_info = Json::decode($request);
            $device_id = $client_info['device_id'];
            $mobile_no = $client_info['mobile_no'];
            $meter_no = $client_info['meter_no'];

            $client = Client::find()->where(['=', 'mobile_no', $mobile_no])->one();

            $registered_device = ClientDevice::find()
                ->where(['=', 'client_id', $client->id])
                ->andWhere(['=', 'device_id', $device_id])
                ->one();

            if (!$registered_device) {
                return Json::encode(['fail' => 'success', 'data' => 'Client doesn\'t exist']);
            }
            $client->meter_no = $meter_no;

            if ($client->save(false)) {
                return Json::encode(['status' => 'success', 'data' => 'meter number has been updated']);
            }
            return Json::encode(['status' => 'fail', 'data' => 'failed to updated']);
        }
    }

    /**
     * Update Client language
     *
     * @return \yii\helpers\Json $response
     */
    public function actionUpdateLanguage()
    {
        if (!Yii::$app->request->isPost) {
            throw new MethodNotAllowedHttpException('Error! Method not allowed', 1);
        } else {
            $request = Yii::$app->request->getRawBody();
            if (is_null($request)) {
                return 'NULL';
            }
            $client_info = Json::decode($request);
            $device_id = $client_info['device_id'];
            $mobile_no = $client_info['mobile_no'];
            $lang_used = $client_info['lang_used'];

            $client = Client::find()->where(['=', 'mobile_no', $mobile_no])->one();

            $registered_device = ClientDevice::find()
                ->where(['=', 'client_id', $client->id])
                ->andWhere(['=', 'device_id', $device_id])
                ->one();

            if (!$registered_device) {
                return Json::encode(['fail' => 'success', 'data' => 'Client doesn\'t exist']);
            }

            $client->lang_used = $lang_used;

            if ($client->save(false)) {
                return Json::encode(['status' => 'success', 'data' => 'language preference has been updated']);
            }
            return Json::encode(['status' => 'fail', 'data' => 'failed to updated']);
        }
    }

    /**
     * Unregister client
     *
     * @return \yii\helpers\Json $response
     */
    public function actionUnregisterDevice()
    {
        if (!Yii::$app->request->isPost) {
            throw new MethodNotAllowedHttpException('Error! Method not allowed', 1);
        } else {
            $request = Yii::$app->request->getRawBody();
            if (is_null($request)) {
                return 'NULL';
            }
            $client_info = Json::decode($request);
            $device_id = $client_info['device_id'];
            $mobile_no = $client_info['mobile_no'];
            $conn_status = $client_info['conn_status'];

            $client = Client::find()->where(['=', 'mobile_no', $mobile_no])->one();

            $registered_device = ClientDevice::find()
                ->where(['=', 'client_id', $client->id])
                ->andWhere(['=', 'device_id', $device_id])
                ->one();

            if (!$registered_device) {
                return Json::encode(['fail' => 'success', 'data' => 'Client doesn\'t exist']);
            }

            $client->conn_status = $conn_status;

            if ($client->save(false)) {
                return Json::encode(['status' => 'success', 'data' => 'unregistered successfully']);
            }
            return Json::encode(['status' => 'fail', 'data' => 'failed to unregister']);
        }
    }

    /**
     * Update Client notification status
     *
     * @return \yii\helpers\Json $response
     */
    public function actionUpdateNotificationStatus()
    {
        if (!Yii::$app->request->isPost) {
            throw new MethodNotAllowedHttpException('Error! Method not allowed', 1);
        } else {
            $request = Yii::$app->request->getRawBody();
            if (is_null($request)) {
                return 'NULL';
            }
            $client_info = Json::decode($request);
            $device_id = $client_info['device_id'];
            $mobile_no = $client_info['mobile_no'];
            $is_notified = $client_info['is_notified'];

            $client = Client::find()->where(['=', 'mobile_no', $mobile_no])->one();

            $registered_device = ClientDevice::find()
                ->where(['=', 'client_id', $client->id])
                ->andWhere(['=', 'device_id', $device_id])
                ->one();

            if (!$registered_device) {
                return Json::encode(['fail' => 'success', 'data' => 'Client doesn\'t exist']);
            }

            $client->is_notified = $is_notified;

            if ($client->save(false)) {
                return Json::encode(['status' => 'success', 'data' => 'notification status has been updated']);
            }
            return Json::encode(['status' => 'fail', 'data' => 'notification status failed to updated']);
        }
    }

    /**
     * Update Client subscription status
     *
     * @return \yii\helpers\Json $response
     */
    public function actionUpdateSubscriptionStatus()
    {
        if (!Yii::$app->request->isPost) {
            throw new MethodNotAllowedHttpException('Error! Method not allowed', 1);
        } else {
            $request = Yii::$app->request->getRawBody();
            if (is_null($request)) {
                return 'NULL';
            }
            $client_info = Json::decode($request);
            $device_id = $client_info['device_id'];
            $mobile_no = $client_info['mobile_no'];
            $is_subscribed = $client_info['is_subscribed'];

            $client = Client::find()->where(['=', 'mobile_no', $mobile_no])->one();

            $registered_device = ClientDevice::find()
                ->where(['=', 'client_id', $client->id])
                ->andWhere(['=', 'device_id', $device_id])
                ->one();

            if (!$registered_device) {
                return Json::encode(['fail' => 'success', 'data' => 'Client doesn\'t exist']);
            }

            $client->is_subscribed = $is_subscribed;

            if ($client->save(false)) {
                return Json::encode(['status' => 'success', 'data' => 'subscription status has been updated']);
            }
            return Json::encode(['status' => 'fail', 'data' => 'subscription status failed to updated']);
        }
    }
}
