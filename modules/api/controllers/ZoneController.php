<?php

namespace app\modules\api\controllers;

use Yii;
use yii\rest\ActiveController;

/**
 * Default controller for the `api` module
 */
class ZoneController extends ActiveController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public $modelClass = "\app\models\Zone";

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator']['formats']['text/html'] = \yii\web\Response::FORMAT_JSON;

        return $behaviors;
    }

    public function actionDoUpdate()
    {
        $zones = Yii::$app->soapclient->getZones();

        foreach (Yii::$app->formatData->xml2array($zones)['Zone'] as $zone) {
            list($zcode, $name) = preg_split('/,/', $zone);
            // print_r($zone);

            $regionDB = $this->modelClass::find()->where(['AND', 'zone_code = :rc', 'name = :name'], [':rc' => $zcode, ':name' => $name])->one();
            if (!$regionDB) {
                $regionDB = new $this->modelClass();
                $regionDB->name = strtolower($name);
                $regionDB->zone_code = $zcode;
                $regionDB->created_by = 1;
                $regionDB->save(false);
            } else {
                echo('Duplicate Zone: (' . $name . ' => ' . $zcode . ")\n");
            }
        }
        return true;
    }
}
