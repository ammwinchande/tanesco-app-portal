<?php

namespace app\modules\api\controllers;

use Yii;
use yii\rest\ActiveController;

/**
 * Default controller for the `api` module
 */
class DistrictController extends ActiveController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public $modelClass = "\app\models\District";
    public $modelClassRegion = "\app\models\Region";

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator']['formats']['text/html'] = \yii\web\Response::FORMAT_JSON;

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionDoUpdate()
    {
        $regions = $this->modelClassRegion::find()
            ->where(['AND', ['!=', 'zone_id', 3], ['NOT LIKE', 'name', '%ZONE%', false]])
            ->all();
        foreach ($regions as $key => $region) {
            $districts = Yii::$app->soapclient->getDistricts(['Region' => $region->name]);
            foreach (Yii::$app->formatData->xml2array($districts)['District'] as $district) {
                list($dcode, $name) = preg_split('/,/', $district);
                $districtDB = $this->modelClass::find()
                    ->where(
                        ['AND', 'dis_code = :dc', 'name = :name', 'region_id = :rid', 'zone_id = :zid'],
                        [':dc' => $dcode, ':name' => $name, ':rid' => $region->id, ':zid' => $region->zone_id]
                    )
                    ->one();
                if (!$districtDB) {
                    $districtDB = new $this->modelClass();
                    $districtDB->name = strtolower($name);
                    $districtDB->zone_id = $region->zone_id;
                    $districtDB->region_id = $region->id;
                    $districtDB->dis_code = $dcode;
                    $districtDB->created_by = 1;
                    $districtDB->save(false);
                }
            }
            $myregion = $this->modelClassRegion::findOne(['id' => $region->id]);
            $myregion->has_district = 1;
            $myregion->save(false);
        }
        return true;
    }

    public function actionIndex($region_id = null)
    {
        $regions = $this->modelClassRegion::find()
            ->select(['id'])
            ->where(['AND', ['!=', 'zone_id', 3], ['NOT LIKE', 'name', '%ZONE%', false]]);
        $whereArgs = !$region_id ? ['IN', 'region_id', $regions] : ['=', 'region_id', $region_id];
        return $this->modelClass::find()
            ->select(['id', 'name', 'region_id'])
            ->where($whereArgs)
            ->orderBy('name ASC')
            ->all();
    }
}
