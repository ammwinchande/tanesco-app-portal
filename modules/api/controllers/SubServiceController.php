<?php

namespace app\modules\api\controllers;

use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Default controller for the `api` module
 */
class SubServiceController extends ActiveController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public $modelClass = "\app\models\SubService";
    public $modelClassService = "\app\models\Service";

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    /**
     * Return all Sub Service based on service id
     *
     * @return /yii/web/Response::json $response
     */
    public function actionIndex($service_id = null)
    {
        $services = $this->modelClassService::find()->select(['id']);
        $whereArgs = !$service_id ? ['IN', 'service_id', $services] : ['=', 'service_id', $service_id];
        return $this->modelClass::find()
            ->select(['id', 'name', 'name_sw', 'is_meter_required', 'service_id'])
            ->where($whereArgs)
            ->all();
    }
}
