<?php

namespace app\modules\api\controllers;

ini_set('max_execution_time', 0);
ini_set('memory_limit', -1);

use Yii;
use yii\web\Response;
use yii\rest\ActiveController;

/**
 * Default controller for the `api` module
 */
class AreaController extends ActiveController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public $modelClass = "\app\models\LocArea";
    public $modelClassStreet = "\app\models\LocStreet";
    public $modelClassRegion = "\app\models\Region";
    public $modelClassDistrict = "\app\models\District";

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionDoUpdate()
    {
        try {
            $count = 0;
            $areas = Yii::$app->soapclient->getAreasStreets();
            foreach (Yii::$app->formatData->xml2array($areas)['Areas'] as $key => $area) {
                $count++;
                $value = Yii::$app->formatData->xml2array($area);

                $area_name = $value['Area_Name'];
                $area_code = $value['Area_Code'];
                $regi_code = $value['Region_Code'];
                $dist_code = $value['District_Code'];
                $stre_name = $value['Street_Name'];
                $stre_code = $value['Street_Code'];

                $regi_id = $this->modelClassRegion::find()->select(['id'])->where(['=', 'reg_code', $regi_code])->scalar();
                $dist_id = $this->modelClassDistrict::find()->select(['id'])->where(
                    ['AND', 'dis_code = :dcode', 'region_id = :reg_id'],
                    [':dcode' => $dist_code, ':reg_id' => $regi_id]
                )->scalar();

                $areaDB = $this->modelClass::find()->where(
                    ['AND', 'name = :name', 'area_code = :acode', 'district_id = :did', 'region_id = :rid'],
                    [':name' => strtolower($area_name), ':acode' => $area_code, ':did' => $dist_id, ':rid' => $regi_id]
                )->one();
                if (!$areaDB) {
                    $areaDB = new $this->modelClass();
                    $areaDB->name = strtolower($area_name);
                    $areaDB->region_id = $regi_id;
                    $areaDB->district_id = $dist_id;
                    $areaDB->area_code = $area_code;
                    $areaDB->save(false);

                    $area_id = $areaDB->id;
                    $this->setStreets($count, strtolower($stre_name), $stre_code, $area_id, $dist_id, $regi_id);

                    $myregion = $this->modelClassDistrict::findOne(['id' => $dist_id]);
                    $myregion->has_area = 1;
                    $myregion->save(false);
                } else {
                    $area_id = $areaDB->id;
                    $this->setStreets($count, strtolower($stre_name), $stre_code, $area_id, $dist_id, $regi_id);
                    echo ("Duplicate Area: $count (" . strtoupper($stre_name) . ' => ' . $area_code . ':Dis[' . $dist_id . '] => Reg[' . $regi_id . "])\n");
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return true;
    }

    private function setStreets($count, $stre_name, $stre_code, $area_id, $dist_id, $regi_id)
    {
        echo ('Street: ' . $count . "\n");
        $streetDB = $this->modelClassStreet::find()->where(
            ['AND', 'name = :name', 'street_code = :scode', 'area_id = :aid', 'district_id = :did', 'region_id = :rid'],
            [':name' => $stre_name, ':scode' => $stre_code, ':aid' => $area_id, ':did' => $dist_id, ':rid' => $regi_id]
        )->one();
        if (!$streetDB) {
            $streetDB = new $this->modelClassStreet();
            $streetDB->name = $stre_name;
            $streetDB->street_code = $stre_code;
            $streetDB->region_id = $regi_id;
            $streetDB->district_id = $dist_id;
            $streetDB->area_id = $area_id;
            $streetDB->save(false);
        } else {
            echo ("Duplicate Street: $count (" . strtoupper($stre_name) . ' => ' . $stre_code .
                ' : Area[' . $area_id . '] => Dis[' . $dist_id . '] => Reg[' . $regi_id . "])\n");
        }
    }

    public function actionIndex($district_id = null)
    {
        $districts = $this->modelClassDistrict::find()->select(['id']);

        $whereArgs = !$district_id ? ['IN', 'district_id', $districts] : ['=', 'district_id', $district_id];
        return $this->modelClass::find()
            ->select(['id', 'name', 'region_id', 'district_id'])
            ->where($whereArgs)
            ->orderBy('name ASC')
            ->all();
    }

    public function actionTest()
    {
        $filters = [
            'FromDate' => '01-01-2020',
            'ToDate' => '31-01-2020',
            'Scope' => '',
            'Status' => '',
        ];
        $services = Yii::$app->soapclient->getProblemsFrequency($filters);
        // $filters = [
        //     'FromDate' => '01-01-2020',
        //     'ToDate' => '31-01-2020',
        // ];
        // $services = Yii::$app->soapclient->getServices($filters);
        // $filters = [
        //     'FromDate' => '01-01-2020',
        //     'ToDate' => '31-01-2020',
        // ];
        // $services = Yii::$app->soapclient->getServicesSummary($filters);
        // $filters = [
        //     'FromDate' => '01-02-2020',
        //     'ToDate' => '29-02-2020',
        //     'Scope' => '',
        // ];
        // $services = Yii::$app->soapclient->getServicesSummaryByLocation($filters);
        // $filters = [
        //     'From_Date' => date('1-m-Y'),
        //     'To_Date' => date('t-m-Y'),
        //     'Zone_Name' => '',
        //     'Region_Name' => '',
        //     'District_Name' => '',
        //     'Service_Name' => '',
        // ];
        // $services = Yii::$app->soapclient->getServicesByDuration($filters);
        // $services = Yii::$app->soapclient->getServiceNames();

        return $services;
    }
}
