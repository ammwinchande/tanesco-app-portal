<?php

namespace app\modules\api\controllers;

ini_set('max_execution_time', 0);
ini_set('memory_limit', -1);

use yii\rest\ActiveController;

/**
 * Default controller for the `api` module
 */
class StreetController extends ActiveController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public $modelClass = "\app\models\LocStreet";
    public $modelClassArea = "\app\models\LocArea";

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator']['formats']['text/html'] = \yii\web\Response::FORMAT_JSON;

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionIndex($area_id = null)
    {
        $areas = $this->modelClassArea::find()->select(['id']);

        $whereArgs = !$area_id ? ['IN', 'area_id', $areas] : ['=', 'area_id', $area_id];
        return $this->modelClass::find()
            ->select(['id', 'name', 'region_id', 'district_id', 'area_id'])
            ->where($whereArgs)
            ->orderBy('name ASC')
            ->all();
    }
}
