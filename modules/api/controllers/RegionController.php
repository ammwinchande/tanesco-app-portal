<?php

namespace app\modules\api\controllers;

use Yii;
use yii\rest\ActiveController;

/**
 * Default controller for the `api` module
 */
class RegionController extends ActiveController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public $modelClass = "\app\models\Region";
    public $modelClassZone = "\app\models\Zone";

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator']['formats']['text/html'] = \yii\web\Response::FORMAT_JSON;

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionDoUpdate()
    {
        $regions = Yii::$app->soapclient->getRegions();
        // die(print_r($regions));
        foreach (Yii::$app->formatData->xml2array($regions)['Region'] as $region) {
            list($zcode, $rcode, $name) = preg_split('/,/', $region);
            $zone_id = $this->modelClassZone::find()->select('id')->where(['zone_code' => $zcode]);
            $regionDB = $this->modelClass::find()->where(['AND', 'reg_code = :rc', 'name = :name'], [':rc' => $rcode, ':name' => $name])->one();
            if (!$regionDB) {
                $regionDB = new $this->modelClass();
                $regionDB->name = strtolower($name);
                $regionDB->reg_code = $rcode;
                $regionDB->zone_id = $zone_id;
                $regionDB->has_district = 1;
                $regionDB->created_by = 7;
                $regionDB->save(false);
            } else {
                echo('Duplicate Region: (' . $name . ' => ' . $rcode . ")\n");
            }
        }
        return true;
    }

    public function actionIndex()
    {
        return $this->modelClass::find()
            ->select(['id', 'name'])
            ->where(['AND', ['!=', 'zone_id', 3], ['NOT LIKE', 'name', '%ZONE%', false]])
            ->orderBy('name ASC')
            ->all();
    }
}
