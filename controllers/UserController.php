<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Region;
use app\models\District;
use app\models\UserSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'create', 'index', 'view'],
                'rules' => [
                    [
                        'actions' => [
                            'logout', 'create', 'index', 'view', 'profile'
                        ],
                        'allow' => true,
                        // 'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($insert)
    {
        if (Yii::$app->user->isGuest) {
            return false;
        } else {
            return true;
        }
    }

    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionProfile()
    {
        return $this->render(
            'profile',
            [
                'model' => $this->findModel(Yii::$app->user->id),
            ]
        );
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // $this->layout = "main-plain";
        $model = new User();
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            Yii::$app->session->setFlash(
                'success',
                'Success! New user (' . $model->username . ') successfully created!'
            );
            return $this->redirect(['index']);
        }
        return $this->render(
            'create',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            Yii::$app->session->setFlash(
                'success',
                'Success. Update action completed successfully.!'
            );
            return $this->redirect(['index']);
        }

        return $this->render(
            'update',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash(
            'success',
            'Success. Delete action completed successfully.!'
        );

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteAll()
    {
        $pk = Yii::$app->request->post('pk');
        $explode = explode(',', $pk);
        if ($explode) {
            foreach ($explode as $v) {
                if ($v) {
                    $this->findModel($v)->delete();
                }
            }
        }
        echo 1;
    }

    /**
     * Lists all Regions based on Filter Zones.
     * @return mixed
     */
    public function actionRegion()
    {
        Yii::$app->response->format = Yii\web\Response::FORMAT_JSON;
        if (isset($_POST['depdrop_parents'])) {
            $zones = $_POST['depdrop_parents'];
            if ($zones != null) {
                $zone = $zones[0];
                return self::getRegion($zone);
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    /**
     * Lists all Districts based on Filter Regions.
     * @return mixed
     */
    public function actionDistrict()
    {
        Yii::$app->response->format = Yii\web\Response::FORMAT_JSON;
        if (isset($_POST['depdrop_parents'])) {
            $regions = $_POST['depdrop_parents'];
            if ($regions != null) {
                $region = $regions[0];
                return self::getDistrict($region);
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    private function getRegion($zone)
    {
        $out = Region::find()
            ->select(['id', 'name'])
            ->where(['zone_id' => $zone])
            ->all();
        return ['output' => $out, 'selected' => ''];
    }
    private function getDistrict($region)
    {
        $out = District::find()
            ->select(['id', 'name'])
            ->where(['region_id' => $region])
            ->all();
        return ['output' => $out, 'selected' => ''];
    }

    /**
     * Fetch DropDown Data List for a child/DepDrop.
     *
     * @return mixed DepDrop Data List and Selected Value
     */
    public function actionDepdrop()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        $dataList = [];
        $selected = '';
        if (!empty($_POST['depdrop_params'])) {
            $selected = $_POST['depdrop_params'][0];
            // $selected = end($_POST['depdrop_params']);
        }
        if (isset($_POST['depdrop_parents'])) {
            $value = end($_POST['depdrop_parents']);
            $key = array_key_last($_POST['depdrop_parents']);
            // $out = $value;
            // $out = $key;
            // die(print_r($value));
            if ($key == 0) {
                $dataList = Region::find()
                    ->select(['id', 'name'])
                    ->where(['zone_id' => $value])
                    ->all();
            } elseif ($key == 1) {
                $dataList = District::find()
                    ->select(['id', 'name'])
                    ->where(['region_id' => $value])
                    ->all();
            }

            if ($value != null && count($dataList) > 0) {
                // return ['output' => $out, 'selected' => ''];
                return ['output' => $out, 'selected' => $selected];
            }
        }
        return ['output' => '', 'selected' => ''];
    }
}
