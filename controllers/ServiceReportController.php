<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\LocArea;
use app\models\District;
use app\models\LocStreet;
use app\models\ServiceReport;
use app\models\ServiceReportSearch;
use yii\web\NotFoundHttpException;
use yii\web\Controller;

/**
 * ServiceReportController implements the CRUD actions for ServiceReport model.
 */
class ServiceReportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [];
        $uid = Yii::$app->getUser()->id;
        $post = Yii::$app->request->post();
        $user = User::findOne($uid)['username'];
        $only = [
            'resend-to-sdm',
            'index',
            'view'
        ];
        $behaviors_verbs = [
            'delete' => ['post'],
            'delete-all' => ['post']
        ];
        $behaviors['verbs'] = Yii::$app->behavior->getList(
            $behaviors_verbs
        );
        $behaviors['access'] = Yii::$app->filter->getArrayAccess(
            $post,
            $only,
            $uid,
            $user
        );

        return $behaviors;
    }

    /**
     * Lists all ServiceReport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServiceReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single ServiceReport model.
     * @param  integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Creates a new ServiceReport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionResendToSdm($id)
    {
        $model = $this->findModel($id);

        if ($model->send_to_sdm == 1) {
            Yii::$app->session->setFlash(
                'warning',
                'Resend Failed. Report already Sent. You cannot Resend it again.!'
            );
            return $this->redirect(['index']);
        }
        $client = ServiceReport::getClient($model->customer_id);
        $service_report_data = [
            'Region' => $model->region,
            'District' => $model->district,
            'Area' => $model->area,
            'Street' => $model->street,
            'Special_Mark' => $model->special_mark,
            'Problem' => (ServiceReport::getClient($model->sub_service_id))->name,
            'Problem_Details' => $model->details,
            'Mobile_Number' => $client['mobile_no'],
            'CustName' => $client['name'],
            'Is_Mobile' => 'TRUE'
        ];
        $sent = Yii::$app->toSdm->send($service_report_data);
        $model->send_to_sdm = $sent ? 1 : 0;

        if ($sent && $model->save()) {
            Yii::$app->session->setFlash(
                'success',
                'Resend succeeded. Reported by: ' . $model->client->name
            );
            return $this->redirect(['index']);
        }
    }

    /**
     * Lists all Districts based on Filter Regions.
     * @return mixed
     */
    public function actionDistrict()
    {
        Yii::$app->response->format = Yii\web\Response::FORMAT_JSON;
        if (isset($_POST['depdrop_parents'])) {
            $regions = $_POST['depdrop_parents'];
            if ($regions != null) {
                $region = $regions[0];
                $out = District::find()
                    ->select(['id', 'name'])
                    ->where(['region_id' => $region])
                    ->all();
                return ['output' => $out, 'selected' => ''];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    /**
     * Lists all Areas based on Filter District.
     * @return mixed
     */
    public function actionArea()
    {
        Yii::$app->response->format = Yii\web\Response::FORMAT_JSON;
        if (isset($_POST['depdrop_parents'])) {
            $districts = $_POST['depdrop_parents'];
            if ($districts != null) {
                $district = $districts[0];
                $out = LocArea::find()
                    ->select(['id', 'name'])
                    ->where(['district_id' => $district])
                    ->all();
                return ['output' => $out, 'selected' => ''];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    /**
     * Lists all Streets based on Filter Area.
     * @return mixed
     */
    public function actionStreet()
    {
        Yii::$app->response->format = Yii\web\Response::FORMAT_JSON;
        if (isset($_POST['depdrop_parents'])) {
            $areas = $_POST['depdrop_parents'];
            if ($areas != null) {
                $area = $areas[0];
                $out = LocStreet::find()
                    ->select(['id', 'name'])
                    ->where(['area_id' => $area])
                    ->all();
                return ['output' => $out, 'selected' => ''];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    /**
     * Fetch DropDown Data List for a child/DepDrop.
     *
     * @return mixed DepDrop Data List and Selected Value
     */
    public function actionDepDrop()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        $dataList = [];
        $selected = '';
        if (!empty($_POST['depdrop_params'])) {
            $selected = end($_POST['depdrop_params']);
        }
        if (isset($_POST['depdrop_parents'])) {
            $value = end($_POST['depdrop_parents']);
            $key = array_key_last($_POST['depdrop_parents']);
            if ($key == 0) {
                $dataList = District::find()
                    ->select(['id', 'name'])
                    ->where(['region_id' => $value])
                    ->all();
            } elseif ($key == 1) {
                $dataList = LocArea::find()
                    ->select(['id', 'name'])
                    ->where(['district_id' => $value])
                    ->all();
            } elseif ($key == 2) {
                $dataList = LocStreet::find()
                    ->select(['id', 'name'])
                    ->where(['area_id' => $value])
                    ->all();
            }

            if ($value != null && count($dataList) > 0) {
                return ['output' => $out, 'selected' => $selected];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    /**
     * Finds the ServiceReport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  integer               $id
     * @return ServiceReport         the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ServiceReport::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
