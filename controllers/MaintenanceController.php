<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
/**
* DistrictController implements the CRUD actions for District model.
*/
class MaintenanceController extends Controller
{
    /**
     * @var StateInterface
     */
    protected $state;

    public function __construct(string $id, Module $module, StateInterface $state, array $config = [])
    {
        $this->state = $state;
        parent::__construct($id, $module, $config);
    }

    public function actionEnable()
    {
        $this->state->enable();
    }
    public function actionDisable()
    {
        $this->state->disable();
    }
}
