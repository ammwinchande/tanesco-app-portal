<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Response;
use app\models\Reports;
use yii\web\Controller;
use app\models\LoginForm;
use app\models\SignupForm;
use yii\filters\VerbFilter;
use app\models\Maintenance;
use app\models\LoginHistory;
use yii\base\ErrorException;
use app\models\ReportsSearch;
use yii\filters\AccessControl;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     *
     * @return array of behaviours
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array of actions
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['site/login']));
        }

        return $this->render('index');
    }

    /**
     * Displays siginUp Page.
     *
     * @return string
     */
    public function actionSignup()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render(
            'signup',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'main-plain';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        try {
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $session = new LoginHistory();
                $session->status = 1;
                $session->user_id = Yii::$app->user->id;
                $session->session_id = Yii::$app->session->getId();
                $session->save();

                return $this->goBack();
            }
            $model->password = '';

            return $this->render(
                'login',
                [
                    'model' => $model,
                ]
            );
        } catch (ErrorException $e) {
            Yii::$app->session->setFlash(
                'error',
                Yii::t('app', 'Session Not Opened.')
            );
            Yii::warning(Yii::t('app', 'Session Not Opened.'));
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        try {
            $session = LoginHistory::find()
                ->where(
                    [
                        'AND',
                        ['=', 'session_id', Yii::$app->session->getId()],
                        ['=', 'user_id', Yii::$app->user->id]
                    ]
                )
                ->distinct()
                ->one();
            $session->status = 0;
            $session->signed_off = date('Y-m-d H:i:s');
            $session->save();
            Yii::$app->user->logout();

            return $this->goBack();
        } catch (ErrorException $e) {
            Yii::$app->session->setFlash(
                'error',
                Yii::t('app', 'Session Not Opened.')
            );
            Yii::warning(Yii::t('app', 'Session Not Opened.'));
        }
    }

    /**
     * Displays Lock Screen.
     *
     * @return string
     */
    public function actionLockScreen()
    {
        $this->layout = 'main-plain';
        $model = new LoginForm();
        if (isset(Yii::$app->user->identity->username)) {
            // save current username
            $username = Yii::$app->user->identity->username;
            // force logout
            Yii::$app->user->logout();
            // render form lockscreen
            $model->username = $username;    //set default value

            return $this->render(
                'lock-screen',
                [
                    'model' => $model,
                ]
            );
        } else {
            return $this->redirect(['login']);
        }
    }

    /**
     * Enable/Disable Maintenance Mode.
     *
     * @return string
     */
    public function actionMaintenance()
    {
        $model = new Maintenance();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->status) {
                Yii::$app->maintenance->enable();

                return $this->redirect('maintenance/index');
            } else {
                Yii::$app->maintenance->disable();

                return $this->redirect('maintenance');
            }
        }

        return $this->render('maintenance');
    }

    /**
     * List results Reported Services/Emergencies/Requests etc.
     *
     * @return Yii\helpers\Json $response
     */
    public function actionServicesSummary()
    {
        $finalResults = [];
        $yesterday = [];
        $results = [];
        $scope = [];
        $today = [];
        $month = [];
        $array = [];
        $week = [];

        $post = Yii::$app->request->post();
        if (isset($post)) {
            if ($post['district']) {
                $scope = ['', 'z', 'r', 'd'];
            } elseif ($post['region']) {
                $scope = ['', 'z', 'r'];
            } elseif ($post['zone']) {
                $scope = ['', 'z'];
            }
            for ($i = 0; $i < count($scope); ++$i) {
                $month = [
                    'FromDate' => $post['startOfMonth'],
                    'ToDate' => $post['endOfMonth'],
                    'Scope' => $scope[$i]
                ];
                $week = [
                    'FromDate' => $post['startOfWeek'],
                    'ToDate' => $post['endOfWeek'],
                    'Scope' => $scope[$i]
                ];
                $yesterday = [
                    'FromDate' => $post['yesterday'],
                    'ToDate' => $post['yesterday'],
                    'Scope' => $scope[$i]
                ];
                $today = [
                    'FromDate' => $post['today'],
                    'ToDate' => $post['today'],
                    'Scope' => $scope[$i]
                ];
                $array = [$month, $week, $yesterday, $today];
                $j = 0;

                while ($j < count($array)) {
                    $data = Yii::$app->soapclient->getServicesSummaryByLocation(
                        $array[$j]
                    );
                    if (count($data) > 0) {
                        if ($j == 0) {
                            $results['month'] = $data;
                        } elseif ($j == 1) {
                            $results['week'] = $data;
                        } elseif ($j == 2) {
                            $results['yesterday'] = $data;
                        } elseif ($j == 3) {
                            $results['today'] = $data;
                        }
                    }
                    ++$j;
                }
                ($scope[$i] == '') ? $finalResults['hq'] = $results :
                    $finalResults[$scope[$i]] = $results;
            }
        } else {
            $finalResults = 'Something went wrong!';
        }

        return Json::encode($finalResults);
    }

    /**
     * List results Reported Services/Emergencies/Requests etc.
     *
     * @return Yii\helpers\Json $response
     */
    public function actionProblemFrequency()
    {
        $post = Yii::$app->request->post();
        if (isset($post)) {
            $filters = [
                'FromDate' => $post['from'],
                'ToDate' => $post['to'],
                'Scope' => $post['scope'],
                'Status' => $post['status'],
            ];
            $jsonDada = Yii::$app->soapclient->getProblemsFrequency($filters);
        } else {
            $jsonDada = 'Something went wrong!';
        }

        return Json::encode($jsonDada);
    }

    /**
     * List Reported Services by Duration.
     *
     * @return Yii\helpers\Json $response
     */
    public function actionServicesByDuration()
    {
        $post = Yii::$app->request->post();
        if (isset($post)) {
            $filters = [
                'From_Date' => $post['fromDate'],
                'To_Date' => $post['toDate'],
                'Zone_Name' => '',
                'Region_Name' => '',
                'District_Name' => '',
                'Service_Name' => '',
            ];
            $jsonDada = Yii::$app->soapclient->getServicesByDuration($filters);
        } else {
            $jsonDada = 'Something went wrong!';
        }

        return Json::encode($jsonDada);
    }

    /**
     * List Reported Services by Duration.
     *
     * @return Yii\helpers\Json $response
     */
    public function actionReports()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['site/login']));
        }

        $usesrId = Yii::$app->user->getId();
        if (Yii::$app->request->post() || Yii::$app->request->get()) {
            $post = Yii::$app->request->post() ?: null;
            $get = Yii::$app->request->get() ?: null;
            $data = $get ?? $post;
            $fromDate = $data['From_Date'];
            $toDate = $data['To_Date'];
            $data = $get ?? $data['Reports'];
            $zone = $data['Zone_Name'];
            $region = empty($zone) ? '' : $data['Region_Name'];
            $district = empty($region) ? '' : $data['District_Name'];
            $service = $data['Service_Name'];
            $status = $data['Service_Status'];
            $duration = $data['Service_Duration'];
        } else {
            $user = User::find()
                ->select(['zone_id', 'region_id', 'district_id'])
                ->where(['=', 'id', $usesrId])
                ->one();
            $fromDate = date('01-m-Y');
            $toDate = date('t-m-Y');
            $zone = $user->zone_id;
            $region = $user->region_id;
            $district = $user->district_id;
            $service = '';
            $status = '';
            $duration = '';
        }

        $model = new Reports();
        $model->From_Date = $fromDate;
        $model->To_Date = $toDate;
        $model->Zone_Name = $zone;
        $model->Region_Name = $region;
        $model->District_Name = $district;
        $model->Service_Name = $service;
        $model->Service_Status = $status;
        $model->Service_Duration = $duration;
        $searchModel = new ReportsSearch();
        $dataProvider = $searchModel->search(
            $fromDate,
            $toDate,
            $zone,
            $region,
            $district,
            $service,
            $status,
            $duration
        );

        return $this->render(
            'reports',
            [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Lists all Districts based on Filter Regions.
     * @return mixed
     */
    public function actionDropdown()
    {
        Yii::$app->response->format = Yii\web\Response::FORMAT_JSON;
        if (isset($_POST['depdrop_parents'])) {
            $regions = $_POST['depdrop_parents'];
            if ($regions != null) {
                $region = $regions[0];
                return self::getDistrict($region);
            }
        }
        return ['output' => '', 'selected' => ''];
    }
}
