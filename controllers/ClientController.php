<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Client;
use app\models\ClientSearch;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\Controller;
use sintret\gii\models\LogUpload;
use sintret\gii\components\Util;

/**
* ClientController implements the CRUD actions for Client model.
*/
class ClientController extends Controller
{
    /**
       * {@inheritdoc}
       */
    public function behaviors()
    {
        $behaviors = [];
        $uid = Yii::$app->getUser()->id;
        $post = Yii::$app->request->post();
        $user = User::findOne($uid)['username'];
        $only = ['index', 'view'];
        $behaviors_verbs = ['delete' => ['post'], 'delete-all' => ['post']];
        $behaviors['verbs'] = Yii::$app->behavior->getList($behaviors_verbs);
        $behaviors['access'] = Yii::$app->filter->getArrayAccess($post, $only, $uid, $user);

        return $behaviors;
    }

    /**
    * Lists all Client models.
    * @return mixed
    */
    public function actionIndex()
    {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    * Displays a single Client model.
    * @param integer $id
    * @return mixed
    */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
    * Finds the Client model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $id
    * @return Client the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id)
    {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
