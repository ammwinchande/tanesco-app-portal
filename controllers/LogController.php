<?php

namespace app\controllers;

use Yii;
use app\models\Log;
use app\models\User;
use app\models\LogSearch;
use app\components\ActionBehaviors;
use app\components\ArrayAccess;

use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use sintret\gii\models\LogUpload;
use sintret\gii\components\Util;
use yii\web\Controller;

/**
* LogController implements the CRUD actions for Log model.
*/
class LogController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $behaviors = [];
        $uid = Yii::$app->getUser()->id;
        $post = Yii::$app->request->post();
        $user = User::findOne($uid)['username'];
        $only = ['index', 'view', 'delete', 'delete-all'];
        $behaviors_verbs = ['delete' => ['post'], 'delete-all' => ['post']];
        $behaviors['verbs'] = Yii::$app->behavior->getList($behaviors_verbs);
        $behaviors['access'] = Yii::$app->filter->getArrayAccess($post, $only, $uid, $user);

        return $behaviors;
    }

    /**
    * Lists all Log models.
    * @return mixed
    */
    public function actionIndex()
    {
        $searchModel = new LogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    * Displays a single Log model.
    * @param integer $id
    * @return mixed
    */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
    * Deletes an existing Log model.
    * If deletion is successful, the browser will be redirected to the 'index' page.
    * @param integer $id
    * @return mixed
    */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Success. Delete action completed successfully.!');

        return $this->redirect(['index']);
    }

    /**
     * Deletes all selected existing data in Model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param array of $ids
     * @return mixed
     */
    public function actionDeleteAll()
    {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        $explode = explode(',', $pk);
        if ($explode) {
            foreach ($explode as $v) {
                if ($v) {
                    $this->findModel($v)->delete();
                }
            }
        }
        echo 1;
    }

    /**
    * Finds the Log model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $id
    * @return Log the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id)
    {
        if (($model = Log::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
