<?php

namespace app\controllers;

use app\models\Service;
use Yii;
use app\models\User;
use app\models\SubService;
use app\models\SubServiceSearch;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * SubServiceController implements the CRUD actions for SubService model.
 */
class SubServiceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [];
        $uid = Yii::$app->getUser()->id;
        $post = Yii::$app->request->post();
        $user = User::findOne($uid)['username'];
        $only = ['create', 'index', 'view', 'update', 'delete', 'delete-all'];
        $behaviors_verbs = ['delete' => ['post'], 'delete-all' => ['post']];
        $behaviors['verbs'] = Yii::$app->behavior->getList($behaviors_verbs);
        $behaviors['access'] = Yii::$app->filter->getArrayAccess($post, $only, $uid, $user);

        return $behaviors;
    }

    public function actionSubservice()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $out = Service::find()->select(['id', 'name', 'name_sw'])->where(['=', 'id', $cat_id])->all();
                return ['output' => $out, 'selected' => ''];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    /**
     * Lists all SubService models.
     *
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SubService model.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SubService model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SubService();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Success. Create action completed successfully.!');
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SubService model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Success. Update action completed successfully.!');
            return $this->redirect(['index']);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SubService model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Success. Delete action completed successfully.!');

        return $this->redirect(['index']);
    }

    /**
     * Deletes all selected existing data in Model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param array of $ids
     * @return mixed
     */
    public function actionDeleteAll()
    {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        $explode = explode(',', $pk);
        if ($explode) {
            foreach ($explode as $v) {
                if ($v) {
                    $this->findModel($v)->delete();
                }
            }
        }
        echo 1;
    }

    /**
     * Finds the SubService model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  integer               $id
     * @return SubService            the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SubService::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
