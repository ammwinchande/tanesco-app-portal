<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
		'css/styles.css',
    ];
    public $js = [
	//'js/canvasjs.min.js',
    //'js/horizontal_chart.js',
    // 'js/apexcharts.js',
    'js/apexcharts.min.js',
    'js/react.production.min.js',
    'js/react-dom.production.min.js',
    'js/prop-types.min.js',
    'js/browser.min.js',
    'js/react-apexcharts.iife.min.js',
    'https://cdn.jsdelivr.net/npm/apexcharts',
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
