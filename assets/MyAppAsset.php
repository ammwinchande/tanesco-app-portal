<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MyAppAsset extends AssetBundle
{
    public $aid=null;
    public $cid=null;
    //public $basePath = '@webroot';
    public $sourcePath = '@app/themes/limitless/assets';
    // public $baseUrl = '@web';
    public $js = [
        // <!-- Core JS files -->
        //"js/main/jquery.min.js",
        "js/main/bootstrap.bundle.min.js",
        "js/plugins/loaders/blockui.min.js",
        // <!-- /core JS files -->

        // <!-- Theme JS files -->
        "js/plugins/visualization/d3/d3.min.js",
        "js/plugins/visualization/d3/d3_tooltip.js",
        "js/plugins/forms/styling/switchery.min.js",
        "js/plugins/forms/selects/bootstrap_multiselect.js",
        "js/plugins/ui/moment/moment.min.js",
        "js/plugins/pickers/daterangepicker.js",

        "js/app.js",
        "js/demo_pages/dashboard.js",
        "https://cdn.jsdelivr.net/npm/apexcharts",
        // <!-- /theme JS files -->
    ];
    public $css = [
        //<!-- Global stylesheets -->
        //"https://fonts.googleapis.com/css?family=Roboto:400300100500700900",
        "css/icons/icomoon/styles.min.css",
        "css/bootstrap.min.css",
        "css/bootstrap_limitless.min.css",
        "css/layout.min.css",
        "css/components.min.css",
        "css/colors.min.css"
        //<!-- /global stylesheets -->
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\jui\CoreAsset',
        'yii\jui\JuiAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
        'yii\bootstrap4\BootstrapAsset',
    ];
    public function init()
    {
        parent::init();
        $this->aid=\Yii::$app->controller->action->id;
        $this->cid=\Yii::$app->controller->id;
        $pages = ["view","post","validate","files",'view-file','upload-luku-sales','report','report-mcc'];
        if (in_array($this->aid, $pages)) {
            // array_push($this->css"datatables.net-bs/css/dataTables.bootstrap.min.css");
            // array_push($this->js"datatables.net/js/jquery.dataTables.min.js");
        }
    }
}
