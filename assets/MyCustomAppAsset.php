<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MyCustomAppAsset extends AssetBundle
{
    public $aid=null;
    public $cid=null;
    //public $basePath = '@webroot';
    public $sourcePath = '@app/web/custom-asset/';
    // public $baseUrl = '@web';
    public $js = [
        // <!-- Core JS files -->
        //"js/main/jquery.min.js",
        "bootstrap-session-timeout.min.js",
    ];
    public $css = [

        "glyphicons/glyphicons.min.css",
    ];
    public $depends = [
        //'yii\web\YiiAsset',
    ];
    public function init()
    {
    }
}
