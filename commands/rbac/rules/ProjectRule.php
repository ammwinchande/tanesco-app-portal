<?php
/**
 * Created by PhpStorm.
 * User: Edson
 * Date: 08/01/2016
 * Time: 20:00
 */

namespace app\commands\rbac\rules;
use yii\rbac\Rule;
use \app\models\Project;

/**
 * Checks if authorID matches user passed via params
 */
class ProjectRule extends Rule
{
    public $name = 'isOwnerOfThisProject';
    /**
     * @param  string|integer $user   The user ID.
     * @param  Item           $item   The role or permission that this rule is associated with
     * @param  array          $params Parameters passed to ManagerInterface::checkAccess().
     * @return boolean                A value indicating whether the rule permits the role or
     *                                permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        $user=\app\models\User::findOne($user);
        $did=$user->did;
        // $model = \Yii::$app->controller->findBillsByDistrictId($did);
        
        //$bill_id=$params['model']->bill_id;
        //$did=intval(substr($bill_id,4,3));
        return isset($params['post']) ? $params['post']->district_id == $did : false;

    }

}