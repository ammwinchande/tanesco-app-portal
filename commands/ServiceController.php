<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\ServiceReport;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Expression;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ServiceController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    /**
     * This command closes all services with status of resolved in every 24hrs
     *
     * @return int Exit code
     */
    public function actionCloseResolvedServices()
    {
        echo "\n Start closing of reported services with status of resolved \n";
        $service_reports = ServiceReport::find()
            ->where(['=', 'status', 'resolved'])
            ->andWhere(['<=', 'updated_at', new Expression('DATE_SUB(NOW(), INTERVAL 24 HOUR)')])
            ->all();
        if ($service_reports != null) {
            foreach ($service_reports as $service_report) {
                $service_report->status = 'closed';
                $service_report->updated_at = date('Y-m-d H:i:s');
                $service_report->save();
            }
        }
        echo "\n .................Completed.............................\n";
    }
}
