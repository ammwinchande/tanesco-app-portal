<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class.
 *
 * @property integer $status
 */
class Maintenance extends Model
{
    private $status;

        /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
