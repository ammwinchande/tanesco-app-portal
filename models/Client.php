<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use \podtserkovsky\userdblog\behaviors\UserDbLogBehavior;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $meter_no Client Meter Number
 * @property string $mobile_no Client Mobile Number
 * @property string $name Client Name
 * @property string $device_id Client Device ID
 * @property int $conn_status Connection Status
 * @property int $region_id Region
 * @property int $district_id District
 * @property int $area_id Area
 * @property int $street_id Street
 * @property string $special_mark Special Mark
 * @property int $is_blocked True if Client is blocked
 * @property string $lang_used Laguage used (default English)
 * @property string $created_at Created Time
 * @property string $updated_at Updated Time
 * @property string $deleted_at Deleted Time
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'mobile_no', 'device_id', 'conn_status', 'region_id',
                    'district_id', 'area_id', 'street_id', 'is_blocked',
                    'lang_used'
                ],
                'required'
            ],
            [
                [
                    'conn_status', 'is_notified', 'is_subscribed', 'region_id',
                    'district_id', 'area_id', 'street_id', 'is_blocked'
                ],
                'integer'
            ],
            [['lang_used'], 'string'],
            [
                [
                    'special_mark', 'created_at', 'updated_at', 'deleted_at',
                    'session_start_date', 'session_end_date'
                ],
                'safe'
            ],
            [['meter_no'], 'string', 'max' => 11],
            [['mobile_no'], 'string', 'max' => 13],
            [['name', 'special_mark'], 'string', 'max' => 256],
            [['device_id'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'meter_no' => Yii::t('app', 'Meter Number'),
            'mobile_no' => Yii::t('app', 'Mobile Number'),
            'name' => Yii::t('app', 'Name'),
            'device_id' => Yii::t('app', 'Device ID'),
            'conn_status' => Yii::t('app', 'Connection Status'),
            'region_id' => Yii::t('app', 'Region'),
            'district_id' => Yii::t('app', 'District'),
            'area_id' => Yii::t('app', 'Area'),
            'street_id' => Yii::t('app', 'Street'),
            'special_mark' => Yii::t('app', 'Special Mark'),
            'is_notified' => Yii::t('app', 'Notification'),
            'is_subscribed' => Yii::t('app', 'Subscription'),
            'is_blocked' => Yii::t('app', 'Block'),
            'lang_used' => Yii::t('app', 'Language Used'),
            'created_at' => Yii::t('app', 'Created Time'),
            'session_start_date' => Yii::t('app', 'Session Start Date'),
            'session_end_date' => Yii::t('app', 'Session End Date'),
            'updated_at' => Yii::t('app', 'Updated Time'),
            'deleted_at' => Yii::t('app', 'Deleted Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Region Names
     */
    public function getRegions()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed District Names
     */
    public function getDistricts()
    {
        return $this->hasOne(District::className(), ['id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Area Names
     */
    public function getAreas()
    {
        return $this->hasOne(LocArea::className(), ['id' => 'area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Streets Names
     */
    public function getStreets()
    {
        return $this->hasOne(LocStreet::className(), ['id' => 'street_id']);
    }

    /**
     * Relationship between clientDevice and Client
     *
     *
     * @return mixed ClientDevice devices
     */
    public function getClientDevices()
    {
        return $this->hasMany(
            ClientDevice::className(),
            ['client_id' => 'id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Reports List
     */
    public function getReports()
    {
        return $this->hasMany(
            ServiceReport::className(),
            ['customer_id' => 'id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed ServiceReports Object
     */
    public static function getReport($id)
    {
        return ServiceReport::find()
            ->where(['=', 'customer_id', $id])
            ->orderBy('created_at')
            ->all();
    }

    /**
     * {@inheritdoc}
     * @return mixed Client Names
     */
    public static function getClients()
    {
        return ArrayHelper::map(
            self::find()
                ->select(['id', 'name'])
                ->orderBy('name')
                ->asArray()
                ->all(),
            'id',
            'name'
        );
    }

    /**
     * {@inheritdoc}
     * @return ClientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClientQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserDbLogBehavior::className()
            ],
        ];
    }
}
