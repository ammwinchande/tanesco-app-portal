<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ClientDevice]].
 *
 * @see ClientDevice
 */
class ClientDeviceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ClientDevice[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ClientDevice|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
