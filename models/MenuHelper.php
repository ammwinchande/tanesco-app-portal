<?php
/**
 * Created by PhpStorm.
 * User: Engr. Kobello
 * Date: 09/09/2019
 * Time: 11:27
 */

namespace app\models;

use Yii;
use app\models\MenuPanel;

class MenuHelper
{
    public static function getMenu()
    {
        $active_page = Yii::$app->controller->action->id;

        if($active_page == 'create' || $active_page == 'view' || $active_page == 'update'){
            $active_page = 'index';
        }

        $menu = [];
        $basicUrl =  Yii::$app->controller->id."/".$active_page;
        $moduleUrl =  Yii::$app->controller->module->id."/".$basicUrl;
        $activeUrl = Yii::$app->controller->module->id == "basic" ? $basicUrl : $moduleUrl;
        $class = "nav-item nav-item-submenu";
        $activeClass = $class." nav-item-expanded nav-item-open";

        $items = MenuPanel::find()
            ->where('parent_id=:pid', [':pid' => 0])
            ->andWhere('status=:sts', [':sts' => 1])
            ->orderBy('id')
            ->asArray()
            ->all();
        $i = 1;
        foreach ($items as $item) {
            if ($item['redirect'] == "#") {
                $params = $item['params'] ? "?".$item['params'] : "";
                $url = "#".$params;
                $subMenu = self::getSubMenu ($item['id'], $activeUrl);
                $active = $activeUrl == $item['redirect'] ? " active nav-item-open" : "";
                $isActiveClass  = self::isActiveSubMenu ($item['id'], $activeUrl) ? ['class' => $activeClass] : ['class' => $class];
            } else {
                $subMenu = [];
                $params = $item['params'] ? "?".$item['params'] : "";
                $url = [$item['redirect'].$params];
                $isActiveClass = ['class' => 'nav-item'];
                $active = $activeUrl == $item['redirect'] ? " active" : "";
            }

            $menu[] = [
                'template' => "<a href='{url}' class='nav-link$active'>\n{icon}\n{label}\n{badge}</a>",
                'label' => "<span>" . $item['name'] . "</span>",
                'url' => $url,
                'icon' => "<i class='fa " . $item['icon'] . "'></i>",
                'options' => $isActiveClass,
                'items' => $subMenu,
                '<li class="divider"></li>',
            ];
            $i++;
        }

        return $menu;
    }

    public static function getSubMenu ($id, $activeUrl)
    {
        $sub_menu = [];
        $items = MenuPanel::find()
            ->where('parent_id=:pid', [':pid' => $id])
            ->andWhere('status=:sts', [':sts' => 1])
            ->orderBy('id')
            ->asArray()
            ->all();

        foreach ($items as $item) {
            $active = $activeUrl == $item['redirect'] ? " active" : "";

            $sub_menu[] = [
                'template' => "<a href='{url}' class='nav-link$active'>\n{icon}\n{label}\n{badge}</a>",
                'label' => "<span>" . $item['name'] . "</span>",
                'url' => [$item['redirect']],
                'icon' => "<i class='fa " . $item['icon'] . "'></i>",
                'options' => ['class' => 'nav-item'],
                '<li class="divider"></li>',
            ];
        }

        return $sub_menu;
    }

    public static function isActiveSubMenu ($id, $activeUrl)
    {
        $items = MenuPanel::find()
            ->where('parent_id=:pid', [':pid' => $id])
            ->andWhere('status=:sts', [':sts' => 1])
            ->orderBy('id')
            ->asArray()
            ->all();

        foreach ($items as $item) {
            if ($activeUrl == $item['redirect']) return true;
        }

        return false;
    }
}