<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_device".
 *
 * @property int $id ID
 * @property int $client_id Client ID
 * @property string $device_id Device ID
 * @property int $status Status
 * @property int $is_primary
 * @property string $created_at Created At
 * @property string|null $updated_at Updated At
 * @property string|null $deleted_at Deleted At
 *
 * @property Client $client
 */
class ClientDevice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_device';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'device_id'], 'required'],
            [['client_id', 'status', 'is_primary'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['device_id'], 'string', 'max' => 100],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'client_id' => Yii::t('app', 'Client ID'),
            'device_id' => Yii::t('app', 'Device ID'),
            'status' => Yii::t('app', 'Status'),
            'is_primary' => Yii::t('app', 'Is Primary'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[Client]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }
}
