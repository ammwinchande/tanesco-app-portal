<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use \podtserkovsky\userdblog\behaviors\UserDbLogBehavior;

class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_IN_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const DEFAULT_PASSWORD = 'Tanesco123';
    public $first_name;
    public $last_name;
    public $role;

    public static function tableName()
    {
        return 'user';
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'email' => Yii::t('app', 'Email'),
            'auth_key' => Yii::t('app', 'AuthKey'),
            'password_reset_token' => Yii::t('app', 'PasswordResetToken'),
            'access_token' => Yii::t('app', 'AccessToken'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'CreatedAt'),
            'updated_at' => Yii::t('app', 'UpdatedAt'),
            'designation' => Yii::t('app', 'Designation'),
            'coyno' => Yii::t('app', 'Coy No'),
            'created_by' => Yii::t('app', 'Created By'),
            'zone_id' => Yii::t('app', 'Zone'),
            'region_id' => Yii::t('app', 'Region'),
            'district_id' => Yii::t('app', 'District'),
            'role' => Yii::t('app', 'User Role'),
        ];
    }

    public function rules()
    {
        return [
            [['first_name', 'last_name', 'username', 'status', 'zone_id', 'region_id', 'email', 'coyno', 'designation', 'role'], 'required'],
            [['status', 'region_id', 'district_id', 'created_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'email', 'designation'], 'string', 'max' => 64],
            [['auth_key'], 'string', 'max' => 32],
            [['coyno'], 'string', 'max' => 8],
            [['first_name'], 'string', 'max' => 64],
            [['last_name'], 'string', 'max' => 64],
            [['password_reset_token', 'access_token', 'password'], 'string', 'max' => 255],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_IN_ACTIVE]],
            [
                ['id'], 'exist', 'skipOnError' => true,
                'targetClass' => AuthAssignment::className(),
                'targetAttribute' => ['id' => 'user_id']
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_at = strtotime('now');
                $this->updated_at = strtotime('now');
                $this->auth_key = Yii::$app->security->generateRandomString();
                $this->access_token = Yii::$app->security->generateRandomString() . ':' . time();
                $this->password = $this->setPassword(self::DEFAULT_PASSWORD);
                $this->created_by = Yii::$app->user->id;
                $this->email = $this->username . '@tanesco.co.tz';
                $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $exists = AuthAssignment::find()->where(['=', 'user_id', $this->id])->one();
        $uRole = ($exists) ?: new AuthAssignment();
        $uRole->user_id = $this->id;
        $uRole->item_name = $this->role;
        $uRole->save(false);
        return true;
    }

    /**
     * {@inheritdoc}
     * @return userRole from \app\models\AuthAssignment()
     */
    public static function getRole($role = null)
    {
        $role = ($role != null) ? $role : Yii::$app->user->getId();
        $user_role = AuthAssignment::find()->orderBy('item_name')->where(
            ['=', 'user_id', $role]
        )->one();
        return $user_role['item_name'];
    }

    /**
     * {@inheritdoc}
     * @return userRoles from \app\models\AuthAssignment()
     */
    public function getRoles()
    {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return userRoles from \app\models\AuthAssignment()
     */
    public static function getRoleItems()
    {
        return ArrayHelper::map(
            AuthItem::find()->select(['name', 'name'])
                ->where(['=', 'type', 1])->orderBy('name')
                ->asArray()->all(),
            'name',
            'name'
        );
    }

    /**
     * {@inheritdoc}
     * @return user_name
     */
    public static function getName($id)
    {
        $username = self::find()->select(['username'])
            ->where(['=', 'id', $id])->one();
        return $username->username;
    }

    /**
     * {@inheritdoc}
     * @return mixed username array
     */
    public static function getUsernames($ids)
    {
        return ArrayHelper::map(
            self::find()
                ->select(['id', 'username'])
                ->orderBy('username')
                ->where(['IN', 'id', $ids])
                ->asArray()
                ->all(),
            'id',
            'username'
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed User Names
     */
    public function getUsers()
    {
        return $this->hasOne(self::className(), ['id' => 'created_by']);
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultPassword()
    {
        static::DEFAULT_PASSWORD;
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public static function findByPasswordResetToken($token)
    {
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        //return $this->password === sha1($password);
        //return Yii::$app->security->validatePassword($password, $this->password);
        return Yii::$app->getSecurity()->validatePassword(
            $password,
            $this->password
        );
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        //$this->password = Security::generatePasswordHash($password);
        //$this->password = Yii::$app->security->generatePasswordHash($password);
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() .
            '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserDbLogBehavior::className()
            ],
        ];
    }
}
