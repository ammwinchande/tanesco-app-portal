<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use app\models\Reports;
use yii\data\ArrayDataProvider;

/**
 * ReportsSearch represents the model behind the search form of `app\models\Reports`.
 */
class ReportsSearch extends Reports
{
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                [
                    'Service_No', 'Service_Name', 'Service_Problem',
                    'Service_Status', 'Service_Date', 'Service_Duration',
                    'Customer_Name', 'Mobile_No', 'Zone_Name', 'Region_Name',
                    'District_Name', 'From_Date', 'To_Date', 'Date_Range'
                ],
                'safe'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Initialize the dataprovider by filling allModels
     *
     * @return ArayDataProvider
     */
    public function init()
    {
        $usesrId = Yii::$app->user->getId();
        $user = User::find()
            ->select(['zone_id', 'region_id', 'district_id'])
            ->where(['=', 'id', $usesrId])
            ->one();
        $district = District::find()
            ->select(['name'])
            ->where(['=', 'id', $user->district_id])
            ->one();
        $region = Region::find()
            ->select(['name'])
            ->where(['=', 'id', $user->region_id])
            ->one();
        $zone = Zone::find()
            ->select(['name'])
            ->where(['=', 'id', $user->zone_id])
            ->one();
        $districtName = $district ? ucwords($district->name) : '';
        $regionName = $region ? ucwords($region->name) : '';
        $zoneName = $zone ? ucwords($zone->name) : '';

        $filters = [
            'From_Date' => date('1-m-Y'),
            'To_Date' => date('t-m-Y'),
            'Zone_Name' => $zoneName,
            'Region_Name' => $regionName,
            'District_Name' => $districtName,
            'Service_Name' => '',
            'Service_Status' => '',
            'Service_Duration' => ''
        ];

        return self::fetchData($filters);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArayDataProvider
     */
    public function search(
        $fromDate = null,
        $toDate = null,
        $zone = '',
        $region = '',
        $district = '',
        $service = '',
        $status = '',
        $duration = ''
    ) {
        $role = User::getRole();

        $service = is_int($service) ?
            Service::find()->select(['name'])->where(['=', 'id', $service])->one() :
            $service;
        $district = is_int($district) ?
            District::find()->select(['name'])->where(['=', 'id', $district])->one()
            : $district;
        $region = is_int($region) ?
            Region::find()->select(['name'])->where(['=', 'id', $region])->one() :
            $region;
        $zone = is_int($zone) ?
            Zone::find()->select(['name'])->where(['=', 'id', $zone])->one() : $zone;

        if ($role == 'sysadmin' || $role == 'admin'
            || $role == 'hq-supervisor' || $role == 'hq-user'
        ) {
            $zone = '';
            $region = '';
            $district = '';
        } elseif ($role == 'zonal-user' || $role == 'zonal-supervisor'
            || $role == 'zonal-manager'
        ) {
            $region = '';
            $district = '';
        } elseif ($role == 'regional-user' || $role == 'regional-supervisor'
            || $role == 'regional-manager'
        ) {
            $district = '';
        }

        $filters = [
            'From_Date' => $fromDate,
            'To_Date' => $toDate,
            'Zone_Name' => $zone,
            'Region_Name' => $region,
            'District_Name' => $district,
            'Service_Name' => $service,
            'Service_Status' => $status != 'PENDING' ? $status : '',
            'Service_Duration' => $duration,
        ];

        return self::fetchData($filters);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $filters
     *
     * @return ArayDataProvider
     */
    public static function fetchData($filters = [])
    {
        $array = Yii::$app->soapclient->getServicesByDuration($filters);

        $data = [];
        if (count($array) > 1) {
            foreach (Yii::$app->formatData->xml2array($array)['Service'] as $value) {
                $data[] = Yii::$app->formatData->xml2array($value);
            }
        } elseif (count($array) == 1) {
            $value = Yii::$app->formatData->xml2arrayElements($array);
            $service = Json::encode(
                Yii::$app->formatData->xml2arrayElements($value)['Service']
            );
            $data[] = Json::decode($service);
        }

        $dataProvider = new ArrayDataProvider(
            [
                'allModels' => $data,
                'pagination' => [
                    'pageSize' => 500,
                ],
                'sort' => [
                    'attributes' => [
                        'Service_No',
                        'Service_Name',
                        'Service_Problem',
                        'Service_Status',
                        'Service_Date',
                        'Service_Duration',
                        'Customer_Name',
                        'Mobile_No',
                    ],
                    'defaultOrder' => [
                        'Service_Duration' => SORT_DESC,
                    ],
                ],
            ]
        );

        if (isset($_GET['ReportsSearch'])) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
