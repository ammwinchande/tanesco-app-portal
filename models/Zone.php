<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use \podtserkovsky\userdblog\behaviors\UserDbLogBehavior;

/**
 * This is the model class for table "zone".
 *
 * @property int $id
 * @property string $name Zone Name
 * @property string $zone_code Zone Code
 * @property string $created_by Created By
 * @property string $created_at Created At
 * @property string $updated_at Updated At
 */
class Zone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zone';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'zone_code', 'created_by'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['zone_code'], 'string', 'max' => 3],
            [['name', 'created_by'], 'string', 'max' => 64],
            [['name'], 'filter', 'filter' => 'strtolower'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Zone Name'),
            'zone_code' => Yii::t('app', 'Zone Code'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed User Names
     */
    public function getUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * {@inheritdoc}
     * @return mixed Zone Names
     */
    public static function getZones()
    {
        return ArrayHelper::map(
            self::find()->select(['id', 'name'])->orderBy('name')->asArray()->all(),
            'id',
            'name'
        );
    }

    /**
     * {@inheritdoc}
     * @return ZoneQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ZoneQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserDbLogBehavior::className()
            ],
        ];
    }
}
