<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ClientSearch represents the model behind the search form about `app\models\Client`.
 */
class ClientSearch extends Client
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'district_id', 'area_id', 'street_id', ], 'integer'],
            [['meter_no', 'mobile_no', 'name', 'device_id', 'conn_status', 'special_mark', 'is_blocked', 'is_notified', 'is_subscribed', 'lang_used', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'region_id' => $this->region_id,
            'district_id' => $this->district_id,
            'area_id' => $this->area_id,
            'street_id' => $this->street_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'meter_no', $this->meter_no])
            ->andFilterWhere(['like', 'mobile_no', $this->mobile_no])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'device_id', $this->device_id])
            ->andFilterWhere(['like', 'conn_status', $this->conn_status])
            ->andFilterWhere(['like', 'special_mark', $this->special_mark])
            ->andFilterWhere(['like', 'is_blocked', $this->is_blocked])
            ->andFilterWhere(['like', 'is_notified', $this->is_notified])
            ->andFilterWhere(['like', 'is_subscribed', $this->is_subscribed])
            ->andFilterWhere(['like', 'lang_used', $this->lang_used]);

        return $dataProvider;
    }
}
