<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "registration_code".
 *
 * @property integer $id
 * @property string $mobile_no
 * @property string $reg_code
 */
class RegistrationCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'registration_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mobile_no', 'reg_code'], 'required'],
            [['mobile_no'], 'string', 'max' => 15],
            [['generated_at', 'expired_at'], 'string', 'max' => 20],
            [['reg_code'], 'string', 'max' => 10],
            //[['mobile_no'], 'unique'],
            [['reg_code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mobile_no' => Yii::t('app', 'MobileNo'),
            'reg_code' => Yii::t('app', 'RegistrationCode'),
            'generated_at' => Yii::t('app', 'GeneratedAt'),
            'expired_at' => Yii::t('app', 'ExpiredAt'),
        ];
    }

    /**
     * @inheritdoc
     * @return RegistrationCodeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RegistrationCodeQuery(get_called_class());
    }

    public static function GenerateVerificationToken($mobile_no)
    {
        $today = new \DateTime('now');
        $gen_at = $today->format('Y-m-d H:i:s');
        $exp_at = $today->modify('+1 days')->format('Y-m-d H:i:s'); // 1 day to expiration

        $vt = self::find()->where(['=', 'mobile_no', $mobile_no])->one();
        if ($vt) {
            $vt->reg_code = self::generateRandomString();
            $vt->generated_at = $gen_at;
            $vt->expired_at = $exp_at;
            if ($vt->save(false)) {
                return $vt->reg_code;
            } else {
                return false;
            }
        } else {
            $registration_code = new self;
            $registration_code->mobile_no = $mobile_no;
            $registration_code->reg_code = self::generateRandomString();
            $registration_code->generated_at = $gen_at;
            $registration_code->expired_at = $exp_at;
            if ($registration_code->save(false)) {
                return $registration_code->reg_code;
            } else {
                return false;
            }
        }
    }

    private static function generateRandomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return substr_replace(strtoupper($randomString), '-', 3, 0);
    }

    public static function pushSMS($mobile_no, $message)
    {
        $url = 'http://192.168.1.92/v1/push/send-sms.html?mobileno=' . $mobile_no . '&message=' . $message;
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = \yii\helpers\Json::decode(curl_exec($ch));
        return $result;
    }

    public static function validateVerificationToken($registration_code, $mobile_no)
    {
        $verify = self::find()->where('mobile_no=:mn and reg_code=:rc', [':mn' => $mobile_no, ':rc' => $registration_code])->one();
        if ($verify) {
            $now_date = (new \DateTime('now'));
            $token_date = new \DateTime($verify->expired_at);
            return ($now_date > $token_date) ? false : true;
        } else {
            return false;
        }
    }

    public static function sendVerificationToken($mobile_no)
    {
        $verification_token = self::GenerateVerificationToken($mobile_no);
        if ($verification_token) {
            $send_token = self::pushSMS($mobile_no, $verification_token);
            $result = json_decode($send_token);

            if ($result) {
                return ['status' => 'success', 'message' => 'Verification token has been sent to your phone'];
            } else {
                return ['status' => 'fail', 'message' => 'Fail to send generated token'];
            }
        } else {
            return ['status' => 'fail', 'message' => 'Fail to generate token'];
        }
    }



    /**
     * Deletes an existing RegistrationCode model.
     * If deletion is successful, return true.
     *
     * @param string $mobile_no
     * @return bool
     */
    public static function delete($mobile_no)
    {
        self::find()->where(['=', 'mobile_no', $mobile_no])->delete();
        return true;
    }
}
