<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ServiceReport]].
 *
 * @see ServiceReport
 */
class ServiceReportQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ServiceReport[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ServiceReport|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
