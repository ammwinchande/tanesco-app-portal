<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[LoginHistory]].
 *
 * @see LoginHistory
 */
class LoginHistoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LoginHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LoginHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
