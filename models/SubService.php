<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use \podtserkovsky\userdblog\behaviors\UserDbLogBehavior;

/**
 * This is the model class for table "sub_service".
 *
 * @property int $id
 * @property string $name Sub-Service Name
 * @property string $name Swahili Sub-Service Name
 * @property bool $is_meter_required is Meter Required
 * @property int $service_id Service ID
 * @property int $created_by Created by
 * @property string $created_at Created At
 * @property string $updated_at Updated At
 *
 * @property Service $service
 */
class SubService extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sub_service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'name_sw', 'service_id'], 'required'],
            [['is_meter_required', 'service_id', 'created_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'name_sw'], 'string', 'max' => 100],
            [['name', 'name_sw'], 'filter', 'filter' => 'strtolower'],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Sub-Service Name'),
            'name_sw' => Yii::t('app', 'Swahili Sub-Service Name'),
            'is_meter_required' => Yii::t('app', 'Is Meter Required'),
            'service_id' => Yii::t('app', 'Service Name'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed User Names
     */
    public function getUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Service Names
     */
    public function getServices()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Region Names
     */
    public function getReports()
    {
        return $this->hasMany(ServiceReport::className(), ['sub_service_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getService($id)
    {
        return Service::find()->where(['=', 'id', $id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed ServiceReports Object
     */
    public static function getReport($id)
    {
        return ServiceReport::find()->where(['=', 'sub_service_id', $id])->orderBy('created_at')->asArray->all();
    }

    /**
     * {@inheritdoc}
     * @return mixed Zone Names
     */
    public static function getSubServices()
    {
        return ArrayHelper::map(self::find()->select(['id', 'name', 'name_sw'])->orderBy('name')->asArray()->all(), 'id', 'name');
    }

    /**
     * {@inheritdoc}
     * @return SubServiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SubServiceQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserDbLogBehavior::className()
            ],
        ];
    }
}
