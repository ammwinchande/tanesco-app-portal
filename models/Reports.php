<?php

namespace app\models;

use Yii;
use yii\base\Model;
use \podtserkovsky\userdblog\behaviors\UserDbLogBehavior;

/**
 * This is the model class behind "reports search-form".
 *
 * @property string $Zone_Name
 * @property string $Region_Name
 * @property string $District_Name
 * @property string $Date_Range
 * @property string $From_Date
 * @property string $To_Date
 * @property string $Service_No
 * @property string $Service_Name
 * @property string $Service_Problem
 * @property string $Service_Status
 * @property string $Service_Date
 * @property string $Service_Duration
 * @property string $Customer_Name
 * @property string $Mobile_No
 */
class Reports extends Model
{
    public $Zone_Name;
    public $Region_Name;
    public $District_Name;
    public $Date_Range;
    public $From_Date;
    public $To_Date;
    public $Service_No;
    public $Service_Name;
    public $Service_Problem;
    public $Service_Status;
    public $Service_Date;
    public $Service_Duration;
    public $Customer_Name;
    public $Mobile_No;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                [
                    'Service_No', 'Service_Name', 'Service_Problem',
                    'Service_Status', 'Service_Date', 'Service_Duration',
                    'Customer_Name', 'Mobile_No', 'Zone_Name', 'Region_Name',
                    'District_Name', 'From_Date', 'To_Date', 'Date_Range'
                ],
                'string'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Zone_Name' => Yii::t('app', 'Zone Name'),
            'Region_Name' => Yii::t('app', 'Region Name'),
            'District_Name' => Yii::t('app', 'District Name'),
            'Date_Range' => Yii::t('app', 'Date Range'),
            'From_Date' => Yii::t('app', 'From Date'),
            'To_Date' => Yii::t('app', 'To Date'),
            'Service_No' => Yii::t('app', 'Service Number'),
            'Service_Name' => Yii::t('app', 'Service Name'),
            'Service_Problem' => Yii::t('app', 'Service Problem'),
            'Service_Status' => Yii::t('app', 'Service Status'),
            'Service_Date' => Yii::t('app', 'Service Date'),
            'Service_Duration' => Yii::t('app', 'Service Duration'),
            'Customer_Name' => Yii::t('app', 'Customer Name'),
            'Mobile_No' => Yii::t('app', 'Mobile Number'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserDbLogBehavior::className()
            ],
        ];
    }
}
