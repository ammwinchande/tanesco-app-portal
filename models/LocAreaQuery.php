<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[LocArea]].
 *
 * @see LocArea
 */
class LocAreaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LocArea[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LocArea|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
