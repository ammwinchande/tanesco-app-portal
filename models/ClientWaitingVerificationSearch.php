<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClientWaitingVerification;

/**
 * ClientWaitingVerificationSearch represents the model behind the search form of `app\models\ClientWaitingVerification`.
 */
class ClientWaitingVerificationSearch extends ClientWaitingVerification
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'conn_status', 'region_id', 'district_id', 'area_id', 'street_id', 'is_blocked', 'is_notified', 'is_subscribed'], 'integer'],
            [['meter_no', 'mobile_no', 'name', 'device_id', 'special_mark', 'lang_used', 'created_at', 'session_start_date', 'session_end_date', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientWaitingVerification::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'conn_status' => $this->conn_status,
            'region_id' => $this->region_id,
            'district_id' => $this->district_id,
            'area_id' => $this->area_id,
            'street_id' => $this->street_id,
            'is_blocked' => $this->is_blocked,
            'is_notified' => $this->is_notified,
            'is_subscribed' => $this->is_subscribed,
            'created_at' => $this->created_at,
            'session_start_date' => $this->session_start_date,
            'session_end_date' => $this->session_end_date,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'meter_no', $this->meter_no])
            ->andFilterWhere(['like', 'mobile_no', $this->mobile_no])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'device_id', $this->device_id])
            ->andFilterWhere(['like', 'special_mark', $this->special_mark])
            ->andFilterWhere(['like', 'lang_used', $this->lang_used]);

        return $dataProvider;
    }
}
