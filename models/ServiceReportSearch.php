<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ServiceReportSearch represents the model behind
 * the search form about `app\models\ServiceReport`.
 */
class ServiceReportSearch extends ServiceReport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id', 'service_id', 'sub_service_id', 'reported_id',
                    'resolved_id', 'region', 'district', 'area', 'street',
                    'customer_id'
                ],
                'integer'
            ],
            [
                [
                    'details', 'remarks', 'special_mark', 'picture', 'video',
                    'coverage', 'meter_no', 'service_no', 'status', 'feedback',
                    'rating', 'send_to_sdm', 'created_at', 'updated_at',
                    'resolved_at'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ServiceReport::find()->with(
            [
                'services', 'subServices', 'clients',
                'regions', 'districts', 'areas', 'streets'
            ]
        );

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => 500,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_DESC,
                    ]
                ],
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'id' => $this->id,
                'service_id' => $this->service_id,
                'sub_service_id' => $this->sub_service_id,
                'reported_id' => $this->reported_id,
                'resolved_id' => $this->resolved_id,
                'customer_id' => $this->customer_id,
                'region' => $this->region,
                'district' => $this->district,
                'area' => $this->area,
                'street' => $this->street,
                'meter_no' => $this->meter_no,
                'service_no' => $this->service_no,
                'send_to_sdm' => $this->send_to_sdm,
            ]
        );

        $query->andFilterWhere(['like', 'details', $this->details])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'special_mark', $this->special_mark])
            ->andFilterWhere(['like', 'picture', $this->picture])
            ->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'coverage', $this->coverage])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'feedback', $this->feedback])
            ->andFilterWhere(['like', 'rating', $this->rating]);

        return $dataProvider;
    }
}
