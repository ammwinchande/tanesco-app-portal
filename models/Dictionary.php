<?php

namespace app\models;

use Yii;
use \podtserkovsky\userdblog\behaviors\UserDbLogBehavior;

/**
 * This is the model class for table "dictionary".
 *
 * @property int $id
 * @property string $en
 * @property string $sw
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class Dictionary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dictionary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['en', 'sw'], 'required'],
            [['created_by'], 'integer'],
            [['en', 'sw'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'en' => Yii::t('app', 'En'),
            'sw' => Yii::t('app', 'Sw'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return True if data inserted successfully
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_by = \Yii::$app->user->id;
            }
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed User Names
     */
    public function getUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * {@inheritdoc}
     * @return DictionaryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DictionaryQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserDbLogBehavior::className()
            ],
        ];
    }
}
