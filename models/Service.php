<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use \podtserkovsky\userdblog\behaviors\UserDbLogBehavior;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $name Service Name
 * @property string $name_sw Swahili Service Name
 * @property int $created_by Created by
 * @property string $created_at Created At
 * @property string $updated_at Updated At
 *
 * @property Category[] $categories
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'name_sw'], 'required'],
            [['service_icon'], 'file', 'extensions' => 'jpg, png, gif', 'mimeTypes' => 'image/jpeg, image/png', 'skipOnEmpty' => false],
            [['created_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'name_sw'], 'string', 'max' => 100],
            [['name', 'name_sw'], 'filter', 'filter' => 'strtolower'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Service Name'),
            'name_sw' => Yii::t('app', 'Swahili Service Name'),
            'service_icon' => Yii::t('app', 'Service Icon'),
            'created_by' => Yii::t('app', 'Created by'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return True if data inserted successfully
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_by = \Yii::$app->user->id;
            }
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               User Names
     */
    public function getUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubServices()
    {
        return $this->hasMany(SubService::className(), ['service_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               Region Names
     */
    public function getReports()
    {
        return $this->hasMany(ServiceReport::className(), ['service_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               SubService Object
     */
    public static function getSubService($id)
    {
        return SubService::find()->where(['=', 'id', $id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               ServiceReports Object
     */
    public static function getReport($id)
    {
        return ServiceReport::find()->where(['=', 'service_id', $id])->orderBy('created_at')->all();
    }

    /**
     * {@inheritdoc}
     * @return mixed Zone Names
     */
    public static function getServices()
    {
        return ArrayHelper::map(self::find()->select(['id', 'name', 'name_sw'])->orderBy('name')->asArray()->all(), 'id', 'name');
    }

    /**
     * {@inheritdoc}
     * @return ServiceQuery the active query used by this AR class
     */
    public static function find()
    {
        return new ServiceQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserDbLogBehavior::className()
            ],
        ];
    }
}
