<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use \podtserkovsky\userdblog\behaviors\UserDbLogBehavior;

/**
 * This is the model class for table "{{%menu_panel}}".
 *
 * @property int $id ID
 * @property string $type Type
 * @property int $parent_id ParentID
 * @property string $name Name
 * @property string $redirect Link
 * @property string $params Params
 * @property int $created_by CreatedBy
 * @property int $status Status
 * @property int $order_index Order
 * @property string $icon Icon
 */
class MenuPanel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu_panel}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'name', 'redirect'], 'required'],
            [['parent_id', 'created_by', 'status', 'order_index', 'created_by'], 'integer'],
            [['type', 'redirect', 'params', 'icon'], 'string', 'max' => 45],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'parent_id' => Yii::t('app', 'ParentID'),
            'name' => Yii::t('app', 'Name'),
            'redirect' => Yii::t('app', 'Link'),
            'params' => Yii::t('app', 'Params'),
            'created_by' => Yii::t('app', 'CreatedBy'),
            'status' => Yii::t('app', 'Status'),
            'order_index' => Yii::t('app', 'Order'),
            'icon' => Yii::t('app', 'Icon'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return True if data inserted successfully
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->parent_id = $this->parent_id ?: 0;
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed User Names
     */
    public function getUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed User Names
     */
    public function getParents()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    /**
     * {@inheritdoc}
     * @return mixed username array
     */
    public static function getParentMenu($parents)
    {
        return ArrayHelper::map(
            self::find()->select(['id', 'name'])->where(['IN', 'id', $parents])->orderBy('name')->asArray()->all(),
            'id',
            'name'
        );
    }

    /**
     * {@inheritdoc}
     * @return mixed Zone Names
     */
    public static function getMenu()
    {
        return ArrayHelper::map(self::find()->select(['id', 'name'])->orderBy('name')->asArray()->all(), 'id', 'name');
    }

    /**
     * {@inheritdoc}
     * @return MenuPanelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MenuPanelQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserDbLogBehavior::className()
            ],
        ];
    }
}
