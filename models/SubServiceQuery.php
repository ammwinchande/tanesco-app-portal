<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SubService]].
 *
 * @see SubService
 */
class SubServiceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return SubService[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SubService|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
