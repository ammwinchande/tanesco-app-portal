<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use \podtserkovsky\userdblog\behaviors\UserDbLogBehavior;

/**
 * This is the model class for table "loc_area".
 *
 * @property int $id
 * @property string $name Area Name
 */
class LocArea extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loc_area';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'district_id', 'name', 'area_code'], 'required'],
            [['id', 'region_id', 'district_id'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['name'], 'filter', 'filter' => 'strtolower'],
            [['area_code'], 'string', 'max' => 3],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Area Name'),
            'area_code' => Yii::t('app', 'Area Code'),
            'region_id' => Yii::t('app', 'Region Name'),
            'district_id' => Yii::t('app', 'District Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               Service Names
     */
    public function getRegions()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               Service Names
     */
    public function getDistricts()
    {
        return $this->hasOne(District::className(), ['id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               Region Names
     */
    public function getReports()
    {
        return $this->hasMany(ServiceReport::className(), ['area' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Client Names
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['area_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getZone($id)
    {
        return Zone::find()->where(['=', 'id', $id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getRegion($id)
    {
        return Region::find()->where(['=', 'id', $id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getDistrict($id)
    {
        return District::find()->where(['=', 'id', $id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getArea($id)
    {
        return LocArea::find()->where(['=', 'id', $id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getStreet($id)
    {
        return LocStreet::find()->where(['=', 'id', $id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               ServiceReports Object
     */
    public static function getReport($id)
    {
        return ServiceReport::find()->where(['=', 'area', $id])
            ->orderBy('created_at')->all();
    }

    /**
     * {@inheritdoc}
     * @return mixed Zone Names
     */
    public static function getAreas()
    {
        return ArrayHelper::map(
            self::find()->select(['id', 'name'])
                ->orderBy('name')->asArray()->all(),
            'id',
            'name'
        );
    }

    /**
     * {@inheritdoc}
     * @return LocAreaQuery the active query used by this AR class
     */
    public static function find()
    {
        return new LocAreaQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserDbLogBehavior::className()
            ],
        ];
    }
}
