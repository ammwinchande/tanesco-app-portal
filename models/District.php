<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use \podtserkovsky\userdblog\behaviors\UserDbLogBehavior;

/**
 * This is the model class for table "district".
 *
 * @property int $id
 * @property string $name District Name
 * @property int $region_id
 * @property string $dis_code District ID
 * @property string $created_by Created By
 * @property string $created_at Created At
 * @property string $updated_at Updated At
 */
class District extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'district';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'region_id', 'dis_code', 'created_by'], 'required'],
            [['region_id', 'zone_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 64],
            [['name'], 'filter', 'filter' => 'strtolower'],
            [['dis_code'], 'string', 'max' => 16],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'District Name'),
            'dis_code' => Yii::t('app', 'District Code'),
            'zone_id' => Yii::t('app', 'Zone Name'),
            'region_id' => Yii::t('app', 'Region Name'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed User Names
     */
    public function getUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Service Names
     */
    public function getZones()
    {
        return $this->hasOne(Zone::className(), ['id' => 'zone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Service Names
     */
    public function getRegions()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Region Names
     */
    public function getReports()
    {
        return $this->hasMany(ServiceReport::className(), ['district' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Client Names
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['district_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed ServiceReports Object
     */
    public static function getReport($id)
    {
        return ServiceReport::find()->where(['=', 'district', $id])->orderBy('created_at')->all();
    }

    /**
     * {@inheritdoc}
     * @return mixed Zone Names
     */
    public static function getDistricts()
    {
        return ArrayHelper::map(self::find()->select(['id', 'name'])->orderBy('name')->asArray()->all(), 'id', 'name');
    }

    /**
     * {@inheritdoc}
     * @return DistrictQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DistrictQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserDbLogBehavior::className()
            ],
        ];
    }
}
