<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RegistrationCode]].
 *
 * @see RegistrationCode
 */
class RegistrationCodeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RegistrationCode[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RegistrationCode|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}