<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_waiting_verification".
 *
 * @property int $id
 * @property string $meter_no Client Meter Number
 * @property string $mobile_no Client Mobile Number
 * @property string $name Client Name
 * @property string $device_id Client Device ID
 * @property int $conn_status Connection Status
 * @property int $region_id Region ID
 * @property int $district_id District ID
 * @property int $area_id Area ID
 * @property int $street_id Street ID
 * @property string $special_mark Special Mark
 * @property int $is_blocked True if Client is blocked
 * @property int $is_notified Is Notified
 * @property int $is_subscribed Is Subscribed
 * @property string $lang_used Laguage used (default English)
 * @property string $created_at Created Time
 * @property string $session_start_date Session Start Date
 * @property string $session_end_date Session End Date
 * @property string $updated_at Updated Time
 * @property string $deleted_at Deleted Time
 */
class ClientWaitingVerification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_waiting_verification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'mobile_no', 'device_id'], 'required'],
            [['id', 'conn_status', 'region_id', 'district_id', 'area_id', 'street_id', 'is_blocked', 'is_notified', 'is_subscribed'], 'integer'],
            [['lang_used'], 'string'],
            [['created_at', 'session_start_date', 'session_end_date', 'updated_at', 'deleted_at'], 'safe'],
            [['meter_no'], 'string', 'max' => 11],
            [['mobile_no'], 'string', 'max' => 13],
            [['name'], 'string', 'max' => 256],
            [['device_id', 'special_mark'], 'string', 'max' => 191],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'meter_no' => Yii::t('app', 'Client Meter Number'),
            'mobile_no' => Yii::t('app', 'Client Mobile Number'),
            'name' => Yii::t('app', 'Client Name'),
            'device_id' => Yii::t('app', 'Client Device ID'),
            'conn_status' => Yii::t('app', 'Connection Status'),
            'region_id' => Yii::t('app', 'Region ID'),
            'district_id' => Yii::t('app', 'District ID'),
            'area_id' => Yii::t('app', 'Area ID'),
            'street_id' => Yii::t('app', 'Street ID'),
            'special_mark' => Yii::t('app', 'Special Mark'),
            'is_blocked' => Yii::t('app', 'True if Client is blocked'),
            'is_notified' => Yii::t('app', 'Is Notified'),
            'is_subscribed' => Yii::t('app', 'Is Subscribed'),
            'lang_used' => Yii::t('app', 'Laguage used (default English)'),
            'created_at' => Yii::t('app', 'Created Time'),
            'session_start_date' => Yii::t('app', 'Session Start Date'),
            'session_end_date' => Yii::t('app', 'Session End Date'),
            'updated_at' => Yii::t('app', 'Updated Time'),
            'deleted_at' => Yii::t('app', 'Deleted Time'),
        ];
    }
}
