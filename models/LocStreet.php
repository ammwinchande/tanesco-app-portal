<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use \podtserkovsky\userdblog\behaviors\UserDbLogBehavior;

/**
 * This is the model class for table "loc_street".
 *
 * @property int $id
 * @property string $name Street Name
 * @property string $special_mark Special Mark
 */
class LocStreet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loc_street';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'street_code'], 'required'],
            [['id', 'region_id', 'district_id', 'area_id'], 'integer'],
            [['name', 'street_code', 'created_at'], 'string', 'max' => 64],
            [['name'], 'filter', 'filter' => 'strtolower'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Street Name'),
            'street_code' => Yii::t('app', 'Street Code'),
            'region_id' => Yii::t('app', 'Region Name'),
            'district_id' => Yii::t('app', 'District Name'),
            'area_id' => Yii::t('app', 'Area Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               Region Names
     */
    public function getRegions()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               District Names
     */
    public function getDistricts()
    {
        return $this->hasOne(District::className(), ['id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               Area Names
     */
    public function getAreas()
    {
        return $this->hasOne(LocArea::className(), ['id' => 'area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               Service Reports
     */
    public function getReports()
    {
        return $this->hasMany(ServiceReport::className(), ['street' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Client Names
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['street_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed               ServiceReports Object
     */
    public static function getReport($id)
    {
        return ServiceReport::find()->where(['=', 'area', $id])
            ->orderBy('created_at')->all();
    }

    /**
     * {@inheritdoc}
     * @return mixed Zone Names
     */
    public static function getStreets()
    {
        return ArrayHelper::map(self::find()->select(['id', 'name'])
            ->orderBy('name')->asArray()->all(), 'id', 'name');
    }

    /**
     * {@inheritdoc}
     * @return LocStreetQuery the active query used by this AR class
     */
    public static function find()
    {
        return new LocStreetQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserDbLogBehavior::className()
            ],
        ];
    }
}
