<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "service_report".
 *
 * @property int $id
 * @property string $details Report Details
 * @property string $remarks Remarks from SDM
 * @property int $region Region
 * @property int $district District
 * @property int $area Area
 * @property string $special_mark Special Mark
 * @property int $street Street
 * @property string $picture Picture Path
 * @property string $video Video Path
 * @property string $coverage Coverage
 * @property string $meter_no Meter Number
 * @property string $service_no Service Number
 * @property string $Status Status
 * @property string $feedback Feedback
 * @property string $rating Rating
 * @property int $service_id Service
 * @property int $sub_service_id Sub-Service
 * @property int $reported_id Reported ID
 * @property int $resolved_id Resolved ID
 * @property int $send_to_sdm
 * @property int $customer_id Customer
 */
class ServiceReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'details', 'region', 'district', 'area', 'special_mark',
                    'street', 'service_no', 'status', 'service_id', 'sub_service_id',
                    'customer_id', 'meter_no', 'picture', 'video', 'coverage',
                    'feedback', 'rating', 'reported_id', 'resolved_id',
                    'send_to_sdm', 'created_at', 'updated_at', 'resolved_at'
                ],
                'safe'
            ],
            [['details', 'remarks'], 'string'],
            [
                [
                    'service_id', 'sub_service_id', 'reported_id',
                    'resolved_id', 'send_to_sdm', 'region', 'district',
                    'area', 'street', 'customer_id'
                ],
                'integer'
            ],
            [
                [
                    'special_mark', 'coverage', 'service_no', 'rating'
                ],
                'string',
                'max' => 64
            ],
            [['picture', 'video', 'feedback'], 'string', 'max' => 256],
            [['status', 'meter_no'], 'string', 'max' => 16],
            [['remarks'], 'filter', 'filter' => 'strtolower'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'details' => Yii::t('app', 'Report Details'),
            'meter_no' => Yii::t('app', 'Meter Number'),
            'coverage' => Yii::t('app', 'Coverage'),
            'service_no' => Yii::t('app', 'Service Number'),
            'picture' => Yii::t('app', 'Photo'),
            'video' => Yii::t('app', 'Video'),
            'remarks' => Yii::t('app', 'Remarks'),
            'rating' => Yii::t('app', 'Rating'),
            'feedback' => Yii::t('app', 'Feedback'),
            'service_id' => Yii::t('app', 'Service'),
            'sub_service_id' => Yii::t('app', 'Sub-Service'),
            'region' => Yii::t('app', 'Region'),
            'district' => Yii::t('app', 'District'),
            'area' => Yii::t('app', 'Area'),
            'street' => Yii::t('app', 'Street'),
            'special_mark' => Yii::t('app', 'Special Mark'),
            'status' => Yii::t('app', 'Status'),
            'send_to_sdm' => Yii::t('app', 'Sent To SDM'),
            'send_to_sdm' => Yii::t('app', 'Sent To SDM'),
            'reported_id' => Yii::t('app', 'Reported ID'),
            'resolved_id' => Yii::t('app', 'Resolved ID'),
            'customer_id' => Yii::t('app', 'Reported by'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'resolved_at' => Yii::t('app', 'Resolved At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Region Names
     */
    public function getRegions()
    {
        return $this->hasOne(Region::className(), ['id' => 'region']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed District Names
     */
    public function getDistricts()
    {
        return $this->hasOne(District::className(), ['id' => 'district']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Area Names
     */
    public function getAreas()
    {
        return $this->hasOne(LocArea::className(), ['id' => 'area']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Streets Names
     */
    public function getStreets()
    {
        return $this->hasOne(LocStreet::className(), ['id' => 'street']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Service Names
     */
    public function getServices()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Sub-Service Names
     */
    public function getSubServices()
    {
        return $this->hasOne(SubService::className(), ['id' => 'sub_service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Clients Names
     */
    public function getClients()
    {
        return $this->hasOne(Client::className(), ['id' => 'customer_id']);
    }

    /**
     * {@inheritdoc}
     * @return mixed Client Names
     */
    public static function getAllClients()
    {
        return ArrayHelper::map(
            Client::find()->select(['id', 'name'])->where(
                [
                    'IN',
                    'id',
                    self::find()->select(['customer_id'])->distinct()
                ]
            )->all(),
            'id',
            'name',
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Service Object
     */
    public static function getService($id)
    {
        return Service::find()->where(['=', 'id', $id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed SubService Object
     */
    public static function getSubService($id)
    {
        return SubService::find()->where(['=', 'id', $id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Client Object
     */
    public static function getClient($id)
    {
        return Client::find()->where(['=', 'id', $id])->one();
    }

    /**
     * {@inheritdoc}
     * @return ServiceReportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ServiceReportQuery(get_called_class());
    }
}
