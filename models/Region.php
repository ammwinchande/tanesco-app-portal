<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use \podtserkovsky\userdblog\behaviors\UserDbLogBehavior;

/**
 * This is the model class for table "region".
 *
 * @property int $id
 * @property string $name Region Name
 * @property string $reg_code
 * @property string $created_by Created By
 * @property string $created_at Created At
 * @property string $updated_at Updated At
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'region';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'reg_code', 'zone_id', 'created_by'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'created_by'], 'string', 'max' => 64],
            [['name'], 'filter', 'filter' => 'strtolower'],
            [['reg_code'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Region Name'),
            'reg_code' => Yii::t('app', 'Region Code'),
            'zone_id' => Yii::t('app', 'Zone Name'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed User Names
     */
    public function getUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Service Names
     */
    public function getZones()
    {
        return $this->hasOne(Zone::className(), ['id' => 'zone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed ServiceReports Names
     */
    public function getReports()
    {
        return $this->hasMany(ServiceReport::className(), ['region' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed Client Names
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getZone($id)
    {
        return Zone::find()->where(['=', 'id', $id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     * @return mixed ServiceReports Object
     */
    public static function getReport($id)
    {
        return ServiceReport::find()
            ->where(['=', 'region', $id])
            ->orderBy('created_at')
            ->all();
    }

    /**
     * {@inheritdoc}
     * @return mixed Zone Names
     */
    public static function getRegions()
    {
        return ArrayHelper::map(
            self::find()->select(['id', 'name'])->orderBy('name')->asArray()->all(),
            'id',
            'name'
        );
    }

    /**
     * {@inheritdoc}
     * @return RegionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RegionQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserDbLogBehavior::className()
            ],
        ];
    }
}
