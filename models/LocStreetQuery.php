<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[LocStreet]].
 *
 * @see LocStreet
 */
class LocStreetQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LocStreet[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LocStreet|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
